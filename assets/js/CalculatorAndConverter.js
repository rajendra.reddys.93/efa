﻿//Functions Related to resistance conversions
function convertResistanceToNumber(ohms, multiplier) {
    switch (multiplier) {
        case 0: ohms = ohms;
            break;
        case 1: ohms = ohms * 1000;
            break;
        case 2: ohms = ohms * 1000000;
            break;
        default: ohms = ohms;
            break;
    }
    return ohms;
}
function convertNumberToResistance(ohms, multiplier) {
    switch (multiplier) {
        case 0: ohms = ohms;
            break;
        case 1: ohms = ohms / 1000;
            break;
        case 2: ohms = ohms / 1000000;
            break;
        default: ohms = ohms;
            break;
    }
    return ohms;
}
function appendMultiplierToResistance(ohms) {
    if (ohms > 999999)
        return ((ohms / 1000000) + "M");
    else if (ohms > 999)
        return ((ohms / 1000) + "K");
    else
        return ohms;
}


//Functions Related to current conversions
function convertCurrentToNumber(amps, multiplier) {
    switch (multiplier) {
        case 0: amps = amps / 1000000;
            break;
        case 1: amps = amps / 1000;
            break;
        case 2: amps = amps;
            break;
        default: amps = amps;
            break;
    }
    return amps;
}
function convertNumberToCurrent(amps, multiplier) {
    switch (multiplier) {
        case 0: amps = amps * 1000000;
            break;
        case 1: amps = amps * 1000;
            break;
        case 2: amps = amps;
            break;
        default: amps = amps;
            break;
    }
    return amps;
}


//Functions Related Capacitance conversions
function convertCapacitanceToNumber(farads, multiplier) {
    switch (multiplier) {
        case 0: farads = farads;
            break;
        case 1: farads = farads / 1000;
            break;
        case 2: farads = farads / 1000000;
            break;
        case 3: farads = farads / 1000000000;
            break;
        case 4: farads = farads / 1000000000000;
            break;
        default: farads = farads;
            break;
    }
    return farads;
}
function convertNumberToCapacitance(farads, multiplier) {
    switch (multiplier) {
        case 0: farads = farads;
            break;
        case 1: farads = farads * 1000;
            break;
        case 2: farads = farads * 1000000;
            break;
        case 3: farads = farads * 1000000000;
            break;
        case 4: farads = farads * 1000000000000;
            break;
        default: farads = farads;
            break;
    }
    return farads;
}


//Functions Related to voltage conversions
function convertVoltageToNumber(volts, multiplier) {
    switch (multiplier) {
        case 0: volts = volts/1000;
            break;
        case 1: volts = volts;
            break;
        default: volts = volts;
            break;
    }
    return volts;
}
function convertNumberToVoltage(volts, multiplier) {
    switch (multiplier) {
        case 0: volts = volts * 1000;
            break;
        case 1: volts = volts;
            break;
        default: volts = volts;
            break;
    }
    return volts;
}

//Functions Related to number system conversions
//Conversion from DEC - BIN, OCT, HEX;
function convertDecToBin(Dec) {
    return Dec.toString(2);
}
function convertDecToOct(Dec) {
    return ("0" + Dec.toString(8));
}
function convertDecToHex(Dec) {
    return ("0x" + (Number(Dec).toString(16))).toUpperCase();
}

//Conversion from HEX - BIN, OCT, DEC;
function convertHexToBin(Hex) {
    return convertDecToBin(convertHexToDec(Hex));
}
function convertHexToOct(Hex) {
    return convertDecToOct(convertHexToDec(Hex));
}
function convertHexToDec(Hex) {
    return parseInt(Hex, 16);
}

//Conversion from OCT - BIN, DEC, HEX;
function convertOctToDec(Oct) {
    return parseInt(Oct, 10);
}
function convertOctToBin(Oct) {
    return convertDecToBin(convertOctToDec(Oct));
}
function convertOctToHex(Oct) {
    return convertDecToHex(convertOctToDec(Oct));
}

//Conversion from BIN - OCT, DEC, HEX;
function convertBinToDec(Bin) {
    return parseInt(Bin, 10);
}
function convertBinToOct(Bin) {
    return convertDecToOct(convertBinToDec(Bin));
}
function convertBinToHex(Bin) {
    return convertDecToHex(convertBinToDec(Bin));
}

//Conversions relateed to Length
//takes meter as input and converts to desired type
function convertLengthToNumber(len, multiplier) {
    switch (multiplier) {
        case 0: len = len / 1000000000;
            break;
        case 1: len = len / 1000000;
            break;
        case 2: len = len / 1000;
            break;
        case 3: len = len / 100;
            break;
        case 4: len = len / 10;
            break;
        case 5: len = len;
            break;
        case 6: len = len * 10;
            break;
        case 7: len = len * 100;
            break;
        case 8: len = len * 1000;
            break;
        case 9: len = (len * 1000) / 0.6214;
            break;
        case 10: len = len / 1.0932;
            break;
        case 11: len = len / 3.281;
            break;
        case 12: len = len / (1/ 0.0254);
            break;
        default: len = len;
            break;
    }
    return len;
}
//converts to meter from other types
function convertNumberToLength(len, multiplier) {
    switch (multiplier) {
        case 0: len = len * 1000000000;
            break;
        case 1: len = len * 1000000;
            break;
        case 2: len = len * 1000;
            break;
        case 3: len = len * 100;
            break;
        case 4: len = len * 10;
            break;
        case 5: len = len;
            break;
        case 6: len = len / 10;
            break;
        case 7: len = len / 100;
            break;
        case 8: len = len / 1000;
            break;
        case 9: len = (len / 1000) / 1.60934;
            break;
        case 10: len = len / 0.9144;
            break;
        case 11: len = len / (1/3.281);
            break;
        case 12: len = len / 0.0254;
            break;
        default: len = len;
            break;
    }
    return len;
}

//Convetions Related to Temparature
//Convert from Fahrenheit to Celcius
function convFahToCel(fah) {
    return ((fah - 32) * (5 / 9));
}
//Convert from Celcius to Fahrenheit
function convCelToFah(cel) {
    return ((cel * (9 / 5)) + 32);
}
//Convert from Fahrenheit to Kelvin
function convFahToKel(fah) {
    return (((fah - 32) * (5 / 9)) + 273.15);
}
//Convert from Kelvin to Fahrenheit
function convKelToFah(kel) {
    return (((kel - 273.15) * (9 / 5)) + 32);
}
//Convert from Celcius to Kelvin
function convCelToKel(cel) {
    return (cel + 273.15);
}
//Convert from Kelvin to Celcius
function convKelToCel(kel) {
    return (kel - 273.15);
}

//Conversion Between Data Rates
//convert bytes to desired data rate
function convBytesToDataRate(to, toMul) {
    switch (toMul) {
        case 0: to = to * 8;
            break;
        case 1: to = to;
            break;
        case 2: to = (to / 1000) * 8;
            break;
        case 3: to = to / 1000;
            break;
        case 4: to = (to / 1000000) * 8;
            break;
        case 5: to = to / 1000000;
            break;
        case 6: to = (to / 1000000000) * 8;
            break;
        case 7: to = to / 1000000000;
            break;
        case 8: to = (to / 1000000000000) * 8;
            break;
        case 9: to = to / 1000000000000;
            break;
        default: to = to;
            break;
    }
    return to;
}
//convert desired data rate to bytes
function convDataRateToBytes(to, toMul) {
    switch (toMul) {
        case 0: to = to / 8;
            break;
        case 1: to = to;
            break;
        case 2: to = (to * 1000) / 8;
            break;
        case 3: to = to * 1000;
            break;
        case 4: to = (to * 1000000) / 8;
            break;
        case 5: to = to * 1000000;
            break;
        case 6: to = (to * 1000000000) / 8;
            break;
        case 7: to = to * 1000000000;
            break;
        case 8: to = (to * 1000000000000) / 8;
            break;
        case 9: to = to * 1000000000000;
            break;
        default: to = to;
            break;
    }
    return to;
}

//Conversions Related to time
//convert minutes to desired time
function convMinutesToTime(time, mul) {
    switch (mul) {
        case 0: time = (time * 60) * 1000000000;
            break;
        case 1: time = (time * 60) * 1000000;
            break;
        case 2: time = (time * 60) * 1000;
            break;
        case 3: time = (time * 60);
            break;
        case 4: time = time;
            break;
        case 5: time = (time / 60);
            break;
        case 6: time = (time / 60) / 24;
            break;
        case 7: time = (time / 60) / 24 / 7;
            break;
        case 8: time = (time / 60) / 24 / 30;
            break;
        case 9: time = (time / 60) / 24 / 365;
            break;
        case 10: time = (time / 60) / 24 / 365 / 10;
            break;
        case 11: time = (time / 60) / 24 / 365 / 100;
            break;
    }
    return time;
}
//convert desired time to minutes
function convTimeToMinutes(time, mul) {
    switch (mul) {
        case 0: time = (time / 60) / 1000000000;
            break;
        case 1: time = (time / 60) / 1000000;
            break;
        case 2: time = (time / 60) / 1000;
            break;
        case 3: time = (time / 60);
            break;
        case 4: time = time;
            break;
        case 5: time = (time * 60);
            break;
        case 6: time = (time * 60) * 24;
            break;
        case 7: time = (time * 60) * 24 * 7;
            break;
        case 8: time = (time * 60) * 24 * 30;
            break;
        case 9: time = (time * 60) * 24 * 365;
            break;
        case 10: time = (time * 60) * 24 * 365 * 10;
            break;
        case 11: time = (time * 60) * 24 * 365 * 100;
            break;
    }
    return time;
}
