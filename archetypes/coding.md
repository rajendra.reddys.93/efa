---
title: {{ replace .Name "-" " " | title }}
date: {{ .Date }}
draft: true
weight: 1
summary: Brief summary
link: link-to-the-problem-source
difficulty: Easy
categories: [Arrays, Leetcode]
slug: two-sum
type: coding
source: Leetcode
---

**Insert Lead paragraph here.**

## Solution 1: Heading

```c
```

> **Time Complexity:** O(nlogn).
>
> **Auxiliary Space:** O(1).
