---
title: "Electronics"
summary: Basics, Micro-controllers, Micro-processors, PCB
date: 2021-04-29
publishdate: 2019-12-20
menu: main
weight: 1
slug: electronics
type: Hardware
---

Learn about Basic Electronics, Micro-controllers, Micro-processors & PCB Designing etc.
