﻿---
title: "Manage Libraries"
date: 2019-12-19T20:05:48+05:30
weight: 14
summary: Manage Libraries
chapter: Schematic
---

The Eagle Software is shipped with built-in libraries for Parts, Packages and Symbols. You can also choose which library you want to use in your project. This can be achieved by Managing Libraries in your project. To do that please follow below steps. You can add or remove the libraries that you want to use in your project in 2 ways.

## Managing Libraries from Control Panel

1. Open the project you want to work.

{{<img alt="Open Project" src="/images/Eagle/ManagingLibraries/ProjectOpen.PNG" cap="Open Project">}}

1. Right click on any libraries you want to use and select use/use all (in case multiple libraries).

{{<img alt="Use Libraries" src="/images/Eagle/ManagingLibraries/UseLib.png" cap="Use Libraries">}}

{{<img alt="Use Multiple Libraries" src="/images/Eagle/ManagingLibraries/UseAllLib.png" cap="Use Multiple Libraries">}}

3. Right click on any libraries you don’t want to use in your project deselect Use (if single library) or click Use None (in case multiple libraries).

{{<img alt="Remove Libraries" src="/images/Eagle/ManagingLibraries/DisableUseLib.png" cap="Remove Libraries">}}

{{<img alt="Remove Multiple Libraries" src="/images/Eagle/ManagingLibraries/UseNoneLib.png" cap="Remove Multiple Libraries">}}

1. By looking at the bubble next the library name you will be able to say whether it’s being used in the current project or not. If the bubble is green then the library in use. If the bubble is grey then the library is not in use.

{{<img alt="Libraries Not in Use" src="/images/Eagle/ManagingLibraries/LibNotInUse.PNG" cap="Libraries Not in Use">}}

{{<img alt="Libraries in Use" src="/images/Eagle/ManagingLibraries/LibInUse.PNG" cap="Libraries in Use">}}

You can also add or remove libraries from your schematic editor window.

## Managing Libraries from Inside Editor

1. Keep the schematic editor open.
2. Click `Library` menu >> select `Manage Libraries` or click on `Libraries Icon` in the top tool bar.

{{<img alt="Manage Libraries" src="/images/Eagle/ManagingLibraries/ManageLib.png" cap="Manage Libraries">}}

1. It will open Library Manager Window as below.

{{<img alt="In Use Libraries" src="/images/Eagle/ManagingLibraries/ManageLib1.png" cap="In Use Libraries">}}

{{<img alt="Available Libraries" src="/images/Eagle/ManagingLibraries/ManageLib2.png" cap="Available Libraries">}}

1. In the Manage Library Window, we can see two tabs named
   1. In Use – Libraries in use. You can either Remove or Update any library. 
   2. Available – Libraries which can be added to use in the present project. You can use Use, Update or Delete any library. It will show the libraries available not only in your local drive but also libraries from Eagle Repositories if you are Online and Only show local libraries is not checked. 
   
2. If you are working online (logged into you Autodesk Account in Eagle) it will automatically check for updates online and fetch them if any.
   
3. The refresh/update Icon indicates that the library need to update before use.

{{<img alt="Libraries Updates" src="/images/Eagle/ManagingLibraries/UpdateLib.PNG" cap="Libraries Updates">}}

1. In case if the library is not available locally when you click on use, Eagle will download, add it to Eagle libraries and will include them in present project.

{{<img alt="Download New Libraries" src="/images/Eagle/ManagingLibraries/ProjectOpen.PNG" cap="Download New Libraries">}}

## Updating Libraries

You can update the Libraries used in the project by selecting Update/Update All option in **Library** menu. If you choose to update a specific library choose Update. Update All will update all libraries in use. This function will be useful to load the modified libraries if the libraries are changed externally while the project is open.