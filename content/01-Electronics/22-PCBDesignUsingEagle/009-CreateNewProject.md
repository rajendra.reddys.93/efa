﻿---
title: "Create New Project"
date: 2019-12-19T20:05:48+05:30
weight: 9
summary: Starting up with new project
chapter: Schematic
---

As you can notice there are two folder under `Projects` entry in the `Control Panel`. 
Under `Examples` you will see the projects that shipped along with your Eagle 
package. By default all new projects created by user will be saved 
`/Documents/eagle` Folder. If you want to have your projects in a specific 
location on your hard drive, you can always do it by adding directories in 
`Eagle`. You can find more details about **adding directories in this Tutorial**. 
To create new project in Eagle, right click on the eagle folder and select 
`New Project`. You can enter Project Name and hit `Enter` key on your Keyboard or 
click away. 

{{<img alt="Create New Project" src="/images/Eagle/CreateNewProject/NewProject.png" cap="Create New Project">}}

You can notice a `Green` marker next to the project name which indicates that the 
project is opened. If you click on this marker it will turn to `Gray` colour, 
which indicates that the project is closed. You can alternately open and close 
a project by double clicking on the Project Name. You can also open / close a 
project by `Right Click` > `Open / Close Project` or from `File` menu.

{{<img alt="New Project" src="/images/Eagle/CreateNewProject/NewProject1.png" cap="New Project">}}

Eagle will create a separate folder in your hard drive for every project you 
create. Any files Related to this project will be placed only in this folder. 
In the status bar (at the bottom of the windows), you can notice the path which 
indicates your present working directory. If you navigate to that folder in your 
explorer, you can notice that a file by name `eagle.epf`, with icon **E** (change image) 
which is an `Eagle Project` file.

{{<img alt="Eagle Project File" src="/images/Eagle/CreateNewProject/EagleProjectFile.png" cap="Eagle Project File">}}

Settings related to your Eagle project will be saved in this file. For example 
current window settings for your project will be saved in this file before 
closing if autosave/backup is enabled. If you are curious you can view the 
content of this file by opening it in any text editor.

## Describe Your Project

You can write description of you project. Let us see how.

1. Select `Your project` >> `Right Click` >> select `Edit Description`.

{{<img alt="Edit Description" src="/images/Eagle/CreateNewProject/EditDescription.png" cap="Edit Description">}}

2. It’ll open an editor window as shown in below pic.

{{<img alt="Description Editor Window" src="/images/Eagle/CreateNewProject/Description.PNG" cap="Description Editor Window">}}

3. You can write brief description about your project in the editor. 
The Headline will be auto captured as you type in your description. 
After typing click `OK`. Now you can see the description in the right 
panel if you select you project in left panel.

{{<img alt="Adding Description" src="/images/Eagle/CreateNewProject/DescriptionAdded.png" cap="Adding Description">}}

{{<img alt="Project Description" src="/images/Eagle/CreateNewProject/DescriptionView.png" cap="Project Description">}}
