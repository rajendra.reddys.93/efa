﻿---
title: "Alias for Grid, Display and Window"
date: 2019-12-19T20:05:48+05:30
weight: 12
summary: Alias for Grid, Display and Window
chapter: Schematic
---

In Eagle you can define aliases for Grids, Display/Layers and Windows. You can 
define these aliases in any sheet, if you have multiple and these aliases are 
available in all sheets. But the alias you choose will be applied on the present 
sheet you are working on.

## Adding New Name

### Adding New Name to Grid

1. First choose the grid setting you want to give an alias by clicking on
   <b>Grid Icon</b>(change image). To know about the details about each
   parameter in Grid settings please follow this chapter.

1. Now Right click on the <b>Gird Icon</b> >> Select New.

{{<img alt="Grid Alias" src="/images/Eagle/Alias/Grid/GridAlias.png" cap="Grid Alias">}}

2. You will asked to enter the alias name, enter a name and click OK.

{{<img alt="Grid Alias Name" src="/images/Eagle/Alias/Grid/GridName.PNG" cap="Grid Alias Name">}}

3. If you right click on the <b>Grid Icon</b>, You can find the names you
   defined and you can select any one to affect to the present working sheet.

{{<img alt="Aliased Grid Name" src="/images/Eagle/Alias/Grid/GridNames.PNG" cap="Aliased Grid Name">}}

4. Clicking on Last will switch to previous settings.

### Adding New Name to Layers/Display

1. First choose the Layer setting you want to give an alias by clicking on 
   <b>Layers Icon</b>. To know about the details about each parameter in 
   Layer settings please follow this chapter.

2. Now Right click on the <b>Layers Icon</b> >> Select New.

{{<img alt="Layer Alias" src="/images/Eagle/Alias/Layers/Layers.png" cap="Layer Alias">}}

1. You will asked to enter the alias name, enter a name and click OK.

{{<img alt="Layer Alias Name" src="/images/Eagle/Alias/Layers/LayerName.PNG" cap="Layer Alias Name">}}

1. If you right click on the <b>Layer Icon</b>, You can find the names you
   defined and you can select any one to affect to the present working sheet.

{{<img alt="Aliased Layer Name" src="/images/Eagle/Alias/Layers/LayerNames.png" cap="Aliased Layer Name">}}

1. Clicking on Last will switch to previous settings.

2. You can also add, modify or remove the Layer aliases from the layer settings itself.

{{<img alt="Layer Settings" src="/images/Eagle/Alias/Layers/AlternateLayerAlias.png" cap="Layer Settings">}}

### Adding New Name to Grid

1. First choose the Windows Zoom Level you want to give an alias by clicking on
   <b>Zoom Select Icon</b>(change image), or any <b>Zoom Icons</b>.

2. Now Right click on the <b>Zoom Select Icon</b> >> Select New.

{{<img alt="Window Alias" src="/images/Eagle/Alias/Window/WindowAlias.png" cap="Window Alias">}}

1. You will asked to enter the alias name, enter a name and click OK.

{{<img alt="Window Alias Name" src="/images/Eagle/Alias/Window/WindowAliasName.PNG" cap="Window Alias Name">}}

4. If you right click on the <b>Zoom Select Icon</b>, You can find the names you
   defined and you can select any one to affect to the present working sheet.

{{<img alt="Aliased Window Name" src="/images/Eagle/Alias/Window/WindowAliasNames.png" cap="Aliased Window Name">}}

5. Clicking on Last will switch to previous settings.

## Edit Parameters of Alias

1. Right Click on an Alias Name and select Edit.

{{<img alt="Edit Alias" src="/images/Eagle/Alias/EditAlias/EditAlias.png" cap="Edit Alias">}}

2. Change the parameter values and click OK.

{{<img alt="Modify Alias Parameters" src="/images/Eagle/Alias/EditAlias/EditAliasParam.PNG" cap="Modify Alias Parameters">}}

## Edit Name of Alias

1. Right Click on an Alias Name and select Rename.

{{<img alt="Rename Alias" src="/images/Eagle/Alias/RenameAlias/EditName.png" cap="Rename Alias">}}

2. Change the Name and click OK.

{{<img alt="Modify Alias Name" src="/images/Eagle/Alias/RenameAlias/EditAliasName.PNG" cap="Modify Alias Name">}}

## Delete a Name

1. Right Click on Alias Name and click Delete.

{{<img alt="Delete Alias" src="/images/Eagle/Alias/DeleteAlias/DeleteAlias.png" cap="Delete Alias">}}

2. You can observe the modified alias list after deleting one.

{{<img alt="Alias List after Deletion" src="/images/Eagle/Alias/DeleteAlias/DeletedAlias.png" cap="Alias List after Deletion">}}
