﻿---
title: "Adding libraries"
date: 2019-12-19T20:05:48+05:30
weight: 4
summary: Adding 3rd party libraries in Eagle
chapter: Schematic
---

Eagle Software comes with an extensive set of predefined libraries with schematic symbols and layout parts. You can use these libraries in your projects just by adding it to the schematic or layout editor. The libraries are grouped into device types and manufacturer. The device groups consists of general parts such as resistors, capacitor, inductors, diodes with different packages and schematic symbols. The manufacturer groups will have the parts supplied by that manufacturer.

Along with these parts you can add other 3rd party libraries to Eagle. The 3rd party libraries are also available in Eagle Website in the below link. But SparkFun Libraries will be very helpful. You can download them in the below link.

### External Links

- 3rd Party Libraries on Eagle website: - [http://eagle.autodesk.com/eagle/libraries](http://eagle.autodesk.com/eagle/libraries)

- SparkFun Libraries for Eagle: - [https://github.com/sparkfun/SparkFun-Eagle-Libraries](https://github.com/sparkfun/SparkFun-Eagle-Libraries)

Now we will concentrate on adding these libraries to the use in Eagle. There are two ways we can achieve it. 

1. Copying the Libraries to Eagle Folder
2. Adding Directories in Eagle Software

We will look into them in detail.

## Copying the Libraries to Eagle Folder

1. After downloading the libraries which will be in zip format, unzip the files into a folder.

2. Copy the unzipped folder into `C:\EAGLE 8.2.2\lbr`. The Eagle Version may vary.

3. Go to `Eagle Control Panel`. Click `View` > `Refresh`. You can do the same just by hitting `F5`.

{{<img alt="Refresh after adding new Libraries" src="/images/Eagle/Libraries/Refresh.png" cap="Refresh after adding new Libraries">}}

4. Now you can notice one folder is added in to the `Libraries` under **`lbr`**.


{{<img alt="Added New Libraries" src="/images/Eagle/Libraries/AddedLib.png" cap="Added New Libraries">}}

## Adding Directories in Eagle Software

1. Place the downloaded libraries into a known folder, which you will not delete.

2. Copy the directory path where you placed the libraries in the previous step.

3. Open `Eagle Control Panel`.

4. Click on `Options` > `Directories`.

{{<img alt="Directories Options in Eagle" src="/images/Eagle/Libraries/Directories.png" cap="Directories Options in Eagle">}}

5. You are presented with the Directories Windows which looks as below.

{{<img alt="Directories Window" src="/images/Eagle/Libraries/DirectoriesEdit.PNG" cap="Directories Window">}}

6. You can paste the directory path you previously copied in step 2 in the text field against `Libraries`. You need to separate two path with a `;`(semicolon) as below. You can also click on `Browse` to select the directory path.

{{<img alt="After adding Directory Path" src="/images/Eagle/Libraries/AddedDirectoryPath.PNG" cap="After adding Directory Path">}}

7. Click `OK` and refresh the control panel by hitting `F5`. Now you can see the newly added libraries in your control panel.

## Using Libraries

To use libraries in your project, you need to mark them as `in use` by right clicking on the particular Library or folder containing it and selecting `use/use all`. You can verify whether any library is being used in current project just by checking the bubble next to the name of the library. If the bubble is **Green**, then that library is used. If the bubble is **Gray** then the library is not being used.

{{<img alt="Using Libraries in your Projects" src="/images/Eagle/Libraries/LibUse.png" cap="Using Libraries in your Projects">}}

You can also manage libraries of any project while adding part to you schematic page. We will discuss about this in next tutorial on ***adding parts to our project***.

