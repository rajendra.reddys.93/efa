﻿---
title: "Changing User Interface"
date: 2019-12-19T20:05:48+05:30
weight: 6
summary: User preferred interface settings
chapter: Schematic
---

Eagle allows the appearance of the editor windows for the layout, schematic diagram and library to be adjusted to your preferences. You can open the settings from `Control Panel` > `Options Menu` > `User Interface`. You can also access this menu from the Editor windows.

{{<img alt="User Interface Options Menu" src="/images/Eagle/UserInterface/UserInterface.png" cap="User Interface Options Menu">}}

In the `Controls` you can specify which objects are to be displayed in the editor window. If you deactivate all the Controls, only the command line will remain for entry. This maximizes the free area available for the drawing.  

{{<img alt="User Interface Options" src="/images/Eagle/UserInterface/UserInterfaceOpen.PNG" cap="User Interface Options">}}

The option `Always vector font` shows and prints texts with the built-in vector font, independently from the originally used font. Using the Vector font guarantees that the output with a printer or the CAM Processor is exactly the same as shown in the editor window. Fonts other than vector font depend on the systems' settings and cannot be controlled by Eagle. The output of non-vector fonts may differ from the editor's view. Opening the `User Interface` dialog from one of the Editor windows (for example, the Layout Editor) the Always vector font option offers an additional item Persistent in this drawing. Setting this option causes Eagle to save the Always vector font setting in the current drawing file. So you can be quite sure that the layout will be shown with vector font at another's person computer (for example, at a board house).

`Limit zoom factor` limits the maximum zoom factor in an editor window. At maximum zoom level the width of the drawing is about one Millimetre (approx. 40 mil). Switching off this option allows you to zoom until the 0.003125 Micron grid. If you are working with a wheel mouse, you can zoom in and out by turning the mouse wheel. Mouse wheel zoom determines the zoom factor. The value 0 switches this function off. The wheel is used for scrolling then.

The field `External text editor` allows you to specify an alternative for the built-in EAGLE text editor. Further details on this can be found in the help function in the section **Editor windows/Text editor**. 

The background colour and the appearance of the drawing cursor can be separately adjusted for the layout and the schematic diagram editors. The background may be black, white or shown in any other colour (Coloured). The background colour definition can be changed from the Editor Window, where you have flexibility to use your custom colour. Go-to `Options` > `Set`. 

The `Cursor` can be displayed optionally as small cross or as large cross-hairs. 

The section `Vertical text` lets you decide whether text should be readable from the right hand side upwards (Up) or from the left hand side downwards (Down) in your drawings. 

`Icon size` can be used for scaling the icons. The value is in pixels. 

Selecting the `User guidance` check box displays additional information about the selected object, like the net or signal name, the net class, or the part's name and value (with NET, MOVE, ROUTE, SHOW...), instructions about the possible mouse actions in the status bar of the editor window.
