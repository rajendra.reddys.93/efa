﻿---
title: "Tools & Options in Eagle Schematic Editor"
date: 2019-12-19T20:05:48+05:30
weight: 15
summary: Tools & Options in Eagle Schematic Editor
chapter: Schematic
---

Will explain each and every tool in schematic editor.

1. File Menu
    1. New
    2. Open
    3. Open Recent
    4. Save
    5. Save As
    6. Save As Eale 7.x
    7. Save As Design Block
    8. Save Section As Design Block
    9. Print Setup
    10. Print
    11. CAM Processor
    12. Switch Board
    13. Import
        1. Ealgle Drawing
        2. P-CAD/Altium/Protel
        3. Bitmap
        4. DXF

    14. Export
        1. Netlist
        2. Partlist
        3. Pinlist
        4. NetScript
        5. Image
        6. BOM
        7. DXF
        8. Libraries

    15. Execute Script
    16. Run ULP
    17. Close
    18. Exit

2. Edit Menu
    1. Undo
    2. Redo
    3. Undo/Redo List
    4. Stop Command
    5. Add
    6. Change
    7. Copy
    8. Delete
    9. GateSwap
    10. Group
    11. Invoke
    12. Mirror
    13. Milter
    14. Move
    15. Name
    16. Optimize
    17. Paste
    18. PinSwap
    19. Replace
    20. Rotate
    21. Slice
    22. Smash
    23. Split
    24. Value
    25. Global Attributes
    26. Net Classes
    27. Assembly Variants
    28. Description
    29. Design Block Properties

3. Draw Menu
    1. Arc
    2. Attribute
    3. Bus
    4. Circle
    5. Dimension
    6. Frame
    7. Junction
    8. Label
    9. Line
    10. Module
    11. Net
    12. Polygon
    13. Port
    14. Rectangle
    15. Text

4. View Menu
    1. Grid
    2. Layer Settings
    3. Mark
    4. Show
    5. Info
    6. Redraw
    7. Zoom To Fit
    8. Zoom In
    9. Zoom Out
    10. Zoom Select

5. Tools Menu
    1. CRC
    2. Errors
    3. Renumber Parts
    4. Search
    5. Snap On Grids
    6. Statistics

6. Library Menu
    1. Manage Libraries
    2. Open
    3. Update
    4. Update All

7. Options Menu
    1. Assign
    2. Set
    3. User Interface

8. Window Menu

9. Help Menu
    1. General
    2. Context
    3. Schematic Editor
    4. About Eagle
