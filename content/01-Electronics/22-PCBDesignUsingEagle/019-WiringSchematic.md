﻿---
title: "Wiring Schematic - Draw Nets"
date: 2019-12-19T20:05:48+05:30
weight: 19
summary: Wiring Schematic - Draw Nets
chapter: Schematic
---

The components are connected through wires with each other which
defines the physical connection between them. The connections are
done by activating NET command.

## Draw Nets

Nets begins and ends at the connection of pins, which can be
displayed when layer 93 made visible. To draw a net click NET
icon but not LINE icon. You can also enable it by NET command.
When NET command is active, and if you move the mouse cursor near
any Pin you notice the tip of the Pin attaches to green circles to
it. On click it will start to draw a green line which is a net.

{{<img alt="Wiring Nets" src="/images/Eagle/Wiring/DrawNets/PinBubble.png" cap="Wiring Nets">}}

Click on the other pin will end that Net by connecting two pins.

{{<img alt="Net" src="/images/Eagle/Wiring/DrawNets/Net.PNG" cap="Net">}}

While NET command is active, you will get below options on the top.

{{<img alt="Net Options" src="/images/Eagle/Wiring/DrawNets/NetOptions.PNG" cap="Net Options">}}

Below is the explanation of each option.

1. Different wire bend styles ***(Add Image)***. Right mouse click while
drawing NET can switch among them.

2. Corner radius of the wire ***(Add Image)***.

3. Wire edge types ***(Add Image)***.

4. Wire styles ***(Add Image)***. Continuous, long
dash, short dash and dash dot.

5. Net classes ***(Add Image)***.

You can choose among different styles while adding electrical
connections. By default the Eagle will provide only 1 NET class,
but you can define up-to 16 NET classes using CLASS command. We
will discuss about NET classes later in the coming section.

By default all Nets are given a default name. All nets with the
same name are connected together irrespective on which sheets they
are drawn and whether drawn continuously or not. You can change
the Net name using Name Command ***(Add Image)***. If you
are connecting different nets Eagle will ask you to select the
resulting Net.

{{<img alt="Merge Nets" src="/images/Eagle/Wiring/DrawNets/MergeNets.PNG" cap="Merge Nets">}}

{{<img alt="Net Name Conflict" src="/images/Eagle/Wiring/DrawNets/NetConflict.PNG" cap="Net Name Conflict">}}

## Note

1. Never Copy Nets, as copied Net will have same name as original Net.

2. Never Move Nets, you will end up with floating connections.

To verify which pins and nets are connected to each other, use SHOW
command or ***(Add Image)***tool. While SHOW tool active,
click on any net it will highlight all the connected nets. In the
below picture you can notice that, though there is no wired
connection between two nets, when you use SHOW Command it will
indicate as connected.

{{<img alt="SHOW Command" src="/images/Eagle/Wiring/DrawNets/ShowCommand.PNG" cap="SHOW Command">}}
