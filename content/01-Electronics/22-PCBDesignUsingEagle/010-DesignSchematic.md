﻿---
title: "Designing First Schematic"
date: 2019-12-19T20:05:48+05:30
weight: 10
summary: Designing First Schematic
chapter: Schematic
---

In the previous sections we have learnt how to create new project, now let 
us learn how to create new schematic and different tools available to 
design a schematic.

## Creating a New Schematic

To create new schematic,

1. Open the Project if it is not opened by clicking on the grey dot next to project name.

{{<img alt="New Schematic" src="/images/Eagle/DesigningSchematic/CreateSchematic/NewSchematic.png" cap="New Schematic">}}

2. Right click on the project >> select New >> choose Schematic. It’ll open the schematic editor.

> **Note :-**
>
> 1. Your Schematic will not be saved until and unless you save it. 
> As soon as you create new schematic first step is to save it by 
> hitting Ctrl + S or File >> Save. Observe the title bar before and 
> after saving the schematic.
>
> 2. All files Related to a project will be saved under a directory with the project name.
>
> 3. Make sure you save the project before closing.

## Open the Schematic Diagram

To open previously created schematic double click on the schematic file or Right Click >> Open.

## Set the Grid

Grids plays vital role in PCB Designing. While designing a schematic it’s 
not necessary to alter the grid size. We can always go with default settings. 
However if you want to change please follow the below steps.

1. You can open the grid setting by clicking the **G**(change image) 
icon in the top tool bar. It opens the grid setting which looks as below.

{{<img alt="Grid Settings" src="/images/Eagle/DesigningSchematic/CreateSchematic/GridSettings.PNG" cap="Grid Settings">}}

We will explore each and every settings in detail now.

- **Display:** You can choose to either Turn On or Off to
display the grids. By default the grid is not displayed, but all
the components you placed are snapped to the grid.

- **Style:** The grids can be displayed either as lines
**L**(change image) or dots **D**(change image).

- **Size:** This represents the size of each cell in grid.
You can also specify the unit. For example inch, mm, mil.
If you click on Finest button it will take the least possible
value of grid size.

{{<img alt="Grid Units" src="/images/Eagle/DesigningSchematic/CreateSchematic/GridUnits.png" cap="Grid Units">}}

- **Alt:** This is alternate/secondary grid size. To access
this grid size while placing parts hold Alt key.

- **Default:** Hitting this key will restore default grid settings.
 
> **Warning :-**
>
> While drawing schematic don’t alter the default grid setting. Since all 
> libraries in the Eagle are designed using default grid size, you may face 
> difficulty while adding these library parts in your schematic if you alter 
> the grid size.
