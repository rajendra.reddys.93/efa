﻿---
title: "Explore Control Panel"
date: 2019-12-19T20:05:48+05:30
weight: 3
summary: Explore different options in control panel
chapter: Schematic
---

If you are opening the Eagle for the first time, you are asked to login to the Autodesk Account. You will be able to use the software only after logging in. You can find the step by step guide to login or create account in Autodesk account. 

## First Time Setup</h2>

- Open Eagle by double clicking on the shortcut on the desktop or selecting the package from the start menu. You’ll be presented with the below screen.

{{<img alt="First Time Sign-in" src="/images/Eagle/Explore/Login.png" cap="First Time Sign-in">}}

- If you have account in Autodesk, you can login by entering Username or Email and your Autodesk Password.

{{<img alt="Autodesk Login" src="/images/Eagle/Explore/Login2.png" cap="Autodesk Login">}}

- If you don’t have account, you can create by clicking on the Create Account from the main screen. Then you will be directed to create new Autodesk Account. Just fill in the details and click create account. After creating new account you can login to the Autodesk Account from main screen.

{{<img alt="Create Autodesk Account" src="/images/Eagle/Explore/Signup.png" cap="Create Autodesk Account">}}

- After successful login, the main application will open which is called as the control panel of Eagle. This is base for all other Eagle modules such as Schematic, Layout Editor, and Package Creator.

{{<img alt="Eagle Control Panel" src="/images/Eagle/Explore/ControlPanel.png" cap="Eagle Control Panel">}}

## Eagle Control Panel

The control panel is the first window that appears after starting the Eagle every time. You can perform the files Related operations (new, copy, cut and paste) on files specific to Eagle. The tree structure on the left side provides a quick overview of the Libraries, Design Blocks, Documentation, and Design Rules, User Language programs, script files, CAM jobs and projects. Special libraries, text, manufacturing and documentation files can belong to a project as well as schematic diagrams and layouts. If an object is selected in the tree view, further relevant information or a preview is displayed in the right hand part of the window.

{{<img alt="Preview of object in Eagle Control Panel" src="/images/Eagle/Explore/ControlPanelPreview.png" cap="Preview of object in Eagle Control Panel">}}

We will discuss about the different sections in the control panel tree view in brief.

### Documentation

The Documentation branch allows direct access to the EAGLE tutorial and manual available in different languages. Additionally, there can be found the UPDATE.txt file and documentation files of some of the User Language programs. Double-click opens the file with the default PDF reader or text editor.

{{<img alt="Documentation" src="/images/Eagle/Explore/Documentation.png" cap="Documentation">}}

### Libraries

Libraries (LBR) contains the parts (schematic symbols and layout footprints). The libraries branch has two sub branches. The Managed Libraries branch contains the all component libraries supplied with EAGLE. There is a huge collection of parts available as part of Eagle. That means you need not to worry about creating a custom package for your new part, you can use it directly in your schematic and layout if available in Eagle libraries.

{{<img alt="Libraries" src="/images/Eagle/Explore/Libraries.png" cap="Libraries">}}

On expanding Managed Libraries entry, in the description field you can find the brief description about the library. If you select a library you can see more information about the library in the right hand panel. Now select any Device from the library: The description of the Device and a graphical representation of it appear on the right. The available Package and technology variants are listed. If you click onto one of the Package versions, the Package preview shown above will change.

{{<img alt="Part Description, Symbol & Package" src="/images/Eagle/Explore/Libraries1.png" cap="Part Description, Symbol & Package">}}

{{<img alt="art Description, Symbol & Different Package" src="/images/Eagle/Explore/Libraries2.png" cap="art Description, Symbol & Different Package">}}

The libraries are commonly a collection of similar parts grouped together. For example, the battery.lbr have collection of different batteries used in the boards, and 751xx.lbr stores a collections of 75xxx series devices. You can add third party libraries (such as sparkfun) to the Eagle. **Please follow the below tutorial**.

### Design Blocks

Design Blocks (DBL) ideally contain a consistent Schematic and Layout pair that can be easily (re-) used in any project. For example, an oscillator circuitry, which is being used in all circuits consists of micro-controller. Design Blocks can have a Description and Attributes for getting information about the intent of the Design Block. A right mouse click onto a Design Block entry opens a context menu that allows to Open, Rename, Copy, Delete a Design Block or directly add it to a Schematic. You can create custom design block in Eagle. **Please follow below tutorial**.

{{<img alt="Design Blocks" src="/images/Eagle/Explore/DesignBlock.png" cap="Design Blocks">}}

### Design Rules

Design rules are a set of rules that your board design must meet before you can send it for the fabrication. The layout will be checked by the DRC in accordance with these criteria. In this tree you’ll find Design Rule (DRU) files, which are pre-defined set of rules. You can define your own Design Rules and use them in other projects. Please follow below tutorial.

### User Language Program

User Language Programs (ULP) are the scripts written using Eagle’s User Language which is a plain text file and written in a C­like syntax. These scripts are useful when performing a set of actions regularly. We can write all such actions in a single ULP, and by executing that ULP will in-turn will all actions for you. The Eagle comes with lot of pre-built ULPs which includes creating BOM (bom.ulp), import a bitmap image to layout (import-bmp.ulp) and many more. You can create your own ULPs and use them in your projects. 
**Please follow below tutorial.**

{{<img alt="User Language Program" src="/images/Eagle/Explore/ULP.png" cap="User Language Program">}}

### Scripts

These scripts (SCR) files are similar to as that of ULPs. The scripts can be used to modify the Eagle user interface in single click, assign default colours and etc.

{{<img alt="Scripts" src="/images/Eagle/Explore/Scripts.png" cap="Scripts">}}

### CAM Jobs

Cam (CAM) jobs are used by the CAM Processor (Gerber generation tool) to generate the final Gerber files for fabrication process.

{{<img alt="CAM Jobs" src="/images/Eagle/Explore/CAMJobs.png" cap="CAM Jobs">}}

### Projects

This is where you can organise and manage all your projects under single folder. A project usually consists of a folder. The folder usually contains all files that belong to your project, for example, schematic and board file, special library files, script files and so on. 

The project to be edited is selected in the Projects branch. On the right of the project's name you will find a marker which is either grey or green. With the help of this marker one can open or close projects. Clicking onto a grey marker, loads the project. The marker appears green now. Clicking onto the green marker again or clicking onto another grey marker closes the current project respectively opens another project after closing the current one. This way one can switch easily from one project to another. As an alternative you can open or close a project by double-clicking onto the entry in the tree view or by pressing the Space or Enter key.

{{<img alt="Projects" src="/images/Eagle/Explore/Projects.png" cap="Projects">}}

New projects are created by clicking the right mouse button onto a folder entry in this branch. A context menu opens which permits new files and directories to be created and the individual projects to be managed. Selecting the option New Project invokes a new folder which has to be given the project's title. The project file eagle.epf will be created automatically. You can also use the File/Open/Project or the File/New/Project menu to open or create a new project.

{{<img alt="New Project" src="/images/Eagle/Explore/NewProject.png" cap="New Project">}}
