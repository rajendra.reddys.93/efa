﻿---
title: "Downloading & Installing"
date: 2019-12-19T20:05:48+05:30
weight: 2
summary: How to download and install Autodesk Eagle
chapter: Schematic
---

As we already discussed in previous sections, Eagle is Free/Less cost software. The Free version of the Eagle Software can be downloaded directly from the website. The Paid version of the software can be purchased through online if you are living in US or Canada, else you need to find retailer in you locality using Partner Finder Tool of Autodesk.

You can find the complete details about the comparison between different versions of the Software.

{{<rawhtml>}}
    <table class="centered highlight responsive-table">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="5">Eagle Subscription Comparison Chart</th>
            </tr>
            <tr>
                <th style="text-align:center;">Parameter</th>
                <th style="text-align:center;">Eagle Free</th>
                <th style="text-align:center;">Eagle Standard</th>
                <th style="text-align:center;">Eagle Premium</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-style:italic;">Schematic sheets</td>
                <td>2 Schematic Sheets</td>
                <td>99 Schematic Sheets</td>
                <td>999 Schematic Sheets</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Board area</td>
                <td>80 cm<sup>2</sup></td>
                <td>160 cm<sup>2</sup></td>
                <td>Unlimited</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Typical board size</td>
                <td>80mm x 100mm <br />(3.2 x 3.9 inches)</td>
                <td>160mm x 100mm <br />(6.3 x 3.9 inches)</td>
                <td>150 x 150 inches</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Maximum no of signal layers</td>
                <td>2 Signal Layers</td>
                <td>4 Signal Layers</td>
                <td>16 Signal Layers</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Best Suited For</td>
                <td>Individuals, Students & Professionals</td>
                <td>-</td>
                <td>-</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Price</td>
                <td>Free</td>
                <td>15$ - Monthly<br />100$ - 1 Year<br />200$ - 2 Year<br />300$ - 3 Year</td>
                <td>65$ - Monthly<br />500$ - 1 Year<br />1000$ - 2 Year<br />1500$ - 3 Year</td>
            </tr>
        </tbody>
    </table>
{{</rawhtml>}}

Along with above 3 pricing models Eagle has special package for Educators & Students, where  they can get complete benefit of the software for 3 years, free of cost. However the usage is limited to education purpose only. Please read the Terms & Conditions before downloading free and Educator editions of the software.

### External Links

- Autodesk Eagle Subscriptions Page: - [https://www.autodesk.com/products/eagle/subscribe](https://www.autodesk.com/products/eagle/subscribe)

## A Word Before Downloading Free Version

1. Though the software is available for free, its terms and conditions states as below. Please read it thoroughly before downloading the free version. You can find the details in the below link given at the end of this section.

**The free download is a Personal Learning License that may be used by individuals for personal, non-commercial use. Free Autodesk software licenses and/or cloud-based services are subject to acceptance of and compliance with the terms and conditions of the license agreement or terms of service, as applicable that accompany such software or cloud-based services. Usage is subject to such terms and conditions for as long as you use the software or until such terms and conditions change.**

{{<img alt="Terms & Conditions" src="/images/Eagle/DownloadAndInstallation/TermsAndConditions.PNG" cap="Terms & Conditions">}}

2. You need to have Autodesk Account to activate (after installing) your free version of the software. You can create one, by going to the below link. Its free account and you can use your e-mail ID to create an account in Autodesk.

{{<img alt="Create Autodesk Account" src="/images/Eagle//DownloadAndInstallation/CreateAutodeskAccount.PNG" cap="Create Autodesk Account">}}

### External Links

- Terms & Conditions: - [https://www.autodesk.com/products/eagle/free-download](https://www.autodesk.com/products/eagle/free-download)

- Create Autodesk Account: - [https://accounts.autodesk.com/register](https://accounts.autodesk.com/register)

## System Requirements for Autodesk Eagle

As we already explained, the Eagle software is light weight. The setup of Eagle 8.1.0 is around 85MB, 1GB of your drive space is more than sufficient to install this packages. Below are the other requirements as specified by the Autodesk on their website specification page. You can find the specifications in the below given link.

### External Links

- System Requirements: - [https://knowledge.autodesk.com/support/eagle/learn-explore/caas/sfdcarticles/sfdcarticles/System-requirements-for-Autodesk-EAGLE.html](https://knowledge.autodesk.com/support/eagle/learn-explore/caas/sfdcarticles/sfdcarticles/System-requirements-for-Autodesk-EAGLE.html)


{{<rawhtml>}}
    <table class="centered highlight responsive-table">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="5">System Requirements for Autodesk EAGLE</th>
            </tr>
            <tr>
                <th style="text-align:center;">Operating System</th>
                <th style="text-align:center;">Requirements</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-style:italic;">Schematic sheets</td>
                <td>2 Schematic Sheets</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Windows</td>
                <td>a.	Microsoft® Windows® 7 or newer is required.
                <br />b.	EAGLE 64-bit requires a 64-bit operating system.</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Linux</td>
                <td>a.	Linux® based on kernel 2.6 for Intel computers, X11 with a minimum 
                colour depth of 8 bpp, the following runtime libraries: libssl.so.1.0.0, 
                libcrypto.so.1.0.0, and CUPS for printing.
                <br />b.	For the 64-bit version EAGLE requires a 64 bit-operating system 
                and libc.so.6 with sub version GLIBC_2.14 or higher.</td>
            </tr>
            <tr>
                <td style="font-style:italic;">Mac</td>
                <td>Apple® Mac OS® X version 10.10 or above for Intel computers</td>
            </tr>
            <tr>
                <td style="font-style:italic;">All Operating Systems</td>
                <td>•	A minimum of 3 MB of memory
                    <br />•	About 700 MB free disk space
                    <br />•	A minimum graphics resolution of 1024x768 pixels
                    <br />•	Preferably a 3-button wheel mouse
                </td>
            </tr>
        </tbody>
    </table>
{{</rawhtml>}}

{{<img alt="Minimum System Requirements for Autodesk EAGLE" src="/images/Eagle/DownloadAndInstallation/SystemRequirements.PNG" cap="Minimum System Requirements for Autodesk EAGLE">}}

## Download & Install

### Download

You can always download latest version of the Eagle Software from the Autodesk Eagle product  page. You can find the link below to the download page. Eagle is available for Windows, Linux and Mac OS-X in 64 bit packages. On selecting the appropriate Operating System your package will download to your system.

### Install

Installation is as simple as any other software. In case if you find any difficulties in installing, follow below steps to installing the software on Windows OS.

1. Locate the downloaded package in your drive, Select >> Right Click >> Run as Administrator.

{{<img alt="Installaion Step 1" src="/images/Eagle/DownloadAndInstallation/Install1.png" cap="Installaion Step 1">}}

2. Click YES! If the system asks for any administrator permission.

3. Read and accept the licence agreement and click Next >.

{{<img alt="Installaion Step 3" src="/images/Eagle/DownloadAndInstallation/Install2.PNG" cap="Installaion Step 3">}}

4. In Select Addition Tasks, select Create a desktop shortcut and click Next >.

{{<img alt="Installaion Step 4" src="/images/Eagle/DownloadAndInstallation/Install3.PNG" cap="Installaion Step 4">}}

5. Click Install.

{{<img alt="Installaion Step 5" src="/images/Eagle/DownloadAndInstallation/Install4.PNG" cap="Installaion Step 5">}}

6. It will install all necessary files required.

{{<img alt="Installaion Step 6" src="/images/Eagle/DownloadAndInstallation/Install5.PNG" cap="Installaion Step 6">}}

7. After successful completion, click Finish. If Launch Eagle is enabled it will start the application. You can always find a shortcut to the software on desktop and your start menu.

{{<img alt="Installaion Step 7" src="/images/Eagle/DownloadAndInstallation/Install6.PNG" cap="Installaion Step 7">}}
