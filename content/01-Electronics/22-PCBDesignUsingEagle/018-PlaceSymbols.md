﻿---
title: "Place Symbols"
date: 2019-12-19T20:05:48+05:30
weight: 18
summary: Place Symbols
chapter: Schematic
---

The Fun starts now. Here we are going to learn how to add components and
connecting them to form circuits. Before we start, make sure your schematic page
grid is set to default that is 0.1 inch or 2.54 mm. If you struggling to set the
grid, **check section 1.2.43 grid settings.**

## Load Drawing Frame

Let us start by adding a frame. To add any part on to your schematic sheets,
you need to have necessary libraries added to your project. You can add
libraries in several ways. Learn how to add libraries to you project in the
**section 1.2.3.**

1. Now click on **Add** icon.

2. You will get a window with all libraries in use. You can add or
remove libraries by clicking Manage Libraries at the bottom.

{{<img alt="Adding Symbols to Schematic" src="/images/Eagle/PlaceSymbols/Add.PNG" cap="Adding Symbols to Schematic">}}

3. Search for A4 and select A4L-LOC Frame and click OK.

{{<img alt="Searching for Parts" src="/images/Eagle/PlaceSymbols/AddSearch.PNG" cap="Searching for Parts">}}

4. Click anywhere (preferably at origin – (0, 0)) in your schematic sheet.
You will notice the Frame is placed.

{{<img alt="Frame" src="/images/Eagle/PlaceSymbols/Frame.png" cap="Frame">}}

5. Click ESC twice, it will end Add command.

6. You can also add empty frame by using frame command.

7. Till now we added the Frame to our schematic sheet. This is the outer border for
our schematic. You place other parts and wire them up inside this frame. There are
different frames sizes. If you search for Frame in the window invoked by add
command, you will find all frames available.

{{<img alt="Available Frames List" src="/images/Eagle/PlaceSymbols/FrameList.PNG" cap="Available Frames List">}}

## Add New Schematic Sheet

To add new schematic sheet:

1. Right Click on Existing Sheet >> Select New.

{{<img alt="Adding New Schematic Sheet" src="/images/Eagle/PlaceSymbols/AddNewSheet.png" cap="Adding New Schematic Sheet">}}

{{<img alt="Describe Schematic Sheet" src="/images/Eagle/PlaceSymbols/RemoveSheet.png" cap="Describe Schematic Sheet">}}

2. You can remove a schematic sheet on clicking Remove, describe it
using Description.

3. Now add the frame to your second sheet also.

## Place Circuit Symbols

So, you are familiar with adding part to the schematic. Below is the circuit we
are going to build. You can find this circuit in CP2102 Data Sheet. Here in our
circuit we are not using any flow control signals. We are using only TXD, RDX and
nRST signals.

{{<img alt="Complete Circuit" src="/images/Eagle/PlaceSymbols/FinalCircuit.PNG" cap="Complete Circuit">}}

It is simple circuit, which can be accommodate in single sheet. However you can
have multiple sheets in your schematic. You can follow steps in section to add new
sheet to your schematic.

Let us add all other components required for our circuit. You can find list of all
components in below table.

## Help in Adding Parts

1. You can search for parts using Devices Names, Device Description and
Attributes such as names and values. Wild cards such as * and ? are
allowed. Multiple search keys separated by space is also allowed.

2. Disable the Pads, Smds and Description will filter out the parts.
Disabling Preview will hide the component preview window.

{{<img alt="Hide Preview" src="/images/Eagle/PlaceSymbols/HidePreview.PNG" cap="Hide Preview">}}

3. You can place components clicking **Add** icon. Alternatively
type ‘add’ in command bar (on top of your schematic) and hit enter.
It does the job for you.

{{<img alt="Command Bar" src="/images/Eagle/PlaceSymbols/CommandBar.png" cap="Command Bar">}}

4. Once you place a component, press ESC it will bring up ADD window with
previously used part. Now can enter new part and place all parts
sequentially without invoking ADD command for each part.

5. To reuse a recently used components which happens with the passive
components Right Click on **ADD** icon which gives a list of
recently used components. You can select one and place it. In the
backend it does invoke the ADD command, select the component and place.

{{<img alt="Recently Used Parts" src="/images/Eagle/PlaceSymbols/RecentParts.png" cap="Recently Used Parts">}}

6. To move a component, click (origin of the component) and drag to the
new position. You can find a + symbol in each component which is the
origin of that component. In the below image the + sign indicates
Positive pin of polarised capacitor. But origin + is submerged in
the symbol, if you look closer you will find it.

{{<img alt="Component Origin" src="/images/Eagle/PlaceSymbols/Origin.PNG" cap="Component Origin">}}

7. To move block of components, select them and moving dragging holding
on to origin of any component.

{{<img alt="Block Move" src="/images/Eagle/PlaceSymbols/BlockMove.png" cap="Block Move">}}

8. To rotate any component Right Click on the component and select Rotate.
Or select **Rotate** icon.

{{<img alt="Rotate" src="/images/Eagle/PlaceSymbols/Rotate.png" cap="Rotate">}}

## Note

1. Pressing ESC will end the current command and selects default Select command in most of the cases.

2. This is how it looks the sheet after adding all components. Notice that we have placed the components randomly. To wire up the circuit we need to rearrange them.

{{<img alt="Placed Components" src="/images/Eagle/PlaceSymbols/PlacedComponents.PNG" cap="Placed Components">}}

3. Let us arrange the components to make the wiring simple. This is how it looks.

{{<img alt="Arrange Components to Ease the Wiring" src="/images/Eagle/PlaceSymbols/ArrangedComponents.PNG" cap="Arrange Components to Ease the Wiring">}}
