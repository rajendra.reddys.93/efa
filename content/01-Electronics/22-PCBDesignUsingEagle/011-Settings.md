﻿---
title: "Color, DRC, Drill & Misc Settings"
date: 2019-12-19T20:05:48+05:30
weight: 11
summary: Color, DRC, Drill & Misc Settings
chapter: Schematic
---

## Color Settings

In `Eagle Schematic Editor` you can switch between 3 different themes for your 
editor window. And these themes are customisable. You can set your own colours. 
Let us see how to do it.

1. Open `Schematic Editor`, by opening recently created project from the control panel.

2. Click `Options` menu >> select `Set...`

{{<img alt="Color Settings" src="/images/Eagle/ColorSettings/ColorSettings.png" cap="Color Settings">}}

3. It will open settings window as below.

{{<img alt="Color Settings Window" src="/images/Eagle/ColorSettings/ColorSettingsWindow.png" cap="Color Settings Window">}}

There you can see 3 different Themes named – Black background, 
White background and Colored background. You can change the 
colours of Palette & Grid from here. You can switch between these 
themes from the `Options` menu >> `User Interface`. Though we can assign 
new colours, recommend you not to change and use the default colours. 
Don’t mess up.

## DRC Settings

Working on it...

## Drill Settings

Working on it...

## Miscellanious Settings

Switch to Misc Settings tab.

{{<img alt="Miscellaneous Settings" src="/images/Eagle/ColorSettings/MiscSettingsWindow.png" cap="Miscellaneous Settings">}}

Below is explanation of each and every parameter.

- **Beep:** Switches on/off the confirmation beep.
  *Default:* `on`.

- **Check connects:** Activates the package check while placing parts in the schematic. 
  *Default:* `on`.

- **Undo:** Switches on/off the undo/redo buffer of the current editor 
  window. In case you are working with a consistent schematic/layout 
  pair, this setting is valid for both editor windows.
  *Default:* `on`.

- **Optimizing:** Enables the automatic removal of bends in straight lines. 
  *Default:* `on`.

- **Ratsnest processes polygons:** The contents of polygons will be calculated 
  with the RATSNEST command. 
  *Default:* `on`.

- **Display pad names:** Pad names are displayed in the Layout or 
  Package Editor.
  *Default:* `off`.

- **Display signal names on pads:** Signal names are displayed on 
  pads in the Layout or Package Editor.
  *Default:* `on`.

- **Display signal names on traces:** The traces in the layout show 
  their signal names.
  *Default:* `on`.

- **Display via lengths:** The length of a via (start layer – end 
  layer) will be displayed in the Layout Editor. 
  *Default:* `off`.

- **Display drills:** Pads/Vias are shown with a drill hole 
  or without it.
  *Default:* `on`.

- **Auto end net and bus:** If placing a net on a pin or a bus the 
  net drops from the mouse cursor.
  *Default:* `on`.

- **Auto set junction:** Ending a net on another net a junction will 
  be set automatically.
  *Default:* `on`.

- **Auto set route width and drill:** If this option is active, the 
  Follow-me-Router uses the values for wire width and via drill diameter 
  given by the Design Rules or the net classes for the tracks. These 
  values will be set automatically as soon as you are clicking onto a 
  signal wire. If this option is switched off, Eagle will take the value 
  you have set with, for example, the previous CHANGE WIDTH command. 

- **Group command default on:** If no command is active in the 
  Layout editor, you can select a group as default mouse action 
  *Default:* `off`.

- **Min. visible text size:** Only texts with the given minimum 
  size are displayed. *Default:* `3 pixels`.

- **Min. visible grid size:** Grid lines/dots which are closer 
  than the given minimum distance are no longer displayed on the 
  screen. *Default:* `5 pixels`.

- **Catch factor:** Within this radius a mouse click can reach 
  objects. Set the value to 0 in order to switch this limitation off. 
  So you can reach even objects that are placed far beyond the area of 
  the currently displayed window. *Default:* `5%` of the height of 
  the current display window.

- **Select factor:** Within this radius (given in % of the height 
  of the current drawing window) EAGLE offers objects for selection. 
  *Default:* `2%`.

- **Snap length:** Defines the radius of the magnetic-pads function 
  of pads and SMDs. If you are laying tracks with the ROUTE command and 
  approach a pad or a SMD beyond the given value – that is to say the 
  dynamically calculated air-wire becomes shorter than the given 
  radius – the wire will be snapped to the pads/SMDs centre. 
  *Default:* `20 mil`.
