﻿---
title: "Backup, Auto-Save & Locking Options"
date: 2019-12-19 T20:05:48 +05:30
weight: 5
notoc: true
summary: Backup, Auto-Save & Locking Options
chapter: Schematic
---

You can set the `Backup` parameters in the `Options` menu of the `Control Panel`. While saving the files, Eagle also creates backup copies of the previous files. The maximum number of backups allowed can be configured, 9 by default. Backup files have different file extensions, enumerated sequentially. Schematic files receive the ending s#x, board files b#x, and library files l#x. x can run from 1 to 9. The file with x = 1 is the newest.

{{<img alt="Backup Menu" src="/images/Eagle/Backup/Backup.png" cap="Backup Menu">}}

We can also set the backup interval. The automatic backup function permits the backup to be scheduled. The time-interval can be between 1 and 60 minutes and default is 5 minutes. The backup files have the endings b##, s## and l## respectively. All these backup files can be further processed in Eagle if they are renamed and given the usual file endings (brd, sch, lbr).

{{<img alt="Backup Options" src="/images/Eagle/Backup/BackupOptions.PNG" cap="Backup Options">}}

