﻿---
title: "Adding Shortcuts in Eagle"
date: 2019-12-19T20:05:48+05:30
weight: 17
summary: Adding Shortcuts in Eagle
chapter: Schematic
---

In Eagle you can define your own shortcuts for any commands. For example you can set Ctrl+D as shortcut to delete a part in schematic editor. Let us see how to do it.

1. Open a Schematic in the Editor.

2. Click Options >> Select Assign.

   {{<img alt="Options Menu" src="/images/Eagle/Shortcuts/OptionsMenu.png" cap="Options Menu">}}

3. You can notice in the Assign window there are few shortcuts already defined. You can change, add and delete shortcuts from here.

{{<img alt="Assign Shortcuts Window" src="/images/Eagle/Shortcuts/AssignWindow.PNG" cap="Assign Shortcuts Window">}}

4. To add new shortcut click on New.

5. Select the Key combinations in the options and Assign a command to it. Here we will select Ctrl and D, Delete as Command.

{{<img alt="Shortcut for Delete Command" src="/images/Eagle/Shortcuts/DeleteShortcut.PNG" cap="Shortcut for Delete Command">}}

6. Now click OK.

7. To exercise, we will add few shapes and lines as below. If you don’t know how to please follow this tutorial.

{{<img alt="Shapes" src="/images/Eagle/Shortcuts/Gif1.PNG" cap="Shapes">}}

8. Hit your shortcut to Delete i.e. Ctrl+D. You can notice that the Delete command is activated.

9. Select any line or shape, it just got deleted. WOW.

{{<img alt="Delete Shortcut Execution" src="/images/Eagle/Shortcuts/gif2.PNG" cap="Delete Shortcut Execution">}}

10. In the similar way you can add shortcut to whatever the command you want. Its best practice to have shortcuts defined if you are working on Eagle extensively. 

11. You can also modify the already created shortcuts. Just hit Change button in Assign Window.

{{<img alt="Change/Modify Shortcut" src="/images/Eagle/Shortcuts/ChangeShortcut.PNG" cap="Change/Modify Shortcut">}}
