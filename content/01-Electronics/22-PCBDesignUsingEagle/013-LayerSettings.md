﻿---
title: "Layer Settings"
date: 2019-12-19T20:05:48+05:30
weight: 13
summary: Layer Settings
chapter: Schematic
---

The layer settings will be different for Schematic Editor and Layout Editor. 
When in Schematic Editor you can specify the layers such as Pins, Names, Values 
and Symbols etc. But in Layout Editor you will be dealing with the physical layers 
of PCB. To change the Layer settings, follow below steps.

1. While you are in Schematic Editor, click on `Layer Icon`, in the tools.

2. It will open Layer Settings Window as below. Whatever the Layers 
marked grey will be visible in your schematic sheet, other don’t. 
You can also Add or Delete a Layer by using `Add` and 
`Delete Icons`, right below the list. You can also switch between 
aliased layers by clicking on Layer Presets drop down list. 
If not add an alias you can add by clicking Add New button next 
to it. You can also remove the aliases by selecting and clicking 
Remove button.

{{<img alt="Layer Settings" src="/images/Eagle/LayerSettings/Layers.PNG" cap="Layer Settings">}}
