﻿---
title: "Window Positions"
date: 2019-12-19T20:05:48+05:30
weight: 7
summary: Window positioning
chapter: Schematic
---

You can store the current window position and size of the Editor windows by going to `Options` menu in `Control Panel` and selecting the `Window Positions` option. Here you can store present position and size of the editor windows. Once you save the options, from then whenever you open the Editor it will open as per your saved settings. If you select delete all saved window position eagle will set the default window position and size to the editor.

{{<img alt="Window Position Options" src="/images/Eagle/WindowPosition/WindowPositionOptions.png" cap="Window Position Options">}}

{{<img alt="Window Position Options Window" src="/images/Eagle/WindowPosition/WindowPositionOptionsopen.PNG" cap="Window Position Options Window">}}
