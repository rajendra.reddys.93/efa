﻿---
title: "Overview"
date: 2019-12-19T20:05:48+05:30
weight: 1
summary: An introduction to Autodesk Eagle PCB Designing suite
chapter: Schematic
---

Eagle is light-weight and free (limited edition) CAD Software with schematic capture, PCB Layout, Auto-router and Gerber generator tools embedded in it. Eagle stands for Easily Applicable Graphical Layout Editor.  The software was developed by CadSoft Computers GmbH which acquired by Autodesk Inc. Eagle software is currently owned by Autodesk Inc.

{{<img alt="Eagle Website" src="/images/Eagle/Overview/OverView.PNG" cap="Eagle Website">}}

Eagle is very popular among the individual hobbyists and open source / DIY electronics companies such as SparkFun, Adafruit and Arduino. We have listed few features of Eagle which made it to stand out.

## Why Eagle

### 1. Free / Less cost:

The free version of the eagle software can be used to develop simple boards for hobbyists. If you are interested in designing complex boards then you can go with their subscription plans which are cheaper than that of competitors. You can check out the different subscriptions of Eagle here.

### 2. Light weight

The setup of Eagle 8.1.0 is around 85MB, 1GB of your drive space is more than sufficient to install this packages.

### 3. Easy to learn

With the tutorials and resources available in the Autodesk website is great
place to start with learning to use Eagle software. You can also search for
SparFun for Eagle Tutorials. Even here we are going to discuss designing
schematics and PCBs using Eagle.

### 4. Great Community Support

The Autodesk website has great community support for Eagle. They run blogs for
latest updates, forum for community support, tutorials and step-by-step guides
for learning, reference designs and libraries to start with the finish the
design process with less time.

{{<img alt="Eagle Forum" src="/images/Eagle/Overview/Forum.PNG" cap="Eagle Forum">}}

### 5. Cross platform

The major advantage of Eagle is that it can be run on different OS with similar UI on any platforms.

### 6. ULP (User Language Programs)

The EAGLE User Language Programs can be used to access the EAGLE data
structures and to create a wide variety of output files. This is added
advantage of Eagle, but user should know to write programs.

Though Eagle have ample features to hang on, it has few drawbacks also. The auto router,
3D viewer, Simulators provided with Eagle can’t match with the ones that provided by other
software vendors. You can always use Eagle for simple to moderate complex boards.


### External Links

- To download Eagle for Free: - <a target="_blank" href="https://www.autodesk.com/products/eagle/free-download">https://www.autodesk.com/products/eagle/free-download</a>

- Eagle Forum: - <a target="_blank" href="https://forums.autodesk.com/t5/eagle-forum/bd-p/3500">https://forums.autodesk.com/t5/eagle-forum/bd-p/3500</a>
