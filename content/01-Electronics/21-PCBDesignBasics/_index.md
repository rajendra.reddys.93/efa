---
title: "PCB Designing - Introduction"
date: 2021-04-29
weight: 21
summary: Foot print, Pads, Layers, Terminologies, etc...
categories: ["Basics", "PCB"]
slug: pcb-designing-basics
draft: true
type: Hardware
menu:
    main:
        parent: "Electronics"
---

Foot print, Pads, Layers, Terminologies, etc...
