﻿---
title: Voltage
date: 2021-05-13T08:33:48+05:30
weight: 4
slug: voltage
chapter: 00-Introduction
expertise: Beginner
---

Voltage is the rate at which energy is drawn from a source that produces a flow of electricity in a circuit; expressed in Volts. Voltage is often described as the difference in charge between two points.

## Two types of Voltage Sources

1. DC Source
2. AC Source

{{<img alt="electrons-flow" src="/images/Basics/Current/electrons-flow.gif" cap="Electrons Flow">}}
{{<img alt="types-of-voltages" src="/images/Basics/Voltage/voltage-sources.png" cap="Types of Voltages">}}

Greater the voltage, greater the flow of conducting or semi-conducting medium for a given resistance to flow. Voltage is symbolized by uppercase italic letter _**V**_ or _**E**_

The standard unit is the volt, symbolized by non-italic uppercase letter **V**

{{<quote>}}
> _1V will drive 1 coulomb(6.24x108) charge carriers, such as electrons, through a resistance of 1Ω in 1 second._
{{</quote>}}
