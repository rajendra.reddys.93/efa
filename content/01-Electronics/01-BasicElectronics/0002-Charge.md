---
title: Charge
date: 2021-05-13T08:33:48+05:30
weight: 2
slug: charge
chapter: 00-Introduction
expertise: Beginner
---

Electric Charge is the physical property of matter that causes it to experience a force when placed in an electromagnetic field.

Two types of Charge
-------------------

1. Positive Charge
2. Negative Charge

{{<img alt="unlike-charge" src="/images/Basics/Charge/electrical_charges-attract.gif" cap="Unlike Charges">}}
{{<img alt="like-charge" src="/images/Basics/Charge/electrical_charges-repell.gif" cap="Like Charges">}}

Like charges repel and unlike charges attract. An object is negatively charged if it has an excess of electrons, and if otherwise positively charged or uncharged. The SI unit of electric charge is _**Coulomb(C)**_. In electrical engineering it is also common to use the ampere-hour(Ah), and in chemistry, it is common to use elementary charge(e) as unit.

{{<quote>}}
> _The symbol **Q** often denotes charge._
{{</quote>}}
