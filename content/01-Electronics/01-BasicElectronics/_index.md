---
title: "Basic Electronics"
date: 2021-04-29
weight: 1
summary: Voltage, Current, Resistor, Capacitor, Inductor, etc...
slug: basic-electronics
type: book
menu:
    main:
        parent: "Electronics"
---

Voltage, Current, Resistor, Capacitor, Inductor, etc...
