﻿---
title: Capacitor
date: 2021-05-13T08:33:48+05:30
weight: 201
slug: capacitor
chapter: 02-Capacitor
expertise: Beginner
---

A capacitor is a passive two-terminal electrical component that stores electrical energy
in an electric field. A capacitor consists of two or more parallel conductive (metal) plates
which are not connected or touching each other, but are electrically separated either by air or
by Dielectric.

{{<img alt="Capacitor Symbols" src="/images/Basics/Capacitor/types_of_capacitor.jpeg" cap="Types of Capacitors">}}

{{<img alt="Capacitor Dielectric" src="/images/Basics/Capacitor/Capacitor_dielectric.jpeg" cap="Capacitor Dielectric">}}

When there is potential difference across the conductors, (e.g. when a capacitor is connected across a battery) an electric field develops across the dielectric, causing positive charge +Q to collect on one plate and negative charge -Q on other plate. If a battery has been attached to a capacitor for a sufficient amount of time, no current can flow through the capacitor. However, if a time varying voltage is applied across the leads of capacitor, a displacement current can flow.

{{<quote>}}
> The SI unit of Capacitance is **farad (F)**
{{</quote>}}

## Capacitance

Capacitance is defined as the ratio of the electric charge Q on each conductor to the potential difference V between them.

{{<quote>}}
> C = Q / V
>
> C -> Capacitance (Farads),
>
> Q -> Charge (Coulombs), and
>
> V -> Voltage (Volts)
{{</quote>}}

## Series Capacitance

{{<img alt="capacitors-in-series" src="/images/Basics/Capacitor/capacitors_in_series.png" cap="Capacitors in Series">}}

Capacitors are said to be connected in "series", when they are daisy chained in a single line. The charging current (iC) flowing through the capacitors is same for all capacitors as it has only one path to flow.

### Equations

The **total capacitance, CTotal** of the series capacitor circuit must be equal to the reciprocal sum of all the individual capacitors added together. Therefore

{{<quote>}}
> 1/CTotal = 1/C1 + 1/C2 + ... + 1/Cn
{{</quote>}}

Parallel Capacitance
--------------------

{{<img alt="capacitors-in-parallel" src="/images/Basics/Capacitor/capacitors_in_parallel.png" cap="Capacitors in Parallel">}}

Capacitors are said to be connected in "parallel", when both of their terminals respectively connected to each terminal of the other capacitors. Capacitors connected in parallel have common voltage across them.

### Equations

The **total capacitance, CTotal** of the parallel capacitor circuit must be equal to the sum of all the individual capacitors added together. Therefore

{{<quote>}}
> CTotal = C1 + C2 + ... + Cn
{{</quote>}}
