---
title: Ohm's Law
date: 2021-05-13T08:33:48+05:30
weight: 102
slug: ohms-law
chapter: 01-Resistor
expertise: Beginner
---

## Definition

Ohm's law states that the voltage (V) across a resistor is proportional to the current (I), where the constant of proportionality is the resistance (R).

{{<quote>}}
> _V = I \* R._
{{</quote>}}

For example, if a 300 ohm resistor is attached across the terminals of a 12 volt battery, then a current of 12 / 300 = 0.04 amperes flows through that resistor.

Three equivalent expressions of Ohm's law

{{<quote>}}
> _V = I \* R  
> R = V / I  
> I = V / R._
{{</quote>}}

## Pie Chart of Ohm's Law

{{<img alt="ohms-law-pie-chart" src="/images/Basics/OhmsLaw/ohms-law-wheel.png" cap="Ohm's Law Pie Chart">}}
