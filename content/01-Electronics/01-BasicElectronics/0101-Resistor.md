---
title: Resistor
date: 2021-05-13T08:33:48+05:30
weight: 101
slug: resistor
chapter: 01-Resistor
expertise: Beginner
---

A Resistor is a passive two terminal electrical component that implements electrical resistance as a circuit element. In electronics circuits, resistors often used to limit current flow, to adjust signal levels, bias active elements, and terminate transmission lines among other uses.

{{<img alt="resistor" src="/images/Basics/Resistor/resistor.jpg" cap="Resistor">}}

**Fixed resistors** have resistance that only changes slightly with temperature, time and operating voltage.  
**Variable resistors** can be used to adjust the circuit elements (such as volume or lamp dimmer), or as sensing devices for heat, light, humidity, force or chemical activity.

{{<img alt="resistor types" src="/images/Basics/Resistor/resistor-types.jpg" cap="Resistor Types">}}

## Resistor Color Code

{{<img alt="resistor-color-code" src="/images/Basics/Resistor/resistor-color-code.jpeg" cap="Resistor Color Code">}}

Easy to remember color code

{{<quote>}}
> _**BBROY** of **G**reat **B**ritan had a **V**ery **G**ood **W**ife_
{{</quote>}}

## Series Resistance

{{<img alt="resistors-in-series" src="/images/Basics/Resistor/resistors_in_series.png" cap="Resistors in Series">}}

Resistors are said to be connected in "series", when they are daisy chained in a single line. Resistors in series have a common Current flowing through them.

### Equations

The **total resistance, RTotal** of the series resistor circuit must be equal to the sum of all the individual resistors added together. Therefore

{{<quote>}}
> _RTotal = R1 + R2 + ... + Rn._
{{</quote>}}

The **total voltage, VTotal** of the series resistor circuit must be equal to the sum of all the individual voltages added together. Therefore

{{<quote>}}
> _VTotal = VR1 + VR2 + ... + VRn._
{{</quote>}}

The **current** across all resistors in the series resistor circuit remains same.

{{<quote>}}
> _ITotal = IR2 = IR2 = ... = IRn._
{{</quote>}}

## Parallel Resistance

{{<img alt="resistors-in-parallel" src="/images/Basics/Resistor/resistors_in_parallel.png" cap="Resistors in Parallel">}}

Resistors are said to be connected together in "parallel", when both of their terminals respectively connected to each terminal of the other resistors. Resistors connected in parallel have common voltage across them.

### Equations

The **total resistance, RTotal** of the parallel resistor circuit must be equal to the reciprocal sum of all the individual resistors added together. Therefore

{{<quote>}}
> _1/RTotal = 1/R1 + 1/R2 + ... + 1/Rn._
{{</quote>}}

The **voltage** across all resistors in the parallel resistor circuit remains same.

{{<quote>}}
> _VTotal = VR1 = VR2 = ... = VRn._
{{</quote>}}

The **total current, ITotal** of the parallel resistor circuit must be equal to the sum of all the individual currents added together. Therefore

{{<quote>}}
> _ITotal = IR2 + IR2 + ... + IRn._
{{</quote>}}
