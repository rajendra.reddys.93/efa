---
title: Current
date: 2021-05-13T08:33:48+05:30
weight: 3
slug: current
chapter: 00-Introduction
expertise: Beginner
---

Current is the flow of electrical charge carriers. usually electronics and electron-deficient atoms. The common symbol for current is **I**. The standard unit is _Ampere_, symbolized by _**A**_.

## Two types of Current

1. **DC** - Direct Current: electrons flow in one direction.
2. **AC** - Alternating Current:pushes the electrons back and forth, changing the direction of the flow several times per second. In India, the current changes direction at the rate of 50Hz or 50 times per second.

{{<img alt="electrons-flow" src="/images/Basics/Current/electrons-flow.gif" cap="Electrons Flow">}}
{{<img alt="types-of-current" src="/images/Basics/Current/current-types.png" cap="Types of Current">}}

The particles that carry the charge in an electric circuit are called charge carriers.

{{<quote>}}
> 1A of current represents 1Q(coulomb) of electric charge(6.24x10<sup>8</sup> charge carriers) moving past a specific point in one second.
{{</quote>}}
