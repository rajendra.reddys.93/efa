---
title: Transistor
date: 2021-05-13T08:33:48+05:30
weight: 501
slug: transistor
chapter: 05-Transistor
expertise: Beginner
---

A transistor is a three layer device (npn and pnp) with two junctions and can provide circuit properties of both current controller and switch.

## Transistor Symbol

{{<img alt="transistor-symbol" src="/images/Basics/Transistor/Transistor-symbols.png" cap="Transistor Symbol">}}

The symbols above depicts the circuits symbols of npn and pnp transistors. The direction of the arrow on emitter terminal indicates the flow of current.

Easy way to remember transistor symbol.

* PNP - **P**oints i**N** **P**ermanently
* NPN - **N**ot **P**ointing **I**n

## Operation

{{<img alt="pnp-transistor-operation" src="/images/Basics/Transistor/PNP_BJT_Basic_Operation.png" cap="PNP Transistor Operation">}}

Consider a biased PNP bipolar junction transistor as shown in the above circuit. A PNP junction transistor is formed by separating two n-type semiconductor material by thin layer of p-type semiconductor material. To explain the operation of the transistor we need to apply voltage across the terminals of the transistor as shown in the above circuit.

Here the Emitter-Base junction is forward biased and Base-Collector junction is reverse biased. If the base voltage is 0 (less than the voltage at emitter), the transistor will be in OFF state. If the base voltage increases over the emitter voltage (which induces current at base terminal) the transistor starts to conduct until it is fully switched ON state. When the base current is sufficient enough the transistor completely turns ON. Then the current flows from collector to emitter.

## Transistor Operating Modes

### Active Mode

Soon after base voltage rises over the emitter voltage the transistor enters into active region. In this mode transistor is generally used as a current amplifier. In active mode, two junctions are differently biased that means emitter-base junction is forward biased whereas collector-base junction is reverse biased. In this mode current flows between emitter and collector and amount of current flow is proportional to the base current.

### Cutoff Mode

{{<img alt="BJT-in-cut-off-region" src="/images/Basics/Transistor/BJT-in-cut-off-region.png" cap="Transistor in Cutoff region">}}

Till the base voltage is less than voltage at emitter the transistor remain in cutoff region. In this mode, both collector base junction and emitter base junction are reverse biased. This in turn not allows the current to flow from collector to emitter when the base-emitter voltage is low. In this mode device is completely switched off as the result the current flowing through the device is zero.

### Saturation Mode

{{<img alt="pnp-transistor-operation" src="/images/Basics/Transistor/PNP_BJT_Basic_Operation.png" cap="PNP Transistor Operation">}}

{{<img alt="BJT-in-saturation-region" src="/images/Basics/Transistor/BJT-in-saturation-region.png" cap="Transistor in Saturation region">}}

When the base current is sufficient enough the transistor completely turns ON and remains in saturation region. In this mode of operation, both the emitter base and collector base junctions are forward biased. Current flows freely from collector to emitter when the base-emitter voltage is high. In this mode device is fully switched ON.

The below figure shows the output characteristics of the BJT Transistor.

{{<img alt="operating-modes-of-transistor" src="/images/Basics/Transistor/operating-modes-of-transistor.jpeg" cap="Operating Modes of Transistor">}}

<!--
### Common Base Configuration

### Common Emitter Configuration

### Common Collector Configuration
-->
