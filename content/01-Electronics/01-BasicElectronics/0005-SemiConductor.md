---
title: Semiconductor
date: 2021-05-13T08:33:48+05:30
weight: 5
slug: semiconductor
chapter: 00-Introduction
expertise: Beginner
---

A semiconductor is a material that has a conductivity somewhere between extremes of an insulator and a conductor. e.g.:- Ge and Si. An increase in temperature of a semiconductor can result in a substantial increase in the number of free electrons in the material. Semiconductor material such as Ge and Si that show a reduction in resistance with increase in temperature are said to be have a negative temperature co-efficient.

## Energy band diagram

{{<img alt="energy-band-diagram" src="/images/Basics/Semicondcutor/energy-band-diagram.jpeg" cap="Energy band diagram">}}

Here EG represents the energy band gap. It is the amount of energy that should be imparted to the electron in the valence conduction bond.

## Extrinsic material

The characteristics of semiconductor materials can be altered significantly by the addition of certain impurity atoms into the relatively pure semiconductor material by doping process. A semiconductor material that has been subjected to the doping process is called as extrinsic material. Pure semiconductor material without any impurity is known as an intrinsic semiconductor material.

### n - type material

The n - type is created by introducing impurity elements that are penta-valent such as Antimony, Arsenic and Phosphorous. These diffused impurities are called donor atoms.

{{<img alt="n-type-semiconductor" src="/images/Basics/Semicondcutor/n-type-semiconductor.jpeg" cap="n - type semiconductor">}}

### p - type material

The p - type material is formed by doping impurity elements that are tri-valent such as Boron, Gallium and Indium. These diffused impurities are called acceptor atoms.

{{<img alt="p-type-semiconductor" src="/images/Basics/Semicondcutor/p-type-semiconductor.jpeg" cap="p - type semiconductor">}}
