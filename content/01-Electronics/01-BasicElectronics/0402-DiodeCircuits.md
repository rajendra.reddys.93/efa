---
title: Diode Circuits
date: 2021-05-13T08:33:48+05:30
weight: 402
slug: diode-circuits
chapter: 04-Diode
expertise: Beginner
---

A diode can be used as clippers, clamper, rectifiers, voltage multipliers, logic gates, reverse voltage protection, voltage spike protection, etc. Let's explore few.

> _Note: please consider Germanium diode with breakdown voltage as 0.3V while analyzing the below circuits and waveforms.._

## Rectifiers

Rectifier is a device that converts alternating voltage or current into a direct voltage or current due to a certain type of non-linear of its characteristics. Basically rectifiers are of two types. Half wave and full wave rectifiers.

### Half-wave Rectifiers

Consider the below half-wave rectifier circuit. In positive half cycle of the input, the diode is forward biased. The output voltage is equal to that of input. During negative half cycle the diode is reverse biased. Therefore the output voltage is Zero. You can achieve the negative output by reversing the diode, which results in filtering out positive half of the input.

{{<img alt="Half-wave Diode Rectifier Circuit" src="/images/Basics/DiodeCircuits/Half-wave-Rectifier.png" cap="Half-wave Diode Rectifier Circuit">}}

{{<img alt="half-wave-diode-rectifier-waveforms" src="/images/Basics/DiodeCircuits/Half-wave-Rectifier-Output.png" cap="Waveforms">}}

In half-wave rectifiers either positive or negative half of the input is filtered out, leaving the other half to pass through the load. Because one half of the input reaches the output, mean voltage in lower. The harmonic content of the rectifier’s output waveform is very large and consequently difficult to filter.

### Full-wave Rectifiers

The disadvantages of huge harmonics in the half-wave rectifier can be overcome by full-wave rectifier. Two types of full-wave rectifier are center tapped and bridge rectifier.

#### Center tap rectifier

Consider the below center tapped full-wave rectifier circuit. In positive half cycle of the input, the diode D1 is forward biased and diode D2 is reverse biased. Therefore the output voltage is equal to that of input. During negative half cycle the diode D1 is reverse biased and diode D2 is forward biased. Therefore the output voltage is same as input. You can achieve the negative output by reversing the diode, which results in filtering out positive half of the input.

{{<img alt="center-tapped-full-wave-diode-rectifier-circuit" src="/images/Basics/DiodeCircuits/Full-wave-Rectifier.png" cap="Centered Tapped Full-wave Diode Rectifier Circuit">}}

{{<img alt="center-tapped-full-wave-diode-rectifier-circuit-waveforms" src="/images/Basics/DiodeCircuits/Full-wave-Rectifier-Output.png" cap="Waveforms">}}

The disadvantage of the center tapped full-wave rectifier is the necessity of transformer with a center tapped secondary winding. Therefore center-tapped full wave rectifier is not popular in high voltage applications as the transformer design becomes expensive.

#### Bridge Rectifiers

Below is full-wave bridge rectifier circuit. In positive half cycle of the input, the diode D1 and D4 are forward biased, diode D2 and D3 are reverse biased. Therefore the output voltage is equal to that of input. During negative half cycle the diode D1 and D4 are reverse biased and diode D2 and D3 are forward biased. Therefore the output voltage is same as input. You can achieve the negative output by reversing the diode, which results in filtering out positive half of the input.

{{<img alt="full-wave-bridge-rectifier-circuit" src="/images/Basics/DiodeCircuits/Full-wave-Bridge-Rectifier.png" cap="Full-wave Bridge Rectifier Circuit">}}

{{<img alt="full-wave-bridge-rectifier-circuit-waveforms" src="/images/Basics/DiodeCircuits/Bridge-Rectifier-Output.png" cap="Waveforms">}}

The disadvantage of the full-wave bridge rectifier is the voltage drop across two diode in series with the load(0.3V x 2 = 0.6V for Ge). The bridge rectifier is best suit for high voltage application as voltage loss can be ignored.

## Clippers

A clipper is a device designed to prevent the output of a circuit from exceeding a predetermined voltage level without distorting the remaining part of the applied waveform.

### Positive Clipper

Consider a Positive Clipper is as shown in the figure below.

{{<img alt="diode-positive-clipping-circuit" src="/images/Basics/DiodeCircuits/Positive-Clipper.png" cap="Diode Positive Clipping Circuit">}}

{{<img alt="diode-positive-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Positive-Clipper-Output.png" cap="Waveforms">}}

In this diode clipping circuit, the diode is forward biased(for input voltage greater than 0.3V in case diode is made of Si) during positive half cycle of input signal. The diode conducts and voltage across diode will never exceed 0.3V.

During the negative half cycle of input signal, the diode is reverse biased which blocks the current flow through diode. The diode doesn't conduct and voltage passes through load. Since the circuit clips off the positive half, it's called as positive clipper circuit.

### Negative Clipper

{{<img alt="diode-negative-clipping-circuit" src="/images/Basics/DiodeCircuits/Negative-Clipper.png" cap="Diode Negative Clipping Circuit">}}

{{<img alt="diode-negative-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Negative-Clipper-Output.png" cap="Waveforms">}}

Here, the diode is forward biased during negative half cycle of input signal and clips the input to -0.3V while allowing the input to pass through the load in the positive half cycle of input signal. Since the circuit limits the negative half, it's called as negative clipper circuit.

### Positive Biased Clipper

{{<img alt="diode-positive-biased-clipping-circuit" src="/images/Basics/DiodeCircuits/Positive-Clipper-Biased.png" cap="Positive Biased Clipping Circuit">}}

{{<img alt="diode-positive-biased-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Positive-Clipper-Biased-Output.png" cap="Waveforms">}}

In this diode clipping circuit, the diode is forward biased only when the input voltage greater than VBIAS+0.3V during positive half cycle of input signal. Any positive voltages grater than VBIAS+0.3V will be clipped off.

### Negative Biased Clipper

{{<img alt="diode-negative-biased-clipping-circuit" src="/images/Basics/DiodeCircuits/Negative-Clipper-Biased.png" cap="Negative Biased Clipping Circuit">}}

{{<img alt="diode-negative-biased-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Negative-Clipper-Biased-Output.png" cap="Waveforms">}}

Here, the diode is forward biased during negative half cycle of input signal and limits the input to (-VBIAS\-0.3V) while allowing the input to pass through the load in the positive half cycle of input signal.

### Symmetric Clipper

Symmetric clipping can be achieved by the combination of both positive and negative clipping circuits together.

{{<img alt="diode-symmetric-clipping-circuit" src="/images/Basics/DiodeCircuits/Symmetric-Clipper.png" cap="Symmetric Clipping Circuit">}}

{{<img alt="diode-symmetric-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Symmetric-Clipper-Output.png" cap="Waveforms">}}

If we connect two diodes in inverse parallel as shown in the above circuit, then both positive and negative half cycles of the input will be clipped off as the diode D1 clips positive half cycle, diode D2 clips the negative half cycle of the input signal.

For an idea diode the output of above circuit would be zero. However, due to forward voltage drop of the diode the actual clipping occurs at +0.3V and -0.3V. Different clipping voltage levels can be achieved by adding more diode in series or by adding bias voltage to diode as below.

### Symmetric Clipper with Bias Voltage

{{<img alt="diode-symmetric-biased-clipping-circuit" src="/images/Basics/DiodeCircuits/Symmetric-Clipper-Biased.png" cap="Symmetric Biased Clipping Circuit">}}

{{<img alt="diode-symmetric-biased-clipping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Symmetric-Clipper-Biased-Output.png" cap="Waveforms">}}

When the voltage of the positive half cycle reaches +3.3 V, diode D1 conducts and limits the waveform at +3.3 V. Diode D2 does not conduct until the voltage reaches –2.3 V. Therefore, all positive voltages above +3.3 V and negative voltages below –2.3 V are automatically clipped.

## Clampers

Clamper is a electronic circuit that shifts the either positive or negative peak of a signal to a defined DC level.

### Positive Un-biased Clamper

{{<img alt="diode-positive-unbiased-clamping-circuit" src="/images/Basics/DiodeCircuits/Positive-Clamper.png" cap="Positive Un-biased Clamping Circuit">}}

{{<img alt="diode-positive-unbiased-clamping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Positive-Clamper-Output.png" cap="Waveforms">}}

In the above positive unbiased diode clamping circuit, during negative half cycle of the AC input signal the diode will conduct. Hence the capacitor gets charged to peak voltage of the input. In positive half cycle, the diode doesn't conduct. Therefore the output voltage is equal to the charge stored in the capacitor plus the input voltage.

{{<quote>}}
> _VOUT = VIN + VINPeak_
{{</quote>}}

### Negative Un-biased Clamper

{{<img alt="diode-negative-unbiased-clamping-circuit" src="/images/Basics/DiodeCircuits/Negative-Clamper.png" cap="Negative Un-biased Clamping Circuit">}}

{{<img alt="diode-negative-unbiased-clamping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Negative-Clamper-Output.png" cap="Waveforms">}}

In the above negative unbiased diode clamping circuit, during positive half cycle of the AC input signal the diode will conduct. Hence the capacitor gets charged to peak voltage of the input. In positive half cycle, the diode doesn't conduct. Therefore the output voltage is equal to the charge stored in the capacitor subtracted by the input voltage.

{{<quote>}}
> _VOUT = VIN - VINPeak_
{{</quote>}}

### Positive Biased Clamper

{{<img alt="diode-positive-biased-clamping-circuit" src="/images/Basics/DiodeCircuits/Positive-Clamper-Biased.png" cap="Positive Biased Clamping Circuit">}}

{{<img alt="diode-positive-biased-clamping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Positive-Clamper-Biased-Output.png" cap="Waveforms">}}

A positive biased voltage clamp is identical to an equivalent unbiased clamp but with the output voltage offset by the bias amount VBIAS.

{{<quote>}}
> _VOUT = VIN + (VINPeak + VBIAS)_
{{</quote>}}

### Negative Biased Clamper

{{<img alt="diode-negative-biased-clamping-circuit" src="/images/Basics/DiodeCircuits/Negative-Clamper-Biased.png" cap="Negative Biased Clamping Circuit">}}

{{<img alt="diode-negative-biased-clamping-circuit-waveforms" src="/images/Basics/DiodeCircuits/Negative-Clamper-Biased-Output.png" cap="Waveforms">}}

A negative biased voltage clamp is likewise identical to an equivalent unbiased clamp but with the output voltage offset in the negative direction by the bias amount VBIAS.

{{<quote>}}
> _VOUT = VIN - (VINPeak + VBIAS)_
{{</quote>}}

## Diode in Logic Gates

Because of special characteristics of the diode, it can be used as logic gates.

### Diode as And Gate

{{<img alt="diode-in-logic-and-circuit" src="/images/Basics/DiodeCircuits/Diode-logic-and.png" cap="Diode - Logic 'AND' Circuit">}}

In the above diode logic circuit, both the diodes are reverse biased only when both the switches A and B are open. When both the diodes are reverse biased the output is tied to logic high by the resistor. When any of the diode is forward biased by closing any of the switches A or B, the output will be 0.

### Diode as OR Gate

{{<img alt="diode-in-logic-or-circuit" src="/images/Basics/DiodeCircuits/Diode-logic-or.png" cap="Diode - Logic 'OR' Circuit">}}

In the above diode logic circuit, both the diodes are reverse biased only when both the switches A and B are open. When both the diodes are reverse biased the output is tied to logic 0 by the resistor. When any of the diode is forward biased by closing any of the switches A or B, the output will be logic high.

The diode logic or circuit can be used in dual powered circuits. For example, if your circuit consists of a dedicated RTC chip which should be powered always. In this case the RTC will be powered from the line voltage whenever it is available. When line voltage goes off the voltage at the RTC power pin falls below the battery voltage so the diode in series with battery becomes forward biased resulting the RTC power pin to get power from battery.

## Diode in Reverse Current Protection

The reverse polarity or current protection is necessary to avoid the damage that occurs due to connecting the battery in a wrong way or reversing the polarities of the DC supply. This accidental connection of supply causes to flow a large amount current, thorough the circuit components results to explode them. Therefore, a protective or blocking diode is connected in series with the positive side of the input to avoid the reverse connection problem.

{{<img alt="diode-in-reverse-current-protection-circuit" src="/images/Basics/DiodeCircuits/Diode-in-Reverse-Current-Protection.png" cap="Diode in Reverse Current Protection Circuit">}}

Above figure shows the reverse current protection circuit where diode is connected in series with the load at the positive side of the battery supply. In case of the correct polarity connection, diode gets forward-biased and load current flows through it. But, in case of wrong connection, the diode is reverse-biased and that doesn’t allow any current to flow to the load. Hence, the load is protected against the reverse current.

## Diode in Voltage Spike Suppression

In case of an inductor or inductive loads, sudden removal of supply source produces a higher voltage due to its stored magnetic field energy. These unexpected spikes in the voltage can cause the considerable damage to the circuit components. Hence, a diode is connected across the inductor or inductive loads to limit the large voltage spikes. These diodes are also called by different names in different circuits such as snubber diode, fly-back diode, suppression diode, and freewheeling diode and so on.

{{<img alt="diode-in-voltage-spike-suppression-circuit" src="/images/Basics/DiodeCircuits/Diodes-in-Voltage-Spike-Suppression.png" cap="Diode in Voltage Spike Suppression Circuit">}}

In the above figure the freewheeling diode is connected across the inductive load for suppressing of voltage spikes in an inductor. When the switch is suddenly opened, the voltage spike is created in the inductor. Therefore, the freewheeling diode makes the safe path to flow the current to discharge the voltage offered by the spike.
