---
title: Diode
date: 2021-05-13T08:33:48+05:30
weight: 401
slug: diode
chapter: 04-Diode
expertise: Beginner
---

A diode is a two terminal electronic component that conducts in one direction. It has low (ideally zero) resistance to the flow of current in one direction, and high (ideally infinite) resistance in the other.

{{<img alt="Diode" src="/images/Basics/Diode/Diode.png" cap="Diode">}}

{{<img alt="diode-symbol" src="/images/Basics/Diode/Diode_symbol.png" cap="Diode Symbol">}}

{{<img alt="LED-symbol" src="/images/Basics/Diode/LED_symbol.png" cap="LED Symbol">}}

{{<img alt="photo-diode-symbol" src="/images/Basics/Diode/Photodiode_symbol.png" cap="Photo Diode">}}

{{<img alt="Schottky-diode" src="/images/Basics/Diode/Schottky_diode_symbol.png" cap="Schottky Diode">}}

{{<img alt="tunnel-diode" src="/images/Basics/Diode/Tunnel_diode_symbol.png" cap="Tunnel Diode">}}

{{<img alt="varicap-diode" src="/images/Basics/Diode/Varicap_symbol.png" cap="Varicap Diode">}}

{{<img alt="zener-diode" src="/images/Basics/Diode/Zener_diode_symbol.png" cap="Zener Diode">}}

The diode is formed by simply bringing n-type and p-type material together. The common fuction of the diode is to allow electric current to pass one direction (called diode's forward direction), while blocking current in the opposite direction (the reverse direction).

## Ideal Diode

{{<img alt="ideal-diode-curve" src="/images/Basics/Diode/Ideal-diode-curve.png" cap="Ideal Diode Curve">}}

The characteristics of an ideal diode are those of a switch that can conduct current only one direction. In forward bias the resistance offered by the diode to the flow of electric current is 0 ohms (closed circuit), while in reverse bias the resistance is infinite (open circuit).

## Diode Biasing

Biasing is process of applying DC voltage across the terminals of diode. Diode biasing can be done in two ways.

* Forward biasing
* Reverse biasing

### Forward Biasing

If an external voltage V is applied across p-n-junction of the diode such that positive terminal to p-type material and negative terminal to n-type material, such a connection is called as forward bias.

{{<img alt="diode-forward-bias" src="/images/Basics/Diode/Diode-forward-bias.png" cap="Diode Forward Bias">}}

As the application of forward bias, the electrons in n-region moves towards positive terminal, and holes in p-region moves towards negative terminal which reduces the depletion region. This results in flow of electricity in the circuit.

### Reverse Biasing

If an external voltage V is applied across p-n-junction of the diode such that positive terminal to n-type material and negative terminal to p-type material, such a connection is called as reverse bias.

{{<img alt="diode-reverse-bias" src="/images/Basics/Diode/Diode-reverse-bias.png" cap="Diode Reverse Bias">}}

As the application of forward bias, the electrons in n-region moves towards positive terminal, and holes in p-region moves towards negative terminal which increases the depletion region. This results in no flow of electricity in the circuit.

{{<quote>}}
> _We can relate diode to switch. When diode is forward biased it acts as closed switch, and when reverse biased it acts as open switch._
{{</quote>}}
