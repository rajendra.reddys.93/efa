---
title: Kirchoff's Law
date: 2021-05-13T08:33:48+05:30
weight: 103
slug: kirchoffs-law
chapter: 01-Resistor
expertise: Beginner
---

Kirchhoff's circuit laws are used to deal with the current and potential difference (commonly known as voltage) in the lumped element model of electrical circuits.

## Kirchoff's Current Law

This law is also called Kirchhoff's first law, Kirchhoff's point rule, or Kirchhoff's junction rule (or nodal rule).

{{<img alt="kirchoffs-current-law" src="/images/Basics/KirchoffsLaw/Kirchhoff's_current_law.png" cap="Kirchoff's Current Law">}}

### Definition

At any node (junction) in an electrical circuit, the sum of currents flowing into that node is equal to the sum of currents flowing out of that node.

or

The algebraic sum of currents in a network of conductors meeting at a point is zero.

{{<quote>}}
> _ΣI = 0_
{{</quote>}}

Thus, total current leaving a junction is equal to total current entering that junction.

## Kirchoff's Voltage Law

This law is also called Kirchhoff's second law, Kirchhoff's loop (or mesh) rule, and Kirchhoff's second rule.

{{<img alt="kirchoffs-voltage-law" src="/images/Basics/KirchoffsLaw/Kirchhoff's_voltage_law.png" cap="Kirchoff's Voltage Law">}}

### Definition

The sum of the emfs in any closed loop is equivalent to the sum of the potential drops in that loop.

or

The algebraic sum of the products of the resistances of the conductors and the currents in them in a closed loop is equal to the total emf available in that loop.

{{<quote>}}
> _ΣV = 0_
{{</quote>}}

Thus, the sum of the electrical potential differences (voltage) around any closed network is zero.
