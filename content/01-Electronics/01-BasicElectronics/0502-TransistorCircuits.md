---
title: Transistor Circuits
date: 2021-05-13T08:33:48+05:30
weight: 502
slug: transistor-circuits
chapter: 05-Transistor
expertise: Beginner
---

A transistor can be used a switch, amplifier, attenuator, Logic Gate, H-Bridge and Oscillator, many more... We will focus on few applications of transistor here.

## Transistor as Switch

{{<img alt="transistor-as-switch" src="/images/Basics/TransistorCircuits/Transistor-as-switch.png" cap="Transistor as Switch">}}

In the above circuit, our control signal flows into the base terminal of the transistor, the output is tied to the collector and the emitter is tied to fixed voltage i.e GND (0V).

Where in case of an mechanical switch to work, it requires an actuator to be flipped physically. The transistor switch can be controlled by applying voltage at base terminal. A micro-controller pin can drive transistor by programming HIGH or LOW on its pin connected to base terminal.

When voltage at base terminal is greater than 0.7V (check data-sheet of the transistor you are using), small current starts to flow at the base terminal. which makes path for the current from collector to emitter. which in turn connects collector to emitter with small resistance.

It is very essential to choose the base resistor in such a way that the base current does not exceeds recommended/max value. A transistor without a base resistor is equal to LED without current limiting resistor.

## Digital Logic

Transistor can be combined together/used as is to create all basic logic gates: NOT, NAD and OR. Let us see one by one.

### NOT Gate

{{<img alt="transistor-as-not-gate" src="/images/Basics/TransistorCircuits/Transistor-as-NOT-gate.png" cap="Transistor as NOT Gate">}}

Here, whenever the voltage at the base is High, the transistor will turn on, which will effectively connects the collector to emitter. Since the emitter is directly connected to ground, the collector will also be connected to ground. If the input voltage is Low, the transistor turn off, the output is pulled up to VCC.

### AND Gate

A pair of two transistors are used to create 2-input AND Gate.

{{<img alt="transistor-as-and-gate" src="/images/Basics/TransistorCircuits/Transistor-as-AND-gate.png" cap="Transistor as AND Gate">}}

If either transistor is turned off, then the output at the second transistor’s collector will be pulled low. If both transistors are “on” (bases both high), then the output of the circuit is also high.

### OR Gate

A pair of two transistors are used to create 2-input OR Gate.

{{<img alt="transistor-as-or-gate" src="/images/Basics/TransistorCircuits/Transistor-as-OR-gate.png" cap="Transistor as OR Gate">}}

In this circuit, if either (or both) A or B are high, that respective transistor will turn on, and pull the output high. If both transistors are off, then the output is pulled low through the resistor.

Likewise any logic gate can be constructed using combination of transistors.

## H - Bridge

A transistor based H-Bridge is a circuit capable of driving motors in both clock and anti-clock directions. Basically, a H-Bridge is a combination four transistor (2 - NPN and 2 - PNP) with two inputs and two outputs.

{{<img alt="transistor-h-bridge-circuit" src="/images/Basics/TransistorCircuits/Transistor-H-Bridge-Circuit.png" cap="Transistor H - Bridge Circuit">}}

When both the inputs are at same voltage (High or Low), the outputs of the transistor circuits are same, and motor wont rotate. When the inputs are different voltage levels, say Input B is at High voltage and Input A at Low voltage, then the motor rotates in clock-wise direction. Please follow the below table for all input and output combinations.

{{<table cap="Truth Table">}}
| Input A | Input B | Output A | Output B | Motor Direction |
|---------|:--------|:---------|:---------|:----------------|
| 0       | 0       | 1        | 1        | Stopped         |
| 0       | 1       | 1        | 0        | Clockwise       |
| 1       | 0       | 0        | 1        | Counter - Clockwise |
| 1       | 1       | 0        | 0        | Stopped         |
{{</table>}}

<!--
### Amplifier

#### Common Emitter Amplifier
-->
