---
title: Inductor
date: 2021-05-13T08:33:48+05:30
weight: 301
slug: inductor
chapter: 03-Inductor
expertise: Beginner
---

An Inductor is nothing more than a coil of wire wound around a central core. For most coils the current (i), flowing through the coil produces a magnetic flux around it that is proportional to flow of electric current.

{{<img alt="inductor" src="/images/Basics/Inductor/Inductor.jpeg" cap="Inductor">}}

{{<img alt="inductor-images" src="/images/Basics/Inductor/Inductor-images.jpg" cap="Inductor images">}}

Inductors have values ranging from 1uH to 1H.

{{<quote>}}
> _The SI unit of inductance is Henry (H)._
{{</quote>}}
