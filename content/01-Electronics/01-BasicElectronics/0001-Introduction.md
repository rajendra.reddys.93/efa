---
title: Introduction
date: 2019-12-19T19:36:26+05:30
weight: 1
slug: introduction
chapter: 00-Introduction
expertise: Beginner
---

Electronics is the science of controlling electrically, in which the electrons have a fundamental role. Electronics deals with electrical circuits that involve active electrical components such as transistors, diodes, integrated circuits, associated passive electrical components, and interconnection technologies. Commonly, electronic devices contain circuitry consisting primarily or exclusively of active semiconductors supplemented with passive elements; such a circuit is described as an electronic circuit.

## Branches of Electronics

### Digital Electronics

Digital Electronics are electronic system that deals with digital signals (discrete levels of analog signals) rather than a analog signal itself. A digital signal is obtained by sampling an analog signal with respect to time. Since controllers/processors can't process an analog signal, it is essential to convert it to digital signal. A computer can only understand logic high (1) or logic low (0), so a digital signal can have only 1's and 0's.

Studying digital electronics is the process of understanding importance of logic gates (AND, OR, NOT, NAND, NOR, XOR, XNOR), timers, counters, memory registers in digital circuits. Extended study of digital electronics includes ADC (Analog to Digital Converter), DAC (Digital to Analog Converter), different transformation and sampling techniques, filters, modulation techniques to generate information rich digital signals.

### Analog Electronics

Analog electronics are electronics systems with a continuously variable signals. In contrast to [Digital Electronics](#digital-electronics) where signal can have only two levels i.e. high or low, the analog signals vary continuously over period of time. The term "analog" describes the relationship between a signal and the parameters (e.g. voltage, current) that represents the signals.

The study of analog electronics includes understanding response of active and passive circuit elements with change in voltage and current with respect to time. The active circuit elements are [diode](~/Electronics/Diode), [transistor](~/Electronics/Transistor), FET, amplifiers, oscillators. The passive circuit elements are [resistor](~/Electronics/Resistor), [capacitor](~/Electronics/Capacitor), [inductor](~/Electronics/Inductor). By mastering analog electronics you will be able to apply various modulation techniques on signal to derive more information from it. To name few modulation techniques - Angle Modulation, Frequency Modulation, Amplitude Modulation.

### Microelectronics

Microelectronics relates to the study and manufacturing of tiny electronics devices and components. These devices usually made from semiconductor materials. There are numerous micro-devices that we use in electronic circuits. To name few [diode](~/Electronics/Diode), [transistor](~/Electronics/Transistor), MOSFET, sensors, actuators, op-amps, etc...

The study and manufacturing of micro electronics opens doors for evolution in new designs with small size, cost-effective and affirmative results. The designing process consists testing and validating against EMI (Electro-Magnetic Induction), piezo-electric effect, erroneous output, thermal loading, maximum and minimum tolerance, etc...

The term "micro" defines the reduction in the size of an circuit element without compromising on performance, It is evident from the present technology advancements that nano-technology or nano-electronics components are the future trend in the electronic industry.

### Circuit Design

The process of circuit design can cover systems ranging from complex electronic systems all the way down to the individual transistors within an integrated circuit.

Circuit Design involves the study of various electronic technologies, product specification, design, verification and testing, prototyping, documentation, manufacturing and maintenance.

### Integrated Circuits

As the name itself represents, Integrated Circuit (IC) is a set of electronic circuits integrated on one small chip of semiconductor material, normally silicon. The major advantages of ICs over discrete electronic component is reduced size, cheaper in cost, faster in performance and lesser power consumption. The IC can consists of only single transistor or basic logic gate to more advanced and complex controller/processor architecture.

By using an IC in designing a circuit, you can reduce adequate complexity in the circuit. For example, to drive 8 Relays you can use 1 - ULN2803 IC instead of adding Transistor-Darlington circuit for each relay.

VLSI (Very-Large-Scale-Integration) is integration of 100s or 1000s of transistors on a small chip. It is used major for designing complicated ICs. Every micro-controller/micro-processor that we use today are designed using VLSI.

### Opto-Electronics

Optoelectronics is the study and application of electronic devices and systems that source, detect and control light. Light often includes invisible forms of radiation such as gamma rays, X-rays, ultraviolet and infrared, in addition to visible light. Optoelectronic devices are electrical-to-optical or optical-to-electrical transducers, or instruments that use such devices in their operation.

Optoelectronic involves the study of photo-diodes, photo-transistors, photomultipliers, opto-isolaters, opto-couplers, LED(Light Emitting Diode), Optical Fiber Communication(OFC).

### Semiconductor Devices

Semiconductor devices are electronic components that exploit the electronic properties of semiconductor materials, principally silicon, germanium, and gallium arsenide. Semiconductor materials are useful because their behavior can be easily manipulated by the addition of impurities, known as doping.

Study of semiconductor devices involves Diodes, Transistors, LED, Solar cell etc.

### Embedded Devices

An embedded system is a computer system with a dedicated function within a larger mechanical or electrical system, often with real-time computing constraints. Example properties of typically embedded computers when compared with general-purpose counterparts are low power consumption, small size, rugged operating ranges, and low per-unit cost.

In embedded systems we can study about micro-controllers, microprocessors, different interfaces / peripherals of controllers, embedded c programming, networks, etc.
