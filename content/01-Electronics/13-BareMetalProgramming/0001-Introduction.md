---
title: Introduction
date: 2019-12-19T19:36:26+05:30
weight: 1
slug: introduction
chapter: 00-Introduction
expertise: Beginner
---

# What is cross-compilation

## Build System

## Host System

## Target

{{<img alt="cross compiler environment" src="/images/bare-metal-programming/gcc-cross-compiler.png" cap="Cross compilation environment">}}

# [gcc-cross-compiler]

## Commands
