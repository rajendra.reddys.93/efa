---
title: Board Details
date: 2019-12-19T19:36:26+05:30
weight: 4
slug: board-details
chapter: 00-Introduction
expertise: Beginner
---

Details of board, controller and respective datasheets.

[https://www.microchip.com/en-us/development-tool/ATSAM4L-EK](ATSAM4L-EK Development Board)

[https://www.microchip.com/en-us/product/ATSAM4LC4C](Cortex-M4 Controller)

## Reference

[1] https://www.microchip.com/en-us/development-tool/ATSAM4L-EK

[2] https://www.microchip.com/en-us/product/ATSAM4LC4C

[3] https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42026-ATSAM4L-EK-User-Guide_ApplicationNote_AVR32850.pdf

[4] https://ww1.microchip.com/downloads/en/DeviceDoc/doc42027_SAM4L-EK_Design_Documentation.PDF

[5] https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/Atmel-42023-ARM-Microcontroller-ATSAM4L-Low-Power-LCD_Datasheet.pdf

[6] https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42023-ARM-Microcontroller-ATSAM4L-Low-Power-LCD_Datasheet-Summary.pdf

