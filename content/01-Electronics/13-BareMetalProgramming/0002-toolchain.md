---
title: Toolchain
date: 2019-12-19T19:36:26+05:30
weight: 2
slug: toolchain
chapter: 00-Introduction
expertise: Beginner
---

## Tool chain naming

Toolchains have a loose name convention like arch[-vendor][-os]-abi.

1. arch is for architecture: arm, mips, x86, i686...
1. vendor is tool chain supplier: apple,
1. os is for operating system: linux, none (bare metal)
1. abi is for application binary interface convention: eabi, gnueabi, gnueabihf

With "arm-eabi-gcc" you have the Linux system C library which will make calls into the kernel IOCTLs, e.g. for allocating memory pages to the process.

With "arm-eabi-none-gcc" you are running on platform which doesn't have an operating system at all - so the C library is different to cope with that.

Other differences include how processes get bootstrapped by the runtime linker / loader (i.e. a lot happens at runtime on Linux, which cannot happen in a baremetal platform).

```bash
$ sudo apt-get install gcc-arm-none-eabi binutils-arm-none-eabi openocd
```

You can also download a version of compiler from the OEM/Vendor.

https://www.microchip.com/en-us/tools-resources/develop/microchip-studio/gcc-compilers

```bash
$ arm-none-eabi-g++ --version
arm-none-eabi-g++ (15:9-2019-q4-0ubuntu1) 9.2.1 20191025 (release) [ARM/arm-9-branch revision 277599]
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

## openocd
is a tool to debug/program the target hardware

Install libs needed by OpenOCD.

```bash
sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev libtool pkg-config autoconf automake texinfo
```

```bash
$ openocd --version
Open On-Chip Debugger 0.11.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
```

```bash
$ openocd --help
Open On-Chip Debugger 0.11.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Open On-Chip Debugger
Licensed under GNU GPL v2
--help       | -h       display this help
--version    | -v       display OpenOCD version
--file       | -f       use configuration file <name>
--search     | -s       dir to search for config files and scripts
--debug      | -d       set debug level to 3
             | -d<n>    set debug level to <level>
--log_output | -l       redirect log output to file <name>
--command    | -c       run <command>
```
