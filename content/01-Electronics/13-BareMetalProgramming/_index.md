---
title: "Bare-metal Programming"
date: 2021-12-22
weight: 13
summary: Bare-metal Programming.
categories: ["Bare-metal"]
slug: bare-metal
draft: true
type: Hardware
menu:
    main:
        parent: "Electronics"
---

Bare-metal Programming
