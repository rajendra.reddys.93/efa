---
title: Building First Program
date: 2019-12-19T19:36:26+05:30
weight: 3
slug: building-first-program
chapter: 00-Introduction
expertise: Beginner
---

Try compiling with gcc-arm-none-eabi. it will complain about _exit (could be other symbol also) not defined.

```bash
$ arm-none-eabi-g++ main.cpp -Wall
/usr/lib/gcc/arm-none-eabi/9.2.1/../../../arm-none-eabi/bin/ld: /usr/lib/gcc/arm-none-eabi/9.2.1/../../../arm-none-eabi/lib/libc.a(lib_a-exit.o): in function `exit':
/build/newlib-CVVEyx/newlib-3.3.0/build/arm-none-eabi/newlib/libc/stdlib/../../../../../newlib/libc/stdlib/exit.c:64: undefined reference to `_exit'
collect2: error: ld returned 1 exit status
```

```bash
$ arm-none-eabi-g++ main.cpp -Wall -specs=nosys.specs
```

```bash
$ file a.out
a.out: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, with debug_info, not stripped
```

In above “nosys.specs” meaning these system calls would be ignored in normal functions but if we want to debug something in the code then we need to enable semihosting, where same above errors can be resolved by passing “–specs=rdimon.specs”

```bash
$ arm-none-eabi-g++ main.cpp -Wall -specs=rdimon.specs
```

The compiler may fail during linking as in case of compiler downloaded from the vendor.

```bash
$ ./arm-none-eabi/bin/arm-none-eabi-g++ main.cpp -specs=nosys.specs
/home/reddy/arm-none-eabi/bin/../lib/gcc/arm-none-eabi/6.3.1/../../../../arm-none-eabi/lib/libstdc++.a(locale.o): In function `std::locale::_Impl::_M_install_cache(std::locale::facet const*, unsigned int)':
locale.cc:(.text._ZNSt6locale5_Impl16_M_install_cacheEPKNS_5facetEj+0x18): undefined reference to `__sync_synchronize'
/home/reddy/arm-none-eabi/bin/../lib/gcc/arm-none-eabi/6.3.1/../../../../arm-none-eabi/lib/libstdc++.a(locale_init.o): In function `(anonymous namespace)::get_locale_mutex()':
locale_init.cc:(.text._ZN12_GLOBAL__N_116get_locale_mutexEv+0xc): undefined reference to `__sync_synchronize'
/home/reddy/arm-none-eabi/bin/../lib/gcc/arm-none-eabi/6.3.1/../../../../arm-none-eabi/lib/libstdc++.a(future.o): In function `std::future_category()':
future.cc:(.text._ZSt15future_categoryv+0xc): undefined reference to `__sync_synchronize'
collect2: error: ld returned 1 exit status
```

Provide -march or -mcpu option for the compiler to generate the respective output. 
> below command is using the compiler downloaded from OEM.

```bash
$ ./arm-none-eabi/bin/arm-none-eabi-g++ main.cpp -specs=nosys.specs -mcpu=cortex-m4

$ file a.out
a.out: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, with debug_info, not stripped
```

Since Cortex-M4 only supports the thumb instructions but not arm instructions we need to provide -mthumb option.

```bash
$ arm-none-eabi-g++ main.cpp -mcpu=cortex-m4 -specs=nosys.specs -mthumb
$ file a.out
a.out: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, with debug_info, not stripped
```

The atmel sam 4l ek board uses on-board debugger

https://www.segger.com/products/debug-probes/j-link/models/j-link-ob/

## Reference
[1] https://lynxbee.com/solved-compilation-error-for-arm-toolchain-undefined-reference-to-_exit/

https://blog.feabhas.com/2019/01/peripheral-register-access-using-c-structs-part-1/