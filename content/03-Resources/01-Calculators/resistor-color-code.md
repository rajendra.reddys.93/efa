﻿---
title: Resistance Color Code Calculator
notoc: true
summary: From the color code, find the resistance value.
---

> Enter color code and click **Calculate** to find other values.

{{<rawhtml>}}
<form>
    <label class="label">Select no of Color Bands below</label>
    <div class="control box">
        <label class="radio">
            <input id="r3Bands" type="radio" name="foobar" checked>
            3 - Bands
        </label>
        <label class="radio">
            <input id="r4Bands" type="radio" name="foobar">
            4 - Bands
        </label>
        <label class="radio">
            <input id="r5Bands" type="radio" name="foobar">
            5 - Bands
        </label>
        <label class="radio">
            <input id="r5Bands" type="radio" name="foobar">
            6 - Bands
        </label>
    </div>
    <div class="field-body">
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 1</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sBand1">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 2</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sSupVoltUnit">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 3</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sSupVoltUnit">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    <!-- </div>
    <div class="field-body"> -->
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 4</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sSupVoltUnit">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 5</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sSupVoltUnit">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    	<div class="field">
        	<label for="iSupVolt" class="label">Band 6</label>
            <p class="control is-expanded">
                <span class="select is-info">
                    <select id="sSupVoltUnit">
                        <option style="background-color:black; color:white;" selected>Black</option>
                        <option style="background-color:brown; color:white;">Brown</option>
                        <option style="background-color:red; color:white;">Red</option>
                        <option style="background-color:orange; color:white;">Orange</option>
                        <option style="background-color:yellow;">Yellow</option>
                        <option style="background-color:green; color:white;">Green</option>
                        <option style="background-color:blue; color:white;">Blue</option>
                        <option style="background-color:violet; color:white;">Violet</option>
                        <option style="background-color:gray; color:white;">Gray</option>
                        <option style="background-color:white;">White</option>
                        <option style="background-color:gold; color:white;">Gold</option>
                        <option style="background-color:silver; color:white;">Silver</option>
                        <option style="background-color:lightgray;">None</option>
                    </select>
                </span>
            </p>
        </div>
    </div>
    </br>
    <a type="button" class="button is-primary" onclick="calc()">Calculate</a>
</form>
{{<rawhtml>}}