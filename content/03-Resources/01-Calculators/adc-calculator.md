---
title: ADC Calculator
summary: Calculate Input Voltage, Reference Voltage & ADC Values.
notoc: true
---

<!-- markdownlint-disable MD033 -->

Input Voltage, Reference Voltage, ADC Resolution & ADC Values can be calculated using this utility.
> Enter any 2 values along with "ADC Resolution" and click "**Calculate**" to find other values.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iRefVolt" class="label">Reference Voltage</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iRefVolt" placeholder="Reference Voltage" value="2.5" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sRefVoltUnit">
                            <option>mV</option>
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iInVolt" class="label">Input Voltage</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iInVolt" placeholder="Input Voltage" value="2.5" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sInVoltUnit">
                            <option selected>mV</option>
                            <option>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iADCRes" class="label">ADC Resolution</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iADCRes" placeholder="ADC Resolution" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sADCResUnit">
                            <option selected>bits</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iADCVal" class="label">ADC Value</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iADCVal" placeholder="ADC Value" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sADCValUnit">
                            <option selected>DEC</option>
                            <option>HEX</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calc()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 3 values, ADC Resolution can't be caculated">}}

<script>
function calc() {
    var calcRefVolt = false;
    var calcInVolt = false;
    var calcADCRes = false;
    var calcADCVal = false;
    var noOfInputs = 0;
    var refVolt = document.getElementById("iRefVolt").value;
    var inVolt = document.getElementById("iInVolt").value;
    var adcRes = document.getElementById("iADCRes").value;
    var adcVal = document.getElementById("iADCVal").value;
    var refVoltMul = document.getElementById("sRefVoltUnit").selectedIndex;
    var inVoltMul = document.getElementById("sInVoltUnit").selectedIndex;
    var adcValMul = document.getElementById("sADCValUnit").selectedIndex;
    if (refVolt != null && refVolt != "" && refVolt != NaN) {
        refVolt = convertVoltageToNumber(refVolt, refVoltMul);
        calcRefVolt = false;
        noOfInputs += 1;
    } else {
        calcRefVolt = true;
    }
    if (inVolt != null && inVolt != "" && inVolt != NaN) {
        inVolt = convertVoltageToNumber(inVolt, inVoltMul);
        calcInVolt = false;
        noOfInputs += 1;
    } else {
        calcInVolt = true;
    }
    if (adcRes != null && adcRes != "" && adcRes != NaN) {
        calcADCRes = false;
        noOfInputs += 1;
    } else {
        calcADCRes = true;
        }
    if (adcVal != null && adcVal != "" && adcVal != NaN) {
        if (adcValMul == 1)
            adcVal = convertHexToDec(adcVal);
        calcADCVal = false;
        noOfInputs += 1;
    } else {
        calcADCVal = true;
        }
        if (noOfInputs < 3) {
            document.getElementById("errorModal").classList.add("is-active");
            } else {
        if (calcADCVal) {
            adcVal = ((Math.pow(2, adcRes) * inVolt) / refVolt).toFixed();
            if (adcValMul == 1)
                adcVal = convertDecToHex(adcVal);
            document.getElementById("iADCVal").value = adcVal;
        } else if (calcADCRes) {
            //adcRes = (Math.sqrt((adcVal * refVolt) / inVolt).toFixed()) / 5;
            //document.getElementById("iADCRes").value = adcRes;
        } else if (calcInVolt) {
            inVolt = (refVolt * adcVal) / Math.pow(2, adcRes);
            if (inVoltMul == 0)
                inVolt = (inVolt * 1000).toFixed(2);
            document.getElementById("iInVolt").value = inVolt;
        } else if (calcRefVolt) {
            refVolt = ((Math.pow(2, adcRes) * inVolt) / adcVal).toFixed(2);
            document.getElementById("iRefVolt").value = refVolt;
        }
    }
}
</script>
{{</rawhtml>}}

