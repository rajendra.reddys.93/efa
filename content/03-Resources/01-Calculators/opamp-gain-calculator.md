﻿---
title: Op-amp Gain Calculator
summary: Op-amp gain calculator.
notoc: true
---

> Enter input, feedback resistance and click **Calculate** to find other values.

{{<rawhtml>}}
<form>
    <label class="label">Select type of Inverter</label>
    <div class="control box">
        <label class="radio">
            <input id="rInverting" type="radio" name="foobar" checked>
            Inverting
        </label>
        <label class="radio">
            <input id="rNonInverting" type="radio" name="foobar">
            Non - Inverting
        </label>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance1" class="label">Input Resistor</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance1" placeholder="Input Resistor" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance1Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance2" class="label">Feedback Resistor</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance2" placeholder="Feedback Resistor" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance2Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label class="label">Gain</label>
		</div>
	  	<div class="field-body">
            <label id="ans" class="label"></label>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcOpAmpGain()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 2 values">}}

<script>
    function calcOpAmpGain() {
        var noOfInputs = 0;
        var res1 = document.getElementById("iResistance1").value;
        var res2 = document.getElementById("iResistance2").value;
        var res1Mul = document.getElementById("sResistance1Unit").selectedIndex;
        var res2Mul = document.getElementById("sResistance2Unit").selectedIndex;
        var isInverting = document.getElementById("rInverting").checked;
        var isNonInverting = document.getElementById("rNonInverting").checked;
        if (res1 != null && res1 != "" && res1 != NaN) {
            res1 = convertResistanceToNumber(res1, res1Mul);
            noOfInputs += 1;
        }
        if (res2 != null && res2 != "" && res2 != NaN) {
            res2 = convertResistanceToNumber(res2, res2Mul);
            noOfInputs += 1;
        }
        if (noOfInputs < 2) {
            document.getElementById("errorModal").classList.add("is-active");
        } else {
            if (isInverting == true) {
                document.getElementById("ans").innerText = "-" + res2 / res1;
            } else if (isNonInverting == true) {
                document.getElementById("ans").innerText = 1 + res2 / res1;
            } else {
                document.getElementById("errorModal").classList.add("is-active");
            }
        }
    }
</script>
{{</rawhtml>}}

