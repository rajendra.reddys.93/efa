﻿---
title: LED Resistance Calculator
notoc: true
summary: Findout the LED Resistance given the input voltage and current.
---

> Enter any 3 values and click "<b>Calculate</b>" to find other values.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iSupVolt" class="label">Supply Voltage</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iSupVolt" placeholder="Supply Voltage" value="2.5" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sSupVoltUnit">
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iVoltDrop" class="label">Voltage drop across LED</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iVoltDrop" placeholder="Voltage drop across LED" value="0.4" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sVoltDropUnit">
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iLedCrnt" class="label">Current through LED</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iLedCrnt" placeholder="Current through LED" value="100" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sLedCrntUnit">
                            <option>uA</option>
                            <option selected>mA</option>
                            <option>A</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResVal" class="label">Resistance</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResVal" placeholder="Resistance" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResUnit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iPowVal" class="label">Power</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iPowVal" placeholder="Power" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sPowUnit">
                            <option selected>Watts (W)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calc()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 2 values">}}
{{<rawhtml>}}
