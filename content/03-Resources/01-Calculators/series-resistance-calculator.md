﻿---
title: Series Resistance Calculator
notoc: true
summary: Given the resistor values calculate effective series resistance.
---

> Enter R1, R2 & R3 values and hit **Calculate** to find series resistance.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="resistance1" class="label">Resistor 1</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="resistance1" placeholder="Resistor 1" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="option1">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="resistance2" class="label">Resistor 2</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="resistance2" placeholder="Resistor 2" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="option2">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="resistance3" class="label">Resistor 3</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="resistance3" placeholder="Resistor 3" value="0.1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="option3">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label class="label">Series Resistance(R<sub>s</sub>)</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance" placeholder="" value="" type="number" readonly>
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistanceUnit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcSeriesResistance()">Calculate</a>
</form>
<script>
    function calcSeriesResistance() {
        var res1 = document.getElementById("resistance1").valueAsNumber;
        var res2 = document.getElementById("resistance2").valueAsNumber;
        var res3 = document.getElementById("resistance3").valueAsNumber;
        var res1option = document.getElementById("option1").selectedIndex;
        var res2option = document.getElementById("option2").selectedIndex;
        var res3option = document.getElementById("option3").selectedIndex;
        var ohmsMul = document.getElementById("sResistanceUnit").selectedIndex;
        res1 = convertResistanceToNumber(res1, res1option);
        res2 = convertResistanceToNumber(res2, res2option);
        res3 = convertResistanceToNumber(res3, res3option);
        document.getElementById("iResistance").value = convertNumberToResistance((res1 + res2 + res3), ohmsMul).toFixed(10);
    }
</script>
{{</rawhtml>}}

## Circuit Diagram

{{<img alt="resistors-in-series" src="/images/Basics/Resistor/resistors_in_series.png" cap="Resistors in Series">}}
