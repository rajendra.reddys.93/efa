﻿---
title: SMD Color Code Calculator
notoc: true
summary: From the SMD code, find the resistance value.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
        <div class="field">
            <label class="label">SMD Resistor Code</label>
            <p class="control">
        <input id="iSMDCode" type="text"
                style="font-family:'Times New Roman', Times, serif; font-weight:bold; font-size:60px; color:white;
border-left:50px solid; border-right:50px solid; border-top:0px; border-bottom:0px; border-color:gray; border-radius:2px;
background-color:black; width:250px; height:80px; text-align:center;"
                maxlength="4" value="12R3">
            </p>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcSMDCode()">Calculate</a>
</form>
</br>
<div class="form-group col-sm-12">
    <h3 id="message">
    </h3>
</div>
<script>
    //-----------------------------------------------------------------------------
    //NOTE:---Implemented only for values with R, need to implement for E-96 and other standard values
    //-------------------------------------------------------------------------
    function calcSMDCode() {
        var smdCode;
        var smdCodeLen;
        var firstIndexOfR;
        var lastIndexOfR;
        var firstIndexOfA;
        var firstIndexOfB;
        var firstIndexOfC;
        var firstIndexOfD;
        var firstIndexOfE;
        var firstIndexOfF;
        var firstIndexOfX;
        var firstIndexOfY;
        var firstIndexOfZ;
        var firstIndexOfH;
        var firstIndexOfS;
        var eia96Mul;
        var eia96Index;
        var eia96Values = [
            100, 102, 105, 107, 110, 113, 115, 118, 121, 124, 127, 130, 133, 137, 140, 143, 147, 150,
            154, 158, 162, 165, 169, 174, 178, 182, 187, 191, 196, 200, 205, 210, 215, 221, 226, 232,
            237, 243, 249, 255, 261, 267, 274, 280, 287, 294, 301, 309, 316, 324, 332, 340, 348, 357,
            365, 374, 383, 392, 402, 412, 422, 432, 442, 453, 464, 475, 487, 499, 511, 523, 536, 549,
            562, 576, 590, 604, 619, 634, 649, 665, 681, 698, 715, 732, 750, 768, 787, 806, 825, 845,
            866, 887, 909, 931, 953, 976];
        var digit3Code;
        var digit3Mul;
        var digit4Code;
        var digit4Mul;
        var valAtIndex0;
        var valAtIndex1;
        var valAtIndex2;
        var valAtIndex3;
        smdCode = document.getElementById("iSMDCode").value;
        smdCodeLen = smdCode.length;
        smdCode = smdCode.toUpperCase();
        document.getElementById("iSMDCode").value = smdCode;
        if (smdCodeLen < 3) {
            document.getElementById("message").innerText = "Invalid SMD Code";
        } else {
            //to check occurances of R
            firstIndexOfR = smdCode.indexOf("R");
            firstIndexOfA = smdCode.indexOf("A");
            firstIndexOfB = smdCode.indexOf("B");
            firstIndexOfC = smdCode.indexOf("C");
            firstIndexOfD = smdCode.indexOf("D");
            firstIndexOfE = smdCode.indexOf("E");
            firstIndexOfF = smdCode.indexOf("F");
            firstIndexOfX = smdCode.indexOf("X");
            firstIndexOfY = smdCode.indexOf("Y");
            firstIndexOfZ = smdCode.indexOf("Z");
            firstIndexOfH = smdCode.indexOf("H");
            firstIndexOfS = smdCode.indexOf("S");
            if (firstIndexOfR != -1) {
                //to check for multiple occurances of R, if exist - not valid code
                lastIndexOfR = smdCode.lastIndexOf("R");
                if ((lastIndexOfR - firstIndexOfR) > 0) {
                    document.getElementById("message").innerText = "Invalid SMD Code";
                } else {
                    //example - 100R
                    if (firstIndexOfR == 3 && smdCodeLen == 4)
                        document.getElementById("message").innerText = smdCode.replace("R", "") + " ohms";
                        //example - 10R
                    else if (firstIndexOfR == 2 && smdCodeLen == 3)
                        document.getElementById("message").innerText = smdCode.replace("R", "") + " ohms";
                        //example - R10
                    else if (firstIndexOfR == 0)
                        document.getElementById("message").innerText = smdCode.replace("R", "0.") + " ohms";
                        //example - 12R2
                    else
                        document.getElementById("message").innerText = smdCode.replace("R", ".") + " ohms";
                }
            } else if (smdCodeLen == 3 && ((firstIndexOfA != -1) || (firstIndexOfB != -1) ||
                (firstIndexOfC != -1) || (firstIndexOfD != -1) || (firstIndexOfE != -1) ||
                (firstIndexOfF != -1) || (firstIndexOfX != -1) || (firstIndexOfY != -1) ||
                (firstIndexOfZ != -1) || (firstIndexOfH != -1) || (firstIndexOfS != -1))) {
                //example - 01A
                if (firstIndexOfA != -1)
                    eia96Mul = 1;
                    //example - 01B or 01H
                else if ((firstIndexOfB != -1) || (firstIndexOfH != -1))
                    eia96Mul = 10;
                    //example - 01C
                else if (firstIndexOfC != -1)
                    eia96Mul = 100;
                    //example - 01D
                else if (firstIndexOfD != -1)
                    eia96Mul = 1000;
                    //example - 01E
                else if (firstIndexOfE != -1)
                    eia96Mul = 10000;
                    //example - 01F
                else if (firstIndexOfF != -1)
                    eia96Mul = 100000;
                    //example - 01X
                else if ((firstIndexOfX != -1) || (firstIndexOfS != -1))
                    eia96Mul = 0.1;
                    //example - 01Y
                else if (firstIndexOfY != -1)
                    eia96Mul = 0.01;
                    //example - 01Z
                else if (firstIndexOfZ != -1)
                    eia96Mul = 0.001;
                //first 2 digits of EIA96 code - "01"A
                eia96Index = smdCode.substr(0, 2);
                //first 2 digits can't be 00
                if (eia96Index == 00)
                    document.getElementById("message").innerText = "Invalid SMD Code";
                    //if first 2 digits is less than 10, remove first digit to get the index
                    //example - 01A, to get index read only second digit
                else {
                    if (eia96Index > 00 && eia96Index < 10)
                        eia96Index = smdCode.substr(1, 1);
                    //ohms = eia96values[index - 1] * multiplier;
                    document.getElementById("message").innerText = (appendMultiplierToResistance(eia96Values[eia96Index - 1] * eia96Mul)) + " ohms";
                }
            } else if (smdCodeLen == 3) {
                //example - 103
                //10 * 10 ^ 3
                digit3Code = smdCode.substr(0, 3);
                if (digit3Code >= 000 && digit3Code <= 999) {
                    digit3Code = smdCode.substr(0, 2);
                    digit3Mul = smdCode.substr(2, 1);
                    document.getElementById("message").innerText = (appendMultiplierToResistance(digit3Code * Math.pow(10, digit3Mul))) + " ohms";
                } else {
                    document.getElementById("message").innerText = "Invalid SMD Code";
                }
            } else if (smdCodeLen == 4) {
                //example - 1003
                //100 * 10 ^ 3
                digit4Code = smdCode.substr(0, 4);
                if (digit4Code >= 0000 && digit4Code <= 9999) {
                    digit4Code = smdCode.substr(0, 3);
                    digit4Mul = smdCode.substr(3, 1);
                    document.getElementById("message").innerText = (appendMultiplierToResistance(digit4Code * Math.pow(10, digit4Mul))) + " ohms";
                } else {
                    document.getElementById("message").innerText = "Invalid SMD Code";
                }
            }
            else {
                document.getElementById("message").innerText = "Invalid SMD Code";
            }
        }
    }
</script>
{{</rawhtml>}}
