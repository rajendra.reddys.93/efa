﻿---
title: Resistor ratio Calculator
notoc: true
summary: Resistaor ratio.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance1" class="label">Resistor 1 (R1)</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance1" placeholder="Resistor 1" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance1Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance2" class="label">Resistor 2 (R2)</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance2" placeholder="Resistor 2" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance2Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label class="label">Resistor Ratio (R2/R1)</label>
		</div>
	  	<div class="field-body">
            <label id="iResistance3" class="label"></label>
        </div>
    </div>
    <div class="field is-horizontal">
        <div class="field-label is-normal">
            <label class="checkbox">
                <input type="checkbox" id="isInverse">Inverse
            </label>
        </div>
	  	<div class="field-body">
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcResistorRatio()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 2 values">}}

<script>
    function calcResistorRatio() {
        var noOfInputs = 0;
        var ohms1 = document.getElementById("iResistance1").value;
        var ohms2 = document.getElementById("iResistance2").value;
        var ohms1Mul = document.getElementById("sResistance1Unit").selectedIndex;
        var ohms2Mul = document.getElementById("sResistance2Unit").selectedIndex;
        var isInverse = document.getElementById("isInverse").checked;
        if (ohms1 != null && ohms1 != "" && ohms1 != NaN) {
            ohms1 = convertResistanceToNumber(ohms1, ohms1Mul);
            noOfInputs += 1;
        } else {
            document.getElementById("modalMessage").innerText = "R1 value can't be empty.";
            document.getElementById("errorModal").classList.add("is-active");
        }
        if (ohms2 != null && ohms2 != "" && ohms2 != NaN) {
            ohms2 = convertResistanceToNumber(ohms2, ohms2Mul);
            noOfInputs += 1;
        } else {
            document.getElementById("modalMessage").innerText = "R2 value can't be empty.";
            document.getElementById("errorModal").classList.add("is-active");
        }
        if (noOfInputs < 2) {
            document.getElementById("modalMessage").innerText = "Provide values of R1 and R2.";
            document.getElementById("errorModal").classList.add("is-active");
        } else {
            ohms3 = ohms1 / ohms2;
            if (isInverse) {
                ohms3 = 1 / ohms3;
            }
            document.getElementById("iResistance3").innerText = ohms3.toFixed(10);
        }
    }
</script>
{{</rawhtml>}}
