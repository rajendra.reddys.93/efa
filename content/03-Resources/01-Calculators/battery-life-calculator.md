﻿---
title: Battery Life Calculator
summary: Battery life estimation based on battery capacity & load consumption.
notoc: true
---

<!-- markdownlint-disable MD033 -->

Battery life estimation based on battery capacity & load consumption.
> Enter any 2 values and click **Calculate** to find other values. The calculations are theoretical only. Battery life may vary in the practical conditions.
    
{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBatCap" class="label">Battery Capacity</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBatCap" placeholder="Battery Capacity" value="4000" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sBatCapUnit">
                            <option Selected>Milli-Amp-Hour (mAh)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDevCon" class="label">Device Consumption</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDevCon" placeholder="Device Consumption" value="100" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sDevConUnit">
                            <option selected>Milli-Amps (mA)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBatLif" class="label">Battery Life</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBatLif" placeholder="Battery Life" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sBatLifUnit">
                            <option selected>Hours (Hr)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcBatteryLife()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 2 values">}}

<script>
    function calcBatteryLife() {
        var batCap;
        var devCon;
        var batLif;
        var calcBatCap = false;
        var calcDevCon = false;
        var calcBatLif = false;
        var noOfInputs = 0;
        var batCap = document.getElementById("iBatCap").value;
        var devCon = document.getElementById("iDevCon").value;
        var batLif = document.getElementById("iBatLif").value;
        if (batCap != null && batCap != "" && batCap != NaN) {
            calcBatCap = false;
            noOfInputs += 1;
        } else { calcBatCap = true; }
        if (devCon != null && devCon != "" && devCon != NaN) {
            calcDevCon = false;
            noOfInputs += 1;
        } else { calcDevCon = true; }
        if (batLif != null && batLif != "" && batLif != NaN) {
            calcBatLif = false;
            noOfInputs += 1;
        } else { calcBatLif = true; }
        if (noOfInputs < 2) {
            document.getElementById("errorModal").classList.add("is-active");
        } else {
            if (calcBatCap) {
                batCap = devCon * batLif;
                document.getElementById("iBatCap").value = batCap.toFixed(4);
            } else if (calcDevCon) {
                devCon = batCap / batLif;
                document.getElementById("iDevCon").value = devCon.toFixed(4);
            } else if (calcBatLif) {
                batLif = batCap / devCon;
                document.getElementById("iBatLif").value = batLif.toFixed(4);
            }
        }
    }
</script>
{{</rawhtml>}}

