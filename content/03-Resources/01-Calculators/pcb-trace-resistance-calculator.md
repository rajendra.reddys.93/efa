﻿@{
    Layout = "~/Views/Shared/_CalculatorsLayout.cshtml";
}

@{
    ViewBag.Title = "PCB Trace Resistance Calculator";
}
<div class="row">
    <div class="body-content">
        <h3 class="section scrollspy">PCB Trace Resistance Calculator</h3>
        <form class="form-horizontal font-comic-sans">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Inputs</h1>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-3">Trace Width</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="tace width" aria-label="...">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control font-lucida-sans">
                                <option>inch</option>
                                <option selected="selected">mil</option>
                                <option>mm</option>
                                <option>cm</option>
                                <option>um</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3">Trace Length</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="trace length" aria-label="...">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control font-lucida-sans">
                                <option selected="selected">inch</option>
                                <option>mil</option>
                                <option>mm</option>
                                <option>cm</option>
                                <option>um</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3">Trace Thickness</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="trace thickness" aria-label="...">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control font-lucida-sans">
                                <option selected="selected">oz/ft^2</option>
                                <option>mil</option>
                                <option>mm</option>
                                <option>um</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3">Temperature</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="ambient temperature" aria-label="...">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control font-lucida-sans">
                                <option selected="selected">Deg C</option>
                                <option>Deg F</option>
                                <option>Deg K</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <button class="btn btn-info col-sm-4 col-sm-offset-4">Calculate</button>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Outputs</h1>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-3">Resistance</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="resistance" aria-label="...">
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control font-lucida-sans">
                                <option>&#x2126;</option>
                                <option selected="selected">K&#x2126;</option>
                                <option>M&#x2126;</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>