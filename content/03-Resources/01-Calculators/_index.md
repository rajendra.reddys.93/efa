---
title: "Calculators"
date: 2021-04-29
publishdate: 2019-12-20
weight: 1
summary: Ohm's law, Voltage divider, Series & Parallel, etc.
type: tool
slug: calculators
menu:
    main:
        parent: "Resources"
---

Ohm's law, Voltage divider, Series & Parallel, etc.
