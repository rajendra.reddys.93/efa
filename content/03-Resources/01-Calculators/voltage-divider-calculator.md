﻿---
title: Voltage Divider
weight: 2
summary: Calculate output voltage divided across load resistor.
---

<!-- markdownlint-disable MD033 -->
> Enter any 3 values and click "**Calculate**" to find other values.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iVoltageIn" class="label">Input Voltage</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iVoltageIn" placeholder="Input Voltage" value="2.5" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sVolatgeInUnit">
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance1" class="label">Resistor 1</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance1" placeholder="Resistor 1" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance1Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance2" class="label">Resistor 2</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance2" placeholder="Resistor 2" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistance2Unit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iVoltageOut" class="label">Output Voltage</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iVoltageOut" placeholder="Output Voltage" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sVolatgeOutUnit">
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calc()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 3 values">}}

<script>
function calc() {
    var calcVoltsIn = false;
    var calcVoltsOut = false;
    var calcOhms1 = false;
    var calcOhms2 = false;
    var noOfInputs = 0;
    var voltsIn = document.getElementById("iVoltageIn").value;
    var voltsOut = document.getElementById("iVoltageOut").value;
    var ohms1 = document.getElementById("iResistance1").value;
    var ohms2 = document.getElementById("iResistance2").value;
    var ohms1Mul = document.getElementById("sResistance1Unit").selectedIndex;
    var ohms2Mul = document.getElementById("sResistance2Unit").selectedIndex;
    if (voltsIn != null && voltsIn != "" && voltsIn != NaN) {
        calcVoltsIn = false;
        noOfInputs += 1;
    } else {
        calcVoltsIn = true;
    }
    if (voltsOut != null && voltsOut != "" && voltsOut != NaN) {
        calcVoltsOut = false;
        noOfInputs += 1;
    } else {
        calcVoltsOut = true;
    }
    if (ohms1 != null && ohms1 != "" && ohms1 != NaN) {
        ohms1 = convertResistanceToNumber(ohms1, ohms1Mul);
        calcOhms1 = false;
        noOfInputs += 1;
    } else {
        calcOhms1 = true;
    }
    if (ohms2 != null && ohms2 != "" && ohms2 != NaN) {
        ohms2 = convertResistanceToNumber(ohms2, ohms2Mul);
        calcOhms2 = false;
        noOfInputs += 1;
    } else {
        calcOhms2 = true;
    }
    if (noOfInputs < 3) {
        document.getElementById("errorModal").classList.add("is-active");
    } else {
        if (calcVoltsIn) {
            voltsIn = ((voltsOut * (ohms1 + ohms2)) / ohms2);
            document.getElementById("iVoltageIn").value = voltsIn.toFixed(10);
        } else if (calcVoltsOut) {
            voltsOut = ((voltsIn * ohms2) / (ohms1 + ohms2));
            document.getElementById("iVoltageOut").value = voltsOut.toFixed(10);
        } else if (calcOhms1) {
            ohms1 = ((ohms2 * (voltsIn - voltsOut)) / voltsOut);
            ohms1 = convertNumberToResistance(ohms1, ohms1Mul);
            document.getElementById("iResistance1").value = ohms1.toFixed(10);
        } else if (calcOhms2) {
            ohms2 = ((ohms1 * voltsOut) / (voltsIn - voltsOut));
            ohms2 = convertNumberToResistance(ohms2, ohms2Mul);
            document.getElementById("iResistance2").value = ohms2.toFixed(10);
        }
    }
}
</script>
{{</rawhtml>}}

## Circuit Diagram

{{<img src="/images/Calculators/VoltageDivider/VoltageDivider.png" alt="voltage-divider" cap="Voltage Divider">}}

The above circuit can be used to explain concept of Voltage Division across Resistors.
The Input Voltage (V<sub>in</sub>) will be divided across Resistors R1 and R2 based on
their resistance.

## How To Use

The Voltage Divider Calculator is used to find the voltage across the load when placed
in series with a current limiting resistor.
In the above calculator, enter any three values and click calculate to update other values.
You can select unit from the drop-down list against to the input field of the parameter
and enter the value in the text input field. Enter only know values, leave other
values blank which you want to find and hit calculate, it'll update the other values.

The calculator accepts scientific notations such as "e" or "E" (meaning exponent).
Since, it is not possible to show the results as accurate as
natural, we are rounding off the results to 10 decimal places. This means that the
results will be rounded to avoid numbers getting too long.

## Calculation Explanation

Voltage Division is practically used in measuring high voltage using Micro-controller's ADC channels. We cannot feed high voltage (24V) directly to the controller port pins. So we will bring down the high voltage to low voltage (3.3V or 5V) which we can directly give to controller pin. You can refer to below [example](example-convertion) calculation where we will get 24V down to 3.3V using Resistors.

## Formula Explanation

We are using below formulas in our calculator. To the best of our knowledge, we hope that the calculations we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below is the formula behind voltage divider circuit

### Equations to calculate Output Voltage (V<sub>out</sub>)

{{<formula>}}
V<sub>out</sub> = <table class="frac"><tr><td>R2<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />R1 + R2</td></tr></table> V<sub>in</sub>
{{</formula>}}

Further we can derive below formulas from above equation.

### Equations to calculate Input Voltage (V<sub>in</sub>)

{{<formula>}}
V<sub>in</sub> = <table class="frac"><tr><td>R1 + R2<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />R1</td></tr></table> V<sub>out</sub>
{{</formula>}}

### Equations to calculate Resistance R1

{{<formula>}}
R1 = <table class="frac"><tr><td>V<sub>in</sub> - V<sub>out</sub><br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />V<sub>out</sub></td></tr></table>R2
{{</formula>}}

### Equations to calculate Resistance R2

{{<formula>}}
R2 = <table class="frac"><tr><td>V<sub>out</sub><br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />V<sub>in</sub> - V<sub>out</sub></td></tr></table>R1
{{</formula>}}

## Example Calculation

In the below example,
we will calculate Resistor R1 and Resistor R2 values to get Input Voltage (V<sub>in</sub>) 
24V to Output Voltage (V<sub>out</sub>) 3.3V
From the above formula list we have,

{{<formula>}}
R2 = <table class="frac"><tr><td>V<sub>out</sub><br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />V<sub>in</sub> - V<sub>out</sub></td></tr></table>R1
{{</formula>}}

We will consider R1 as 10K&Omega; and calculate R2.

{{<formula>}}
R2 = <table class="frac"><tr><td>3.3V<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013<br />24V - 3.3V</td></tr></table>10K&Omega;
{{</formula>}}

{{<formula>}}
R2 = <table class="frac"><tr><td>3.3<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013<br />24 - 3.3</td></tr></table>10000
{{</formula>}}

{{<formula>}}
R2 = 1594.2&Omega; = 1.594K&Omega;
{{</formula>}}

We can use 1.58K&Omega;, which is [standard resistor value](/resources/standards/standard-resistor-values/). Let us find output voltage for these resistor values. Substitute R1, R2 and V<sub>in</sub> in below equation.

{{<formula>}}
V<sub>out</sub> = <table class="frac"><tr><td>R2<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />R1 + R2</td></tr></table> V<sub>in</sub>
{{</formula>}}

{{<formula>}}
V<sub>out</sub> = <table class="frac"><tr><td>1.58K<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />10K + 1.58K</td></tr></table>24V
{{</formula>}}

{{<formula>}}
V<sub>out</sub> = 3.2746V
{{</formula>}}

{{<rawhtml>}}
V<sub>in</sub> = 24V, <br/>
V<sub>out</sub> = 3.2746V, <br/>
R1 = 10K&Omega;, and <br/>
R2 = 1.58K&Omega; <br />
{{</rawhtml>}}

There is no harm to controller giving 3.27V to its port pins.
