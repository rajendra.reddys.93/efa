﻿---
title: Ohm's Law Calculator
weight: 1
summary: Calculate volatge, current and resistance given other 2, and find also power.
---

<!-- markdownlint-disable MD033 -->

> Enter any 2 values and click "**Calculate**" to update other values.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iVoltage" class="label">Voltage</label>
		</div>
        	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iVoltage" placeholder="Voltage (V)" value="2.5" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sVoltDropUnit">
                            <option selected>V</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCurrent" class="label">Current</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCurrent" placeholder="Current (I)" value="100" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sCurrentUnit">
                            <option>uA</option>
                            <option selected>mA</option>
                            <option>A</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iResistance" class="label">Resistance</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iResistance" placeholder="Resistance (R)" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sResistanceUnit">
                            <option>&#x2126;</option>
                            <option selected>K&#x2126;</option>
                            <option>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iPower" class="label">Power</label>
		</div>
        <div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iPower" placeholder="Power (P)" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sPowUnit">
                            <option selected>Watts (W)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calc()">Calculate</a>
</form>

<!-- Modal -->
{{<modal id="errorModal" button="OK" title="Note!" message="Enter atlease 2 values">}}

<script>
function calc() {
    var calcVolts = false;
    var calcAmps = false;
    var calcOhms = false;
    var calcWatts = false;
    var noOfInputs = 0;
    var volts = document.getElementById("iVoltage").value;
    var amps = document.getElementById("iCurrent").value;
    var ohms = document.getElementById("iResistance").value;
    var watts = document.getElementById("iPower").value;
    var ohmsMul = document.getElementById("sResistanceUnit").selectedIndex;
    var ampsMul = document.getElementById("sCurrentUnit").selectedIndex;
    if (volts != null && volts != "" && volts != NaN) {
        calcVolts = false;
        noOfInputs += 1;
    } else { calcVolts = true; }
    if (amps != null && amps != "" && amps != NaN) {
        amps = convertCurrentToNumber(amps, ampsMul);
        calcAmps = false;
        noOfInputs += 1;
    } else { calcAmps = true; }
    if (ohms != null && ohms != "" && ohms != NaN) {
        ohms = convertResistanceToNumber(ohms, ohmsMul);
        calcOhms = false;
        noOfInputs += 1;
    } else { calcOhms = true; }
    if (watts != null && watts != "" && watts != NaN) {
        calcWatts = false;
        noOfInputs += 1;
    } else { calcWatts = true; }
    if (noOfInputs < 2) {
        document.getElementById("errorModal").classList.add("is-active");
    }
    else {
        if (calcWatts && calcOhms) {
            watts = volts * amps;
            ohms = volts / amps;
            ohms = convertNumberToResistance(ohms, ohmsMul);
            document.getElementById("iResistance").value = ohms.toFixed(10);
            document.getElementById("iPower").value = watts.toFixed(10);
        }
        else if (calcWatts && calcAmps) {
            watts = Math.pow(volts, 2) * ohms;
            amps = volts / ohms;
            amps = convertNumberToCurrent(amps, ampsMul);
            document.getElementById("iCurrent").value = amps.toFixed(10);
            document.getElementById("iPower").value = watts.toFixed(10);
        }
        else if (calcWatts && calcVolts) {
            watts = Math.pow(amps, 2) * ohms;
            volts = ohms * amps;
            document.getElementById("iVoltage").value = volts.toFixed(10);
            document.getElementById("iPower").value = watts.toFixed(10);
        }
        else if (calcOhms && calcAmps) {
            ohms = Math.pow(volts, 2) * watts;
            amps = watts / volts;
            ohms = convertNumberToResistance(ohms, ohmsMul);
            document.getElementById("iResistance").value = ohms.toFixed(10);
            amps = convertNumberToCurrent(amps, ampsMul);
            document.getElementById("iCurrent").value = amps.toFixed(10);
        }
        else if (calcOhms && calcVolts) {
            volts = watts / amps;
            ohms = watts / Math.pow(amps, 2);
            ohms = convertNumberToResistance(ohms, ohmsMul);
            document.getElementById("iResistance").value = ohms.toFixed(10);
            document.getElementById("iVoltage").value = volts.toFixed(10);
        }
        else if (calcVolts && calcAmps) {
            volts = Math.sqrt((watts * ohms), 2);
            amps = Math.sqrt((watts / ohms), 2);
            amps = convertNumberToCurrent(amps, ampsMul);
            document.getElementById("iCurrent").value = amps.toFixed(10);
            document.getElementById("iVoltage").value = volts.toFixed(10);
        }
    }
}
</script>
{{</rawhtml>}}

## Circuit Diagram

{{<img alt="ohms-law" src="/images/Basics/OhmsLaw/OhmsLaw.png" cap="Ohm's Law">}}

The above circuit can be used to explain Ohm's Law. Ohm's Law states that the
voltage (V) across a resistor is proportional to the current (I), where the constant
of proportionality is the resistance (R). i.e.

{{<rawhtml>}}
<div class="inlineformula numfor">
    <h3 class="numforleft">
        V = I * R
    </h3>
</div>
<p>
    where <br />
    <label>
    V - Voltage <br />
    I - Current <br />
    R - Resistance
    </label>
</p>
{{</rawhtml>}}

## How To Use

The Ohm's Law Calculator is the one of fundamental calculator used by any electrical engineer as it is one of the basic and important calculation used in any circuit. You can read more about (~/learn/electronics/basics/ohmslaw) under [Electronics](~/electronics) in Education. In the above calculator, enter any two values and click calculate to update other values. You can select unit from the drop-down list against to the input field of the parameter and enter the value in the text input field. Enter only know values, leave other values blank which you want to find and hit calculate, it'll update the other values.

The calculator accepts scientific notations such as "e" or "E" (meaning exponent). Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Calculation Explanation

Ohm's Law is being widely used in analyzing any circuit which constitutes a conductor with certain resistance and electricity flowing through it. Ohm's is used by a learner to find current limiting resistor in his first circuit or by an engineer working on an rocket design.

## Formula Explanation

We are using below formulas in our calculator. To the best of our knowledge, we hope that the calculations we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

The below pie chart shows relation ship between Voltage (V), Current (I), Resistance (R) and Power (P).

{{<img alt="ohms-law-wheel" src="/images/Basics/OhmsLaw/ohms-law-wheel.png" cap="Ohm's Law Pie Chart">}}

You can derive below formulas from Ohm's Law

### Equations to calculate Voltage (V)

{{<formula>}}
V = I * R
{{</formula>}}

{{<formula>}}
V = <table class="frac"><tr><td>P<br />&#x2013;&#x2013;&#x2013;<br />I</td></tr></table>
{{</formula>}}

{{<formula>}}
V = &radic;(P * R)
{{</formula>}}

### Equations to calculate Current (C)

{{<formula>}}
I = <table class="frac"><tr><td>V<br />&#x2013;&#x2013;&#x2013;<br />R</td></tr></table>
{{</formula>}}

{{<formula>}}
I = <table class="frac"><tr><td>P<br />&#x2013;&#x2013;&#x2013;<br />V</td></tr></table>
{{</formula>}}

{{<formula>}}
I = <table class="comb"><tr><td class="bigmiddle">&radic;(<td class="bincoef"><table class="frac"><tr><td>P<br />&#x2013;&#x2013;&#x2013<br />R</td></tr></table><br />&nbsp;<td class="bigmiddle">)</table>
{{</formula>}}

### Equations to calculate Resistance (R)

{{<formula>}}
R = <table class="frac"><tr><td>V<br />&#x2013;&#x2013;&#x2013;<br />I</td></tr></table>
{{</formula>}}

{{<formula>}}
R = <table class="frac"><tr><td>V<sup>2</sup><br />&#x2013;&#x2013;&#x2013;<br />P</td></tr></table>
{{</formula>}}

{{<formula>}}
R = <table class="frac"><tr><td>P<br />&#x2013;&#x2013;&#x2013;<br />I<sup>2</sup></td></tr></table>
{{</formula>}}

### Equations to calculate Power (P)

{{<formula>}}
P = V * I
{{</formula>}}

{{<formula>}}
P = R * I<sup>2</sup>
{{</formula>}}

{{<formula>}}
P = <table class="frac"><tr><td>V<sup>2</sup><br />&#x2013;&#x2013;&#x2013;<br />R</td></tr></table>
{{</formula>}}

## Example Calculation

In the below example, we will calculate Power (P) and Current (I) for Input Voltage (V) - 12V and Resistance (R) - 1KΩ. Let us calculate Current (I) first.
From the above formula list we have,

{{<formula>}}
I = <table class="frac"><tr><td>V<br />&#x2013;&#x2013;&#x2013;<br />R</td></tr></table>
= <table class="frac"><tr><td>12V<br />&#x2013;&#x2013;&#x2013;<br />1K&Omega;</td></tr></table>
= <table class="frac"><tr><td>12V<br />&#x2013;&#x2013;&#x2013;<br />1000</td></tr></table>
= 0.012 = 12mA
{{</formula>}}

Now we know Current (I) and Voltage (V), let us calculate Power (P) using them.

{{<formula>}}
P = V * I <br />
= 12V * 12mA <br />
= 12 * 0.012 <br />
= 0.144W  <br />
= 144mW
{{</formula>}}

{{<rawhtml>}}
V = 12V, <br />
I = 12mA, <br />
R = 1K&Omega;, and <br />
P = 144mW <br />
{{</rawhtml>}}
