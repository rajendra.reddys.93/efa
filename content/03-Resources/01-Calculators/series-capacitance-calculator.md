﻿---
title: Series Capacitance Calculator
notoc: true
summary: Given the capacitor values calculate effective series capacitance.
---

> Enter C1, C2 & C3 values and hit **Calculate** to find series capacitance.

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCapacitance1" class="label">Capacitor 1</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCapacitance1" placeholder="Capacitor 1" value="100" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sCap1Unit">
                            <option>F</option>
                            <option>mF</option>
                            <option selected>uF</option>
                            <option>nF</option>
                            <option>pF</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCapacitance2" class="label">Capacitor 2</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCapacitance2" placeholder="Capacitor 2" value="1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sCap2Unit">
                            <option>F</option>
                            <option>mF</option>
                            <option selected>uF</option>
                            <option>nF</option>
                            <option>pF</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCapacitance3" class="label">Capacitor 3</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCapacitance3" placeholder="Capacitor 3" value="0.1" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sCap3Unit">
                            <option>F</option>
                            <option>mF</option>
                            <option selected>uF</option>
                            <option>nF</option>
                            <option>pF</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label class="label">Series Capacitance</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCapacitance" placeholder="" value="" type="number" readonly>
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sCapacitanceUnit">
                            <option>F</option>
                            <option>mF</option>
                            <option selected>uF</option>
                            <option>nF</option>
                            <option>pF</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <a type="button" class="button is-primary" onclick="calcSeriesCapacitance()">Calculate</a>
</form>

<!-- <script src="~/Scripts/CalculatorAndConverter/CalculatorAndConverter.js"></script> -->
<script>
    function calcSeriesCapacitance() {
        var cap1 = document.getElementById("iCapacitance1").valueAsNumber;
        var cap2 = document.getElementById("iCapacitance2").valueAsNumber;
        var cap3 = document.getElementById("iCapacitance3").valueAsNumber;
        var cap1Mul = document.getElementById("sCap1Unit").selectedIndex;
        var cap2Mul = document.getElementById("sCap2Unit").selectedIndex;
        var cap3Mul = document.getElementById("sCap3Unit").selectedIndex;
        var capSeriesMul = document.getElementById("sCapacitanceUnit").selectedIndex;
        cap1 = convertCapacitanceToNumber(cap1, cap1Mul);
        cap2 = convertCapacitanceToNumber(cap2, cap2Mul);
        cap3 = convertCapacitanceToNumber(cap3, cap3Mul);
        document.getElementById("iCapacitance").value = convertNumberToCapacitance((1 / ((1 / cap1) + (1 / cap2) + (1 / cap3))), capSeriesMul).toFixed(10);
    }
</script>
{{</rawhtml>}}

## Circuit Diagram

{{<img alt="capacitors-in-series" src="/images/Basics/Capacitor/capacitors_in_series.png" cap="Capacitors in Series">}}

