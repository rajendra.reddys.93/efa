﻿---
title: Raspberry PI board Pin-outs
---
{{<rawhtml>}}
        <form class="form-horizontal">
            <div class="row">
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/Raspberry_Pi_Zero.png" alt="Pi Zero">
                        <div class="caption">
                            <h4 class="caption-center">Pi Zero</h4>
                            <p>Location of connectors and main ICs</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/Raspberry_Pi_1A.png" alt="Model 1A">
                        <div class="caption">
                            <h4 class="caption-center">Model 1A</h4>
                            <p>Location of connectors and main ICs on Raspberry Pi 1 Model A</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/Raspberry_Pi_model_B_rev2.png" alt="Model 1B">
                        <div class="caption">
                            <h4 class="caption-center">Model 1B</h4>
                            <p>Location of connectors and main ICs on Raspberry Pi 1 Model B revision 1.2</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/Raspberry_Pi_model_A_plus_rev1.1.png" alt="Model 1A+">
                        <div class="caption">
                            <h4 class="caption-center">Model 1A+</h4>
                            <p>Location of connectors and main ICs on Raspberry Pi 1 Model A+ revision 1.1</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/Raspberry_Pi_B_plus_rev_1.2.png" alt="Model 2">
                        <div class="caption">
                            <h4 class="caption-center">Model 2</h4>
                            <p>Location of connectors and main ICs on Raspberry Pi 1 Model B+ revision 1.2 and Raspberry Pi 2</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/RaspberryPi/RaspberryPi_3B.png" alt="Model 3">
                        <div class="caption">
                            <h4 class="caption-center">Model 3</h4>
                            <p>Location of connectors and main ICs on Raspberry Pi 3</p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form class="form-horizontal font-comic-sans">
            <div class="table-responsive text-center">
                <table class="centered highlight">
                    <thead>
                        <tr>
                            <th style="text-align:center;">GPIO#</th>
                            <th style="text-align:center;">2nd func.</th>
                            <th style="text-align:center;">Pin#</th>
                            <th style="text-align:center;"> </th>
                            <th style="text-align:center;">Pin#</th>
                            <th style="text-align:center;">2nd func.</th>
                            <th style="text-align:center;">GPIO#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>+3.3 V</td>
                            <td>1</td>
                            <td> </td>
                            <td>2</td>
                            <td>+5 V</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SDA1 (I²C)</td>
                            <td>3</td>
                            <td> </td>
                            <td>4</td>
                            <td>+5 V</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>SCL1 (I²C)</td>
                            <td>5</td>
                            <td> </td>
                            <td>6</td>
                            <td>GND</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>GCLK</td>
                            <td>7</td>
                            <td> </td>
                            <td>8</td>
                            <td>TXD0 (UART)</td>
                            <td>14</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>GND</td>
                            <td>9</td>
                            <td> </td>
                            <td>10</td>
                            <td>RXD0 (UART)</td>
                            <td>15</td>
                        </tr>
                        <tr>
                            <td>17</td>
                            <td>GEN0</td>
                            <td>11</td>
                            <td> </td>
                            <td>12</td>
                            <td>GEN1</td>
                            <td>18</td>
                        </tr>
                        <tr>
                            <td>27</td>
                            <td>GEN2</td>
                            <td>13</td>
                            <td> </td>
                            <td>14</td>
                            <td>GND</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>22</td>
                            <td>GEN3</td>
                            <td>15</td>
                            <td> </td>
                            <td>16</td>
                            <td>GEN4</td>
                            <td>23</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>+3.3 V</td>
                            <td>17</td>
                            <td> </td>
                            <td>18</td>
                            <td>GEN5</td>
                            <td>24</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>MOSI (SPI)</td>
                            <td>19</td>
                            <td> </td>
                            <td>20</td>
                            <td>GND</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>MISO (SPI)</td>
                            <td>21</td>
                            <td> </td>
                            <td>22</td>
                            <td>GEN6</td>
                            <td>25</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>SCLK (SPI)</td>
                            <td>23</td>
                            <td> </td>
                            <td>24</td>
                            <td>CE0_N (SPI)</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>GND</td>
                            <td>25</td>
                            <td> </td>
                            <td>26</td>
                            <td>CE1_N (SPI)</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td colspan="7">(Pi 1 Models A and B stop here)</td>
                        </tr>
                        <tr>
                            <td>EEPROM</td>
                            <td>ID_SD</td>
                            <td>27</td>
                            <td> </td>
                            <td>28</td>
                            <td>ID_SC</td>
                            <td>EEPROM</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>N/A</td>
                            <td>29</td>
                            <td> </td>
                            <td>30</td>
                            <td>GND</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>N/A</td>
                            <td>31</td>
                            <td> </td>
                            <td>32</td>
                            <td></td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>N/A</td>
                            <td>33</td>
                            <td> </td>
                            <td>34</td>
                            <td>GND</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19</td>
                            <td>N/A</td>
                            <td>35</td>
                            <td> </td>
                            <td>36</td>
                            <td>N/A</td>
                            <td>16</td>
                        </tr>
                        <tr>
                            <td>26</td>
                            <td>N/A</td>
                            <td>37</td>
                            <td> </td>
                            <td>38</td>
                            <td>Digital IN</td>
                            <td>20</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>GND</td>
                            <td>39</td>
                            <td> </td>
                            <td>40</td>
                            <td>Digital OUT</td>
                            <td>21</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
 {{</rawhtml>}}