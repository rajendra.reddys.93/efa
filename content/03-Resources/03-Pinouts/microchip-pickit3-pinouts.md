﻿---
title: Microchip Pickit-3 pinouts
----
{{<rawhtml>}}
        <form class="form-horizontal">
            <div class="row">
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/Pickit3/PICkit3-pinouts.png" alt="pickit-3 pinouts">
                        <div class="caption">
                            <p class="text-center">Microchip Pickit-3</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail">
                        <img src="~/Images/Pinouts/Pickit3/PICKit-ICSP-pinouts.png" alt="pickit-3 ICSP pinouts">
                        <div class="caption">
                            <p class="text-center">Pickit-3 pinouts</p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        {{</rawhtml>}}