---
title: "Pin Outs"
date: 2021-04-29T19:13:22+05:30
publishdate: 2019-12-19T19:13:22+05:30
weight: 3
summary: Pinout of different connecting ports & devices.
type: resources
slug: pinouts
menu:
    main:
        parent: "Resources"
---

Pinout of different connecting ports & devices.
