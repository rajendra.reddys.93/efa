﻿---
title: HDMI Connector Pinouts
---

{{<rawhtml>}}
<div class="row">
    <div class="body-content">
        <h1 class="section scrollspy">HDMI Type A Pin-outs</h1>
        <p>Pin-outs remain same for Type A(standard), C(mini), D(micro) and E(automotive)</p>
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail">
                    <img src="~/Images/Pinouts/HDMI/HDMI_TypeA_Connector_Pinout.png" alt="HDMI Type A female">
                    <div class="caption">
                        <p class="text-center">HDMI type A receptacle (female)</p>
                    </div>
                </div>
            </div>
            <form class="form-horizontal font-comic-sans">
                <div class="table-responsive text-center">
                    <table class="centered highlight">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Pin#</th>
                                <th style="text-align:center;">Signal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>TMDS Data2+</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>TMDS Data2 Shield</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>TMDS Data2-</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>TMDS Data1+</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>TMDS Data1 Shield</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>TMDS Data1-</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>TMDS Data0+</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>TMDS Data0 Shield</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>TMDS Data0-</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>TMDS Clock+</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>TMDS Clock Shield</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>TMDS Clock-</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>CEC</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>Reserved/HEC Data−</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>SCL (I²C serial clock for DDC)</td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>SDA (I²C serial data for DDC)</td>
                            </tr>
                            <tr>
                                <td>17</td>
                                <td>Ground (for DDC, CEC, ARC, and HEC)</td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>+5 V</td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>Hot Plug Detect/HEC Data+</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
{{</rawhtml>}}