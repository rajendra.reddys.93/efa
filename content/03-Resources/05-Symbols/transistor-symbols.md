﻿---
title: Transistor Symbols
weight: 9
notoc: true
summary: NPN, PNP, Darlington & MOSFET.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/NPN-BJT-transistor.png"></td>
                    <td>NPN Bipolar Transistor</td>
                    <td>Allows current flow when high potential at base (middle)</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/PNP-BJT-transistor.png"></td>
                    <td>PNP Bipolar Transistor</td>
                    <td>Allows current flow when low potential at base (middle)</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/Darlington-transistor.png"></td>
                    <td>Darlington Transistor</td>
                    <td>Made from 2 bipolar transistors. Has total gain of the product of each gain.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/JFET-N-transistor.png"></td>
                    <td>JFET-N Transistor</td>
                    <td>N-channel field effect transistor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/JFET-P-transistor.png"></td>
                    <td>JFET-P Transistor</td>
                    <td>P-channel field effect transistor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/NMOS-transistor.png"></td>
                    <td>NMOS Transistor</td>
                    <td>N-channel MOSFET transistor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/TransistorSymbols/PMOS-transistor.png"></td>
                    <td>PMOS Transistor</td>
                    <td>P-channel MOSFET transistor</td>
                </tr>
            </tbody>
        </table>
        <br />
        {{</rawhtml>}}