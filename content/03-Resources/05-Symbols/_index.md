---
title: "Symbols"
date: 2021-04-29T19:13:22+05:30
publishdate: 2019-12-19T19:13:22+05:30
weight: 5
summary: Symbols of different components/devices used in electronics.
type: resources
slug: symbols
menu:
    main:
        parent: "Resources"
---

Symbols of different components/devices used in electronics.
