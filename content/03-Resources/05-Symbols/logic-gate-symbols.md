﻿---
title: Logic Gate Symbols
weight: 12
notoc: true
summary: Gates, Flip-flops & Multiplexers.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/NOT-gate.png"></td>
                    <td>NOT Gate (Inverter)</td>
                    <td>Outputs 1 when input is 0</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/AND-gate.png"></td>
                    <td>AND Gate</td>
                    <td>Outputs 1 when both inputs are 1.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/NAND-gate.png"></td>
                    <td>NAND Gate</td>
                    <td>Outputs 0 when both inputs are 1. (NOT + AND)</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/OR-gate.png"></td>
                    <td>OR Gate</td>
                    <td>Outputs 1 when any input is 1.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/NOR-gate.png"></td>
                    <td>NOR Gate</td>
                    <td>Outputs 0 when any input is 1. (NOT + OR)</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/XOR-gate.png"></td>
                    <td>XOR Gate</td>
                    <td>Outputs 1 when inputs are different. (Exclusive OR)</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/D-flip-flop.png"></td>
                    <td>D Flip-Flop</td>
                    <td>Stores one bit of data</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/mux-2-to-1.png"></td>
                    <td>Multiplexer / Mux 2 to 1</td>
                    <td>Connects the output to  selected input line.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/mux-4-to-1.png"></td>
                    <td>Multiplexer / Mux 4 to 1</td>
                    <td>Connects the output to  selected input line.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/LogicGateSymbols/demux-1-to-4.png"></td>
                    <td>De-multiplexer / Demux 1 to 4</td>
                    <td>Connects selected output to the input line.</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}