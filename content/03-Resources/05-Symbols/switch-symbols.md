﻿---
title: Switch & Relay Symbols
weight: 2
notoc: true
summary: SPST, SPDT, Relay, Jumper & Push button.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/SPST-toggle-switch.png"></td>
                    <td>SPST Toggle Switch</td>
                    <td>Disconnects current when open</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/SPDT-toggle-switch.png"></td>
                    <td>SPDT Toggle Switch</td>
                    <td>Selects between two connections</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/push-button-switch-NO.png"></td>
                    <td>Pushbutton Switch (N.O)</td>
                    <td>Momentary switch - normally open</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/push-button-switch-NC.png"></td>
                    <td>Pushbutton Switch (N.C)</td>
                    <td>Momentary switch - normally closed</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/dip-switch.png"></td>
                    <td>DIP Switch</td>
                    <td>DIP switch is used for onboard configuration</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/relay-spst.png"></td>
                    <td>SPST Relay</td>
                    <td>Relay open / close connection by an electromagnet</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/relay-spdt.png"></td>
                    <td>SPDT Relay</td>
                    <td>Relay open / close connection by an electromagnet</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/jumper.png"></td>
                    <td>Jumper</td>
                    <td>Close connection by jumper insertion on pins.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/SwitchSymbols/solder-bridge.png"></td>
                    <td>Solder Bridge</td>
                    <td>Solder to close connection</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}
