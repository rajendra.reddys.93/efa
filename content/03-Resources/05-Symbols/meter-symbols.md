﻿---
title: Meter Symbols
weight: 10
notoc: true
summary: Voltmeter, Ammeter, Ohmeter & Wattmeter.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MeterSymbols/voltmeter.png"></td>
                    <td>Voltmeter</td>
                    <td>Measures voltage. Has very high resistance. Connected in parallel.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MeterSymbols/ammeter.png"></td>
                    <td>Ammeter</td>
                    <td>Measures electric current. Has near zero resistance. Connected serially.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MeterSymbols/ohmmeter.png"></td>
                    <td>Ohmmeter</td>
                    <td>Measures resistance</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MeterSymbols/wattmeter.png"></td>
                    <td>Wattmeter</td>
                    <td>Measures electric power</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}