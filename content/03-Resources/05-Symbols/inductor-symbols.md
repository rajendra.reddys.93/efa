﻿---
title: Inductor Symbols
weight: 7
notoc: true
summary: Iron core & Variable inductor.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/InductorSymbols/inductor.png"></td>
                    <td>Inductor</td>
                    <td>Coil / solenoid that generates magnetic field</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/InductorSymbols/iron-core-inductor.png"></td>
                    <td>Iron Core Inductor</td>
                    <td>Includes iron</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/InductorSymbols/variable-inductor.png"></td>
                    <td>Variable Inductor</td>
                    <td>Variable Inductor</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}