﻿---
title: Capacitor Symbols
weight: 6
notoc: true
summary: Polarised, Non-polarised & Variable capacitor.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/CapacitorSymbols/capacitor.png"></td>
                    <td>Capacitor</td>
                    <td>Capacitor is used to store electric charge. It acts as short circuit with AC and open circuit with DC.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/CapacitorSymbols/capacitor-2.png"></td>
                    <td>Capacitor</td>
                    <td>Capacitor is used to store electric charge. It acts as short circuit with AC and open circuit with DC.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/CapacitorSymbols/polarized-capacitor.png"></td>
                    <td>Polarized Capacitor</td>
                    <td>Electrolytic capacitor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/CapacitorSymbols/polarized-capacitor-2.png"></td>
                    <td>Polarized Capacitor</td>
                    <td>Electrolytic capacitor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/CapacitorSymbols/variable-capacitor.png"></td>
                    <td>Variable Capacitor</td>
                    <td>Adjustable capacitance</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}