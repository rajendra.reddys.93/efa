﻿---
title: Antenna Symbols
weight: 11
notoc: true
summary: Arial & Dipole antenna.
slug: antenna-symbols
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/AntennaSymbols/antenna.png"></td>
                    <td>Antenna / aerial</td>
                    <td>Transmits & receives radio waves</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/AntennaSymbols/antenna-aerial.png"></td>
                    <td>Antenna / aerial</td>
                    <td>Transmits & receives radio waves</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/AntennaSymbols/antenna-dipole.png"></td>
                    <td>Dipole Antenna</td>
                    <td>Two wires simple antenna</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}