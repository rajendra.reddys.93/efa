﻿---
title: Wire Symbols
weight: 1
notoc: true
summary: Electrical wire, connected and non-connected wire.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/WiresSymbols/electrical-wire.png"></td>
                    <td>Electrical Wire</td>
                    <td>Conductor of electrical current</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/WiresSymbols/connected-wires.png"></td>
                    <td>Connected Wires</td>
                    <td>Connected crossing</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/WiresSymbols/not-connected-wires.png"></td>
                    <td>Not Connected Wires</td>
                    <td>Wires are not connected</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}