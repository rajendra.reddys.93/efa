﻿---
title: Diode Symbols
weight: 8
notoc: true
summary: Zener, Shocttky, Tunnel Diode, LED, Photo Diode.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/diode.png"></td>
                    <td>Diode</td>
                    <td>Diode allows current flow in one direction only - left (anode) to right (cathode).</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/zener-diode.png"></td>
                    <td>Zener Diode</td>
                    <td>Allows current flow in one direction, but also can flow in the reverse direction when above breakdown voltage</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/schottky-diode.png"></td>
                    <td>Schottky Diode</td>
                    <td>Schottky diode is a diode with low voltage drop</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/varicap-diode.png"></td>
                    <td>Varactor / Varicap Diode</td>
                    <td>Variable capacitance diode</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/tunnel-diode.png"></td>
                    <td>Tunnel Diode</td>
                    <td>Tunnel Diode</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/led.png"></td>
                    <td>Light Emitting Diode (LED)</td>
                    <td>LED emits light when current flows through</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/DiodeSymbols/photodiode.png"></td>
                    <td>Photo-diode</td>
                    <td>Photo-diode allows current flow when exposed to light</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}