﻿---
title: Resistor Symbols
weight: 5
notoc: true
summary: Resistor, Thermister, Potentiometer & LDR.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/resistor-IEEE.png"></td>
                    <td>Resistor (IEEE)</td>
                    <td>Resistor reduces the current flow.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/resistor-IEC.png"></td>
                    <td>Resistor (IEC)</td>
                    <td>Resistor reduces the current flow.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/potentiometer-IEEE.png"></td>
                    <td>Potentiometer (IEEE)</td>
                    <td>Adjustable resistor - has 3 terminals.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/potentiometer-IEC.png"></td>
                    <td>Potentiometer (IEC)</td>
                    <td>Adjustable resistor - has 3 terminals.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/variable-resistor-IEEE.png"></td>
                    <td>Variable Resistor / Rheostat (IEEE)</td>
                    <td>Adjustable resistor - has 2 terminals.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/variable-resistor-IEC.png"></td>
                    <td>Variable Resistor / Rheostat (IEC)</td>
                    <td>Adjustable resistor - has 2 terminals.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/trimmer-resistor.png"></td>
                    <td>Trimmer Resistor</td>
                    <td>Preset resistor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/thermistor.png"></td>
                    <td>Thermistor</td>
                    <td>Thermal resistor - change resistance when temperature changes</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/ResistorSymbols/photoresistor.png"></td>
                    <td>Photoresistor / Light dependent resistor (LDR)</td>
                    <td>Photo-resistor - change resistance with light intensity change</td>
                </tr>
            </tbody>
        </table>
        <br />
{{<rawhtml>}}