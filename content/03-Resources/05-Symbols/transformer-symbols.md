﻿---
title: Transformer Symbols
weight: 14
draft: true
notoc: true
summary: Transformer symbols.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/WiresSymbols/electrical-wire.png"></td>
                    <td>Electrical Wire</td>
                    <td>Conductor of electrical current</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}