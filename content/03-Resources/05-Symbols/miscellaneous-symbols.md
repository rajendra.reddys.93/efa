﻿---
title: Miscellaneous Symbols
weight: 13
notoc: true
summary: Lamp, Fuse, Motor, Buzzer & Osceillator.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/lamp-1.png"></td>
                    <td>Lamp / light bulb</td>
                    <td>Generates light when current flows through</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/lamp-2.png"></td>
                    <td>Lamp / light bulb</td>
                    <td>Generates light when current flows through</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/lamp-3.png"></td>
                    <td>Lamp / light bulb</td>
                    <td>Generates light when current flows through</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/motor.png"></td>
                    <td>Motor</td>
                    <td>	Electric motor</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/transformer.png"></td>
                    <td>Transformer</td>
                    <td>Change AC voltage from high to low or low to high.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/electric-bell.png"></td>
                    <td>Electric bell</td>
                    <td>Rings when activated</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/buzzer.png"></td>
                    <td>Buzzer</td>
                    <td>Produce buzzing sound</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/fuse.png"></td>
                    <td>Fuse</td>
                    <td>The fuse disconnects when current above threshold. Used to protect circuit from high currents.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/fuse-2.png"></td>
                    <td>Fuse</td>
                    <td>The fuse disconnects when current above threshold. Used to protect circuit from high currents.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/bus-1.png"></td>
                    <td>Bus</td>
                    <td>Contains several wires. Usually for data / address.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/bus-2.png"></td>
                    <td>Bus</td>
                    <td>Contains several wires. Usually for data / address.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/bus-3.png"></td>
                    <td>Bus</td>
                    <td>Contains several wires. Usually for data / address.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/optocoupler.png"></td>
                    <td>Optocoupler / Opto-isolator</td>
                    <td>Optocoupler isolates connection to other board</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/speaker.png"></td>
                    <td>Loudspeaker</td>
                    <td></td>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/microphone.png"></td>
                    <td>Microphone</td>
                    <td>Converts sound waves to electrical signal</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/operational-amplifier.png"></td>
                    <td>Operational Amplifier</td>
                    <td>Amplify input signal</td>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/schmitt-trigger.png"></td>
                    <td>Schmitt Trigger</td>
                    <td>Operates with hysteresis to reduce noise.</td>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/adc.png"></td>
                    <td>Analog-to-digital converter (ADC)</td>
                    <td>Converts analog signal to digital numbers</td>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/dac.png"></td>
                    <td>Digital-to-Analog converter (DAC)</td>
                    <td>Converts digital numbers to analog signal</td>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/MiscellaniousSymbols/oscillator-crystal.png"></td>
                    <td>Crystal Oscillator</td>
                    <td>Used to generate precise frequency clock signal</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}