﻿---
title: Ground Symbols
weight: 4
notoc: true
summary: Earth, Chasis & Digital Ground.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Symbol</th>
                    <th style="text-align:center;">Component Name</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/GroundSymbols/earth-ground.png"></td>
                    <td>Earth Ground</td>
                    <td>Used for zero potential reference and electrical shock protection.</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/GroundSymbols/chassis-ground.png"></td>
                    <td>Chassis Ground</td>
                    <td>Connected to the chassis of the circuit</td>
                </tr>
                <tr>
                    <td><img class="img-100px" src="/images/Symbols/GroundSymbols/digital-ground.png"></td>
                    <td>Digital / Common Ground</td>
                    <td>Digital / Common Ground</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}