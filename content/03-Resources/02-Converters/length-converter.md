﻿---
title: Length Converter
summary: Convert between mm, um, meter, yard, Km, foot & inch.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>Nanometer</option>
                            <option>Micrometer</option>
                            <option>Millimeter</option>
                            <option>Centimeter</option>
                            <option>Decimeter</option>
                            <option>Meter</option>
                            <option>Decameter</option>
                            <option>Hectometer</option>
                            <option>Kilometer</option>
                            <option selected>Mile</option>
                            <option>Yard</option>
                            <option>Foot</option>
                            <option>Inch</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iNanoMtr" class="label">Nanometer</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iNanoMtr" placeholder="nanometer" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMicrMtr" class="label">Micrometer</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMicrMtr" placeholder="micrometer" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMiliMtr" class="label">Millimeter</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMiliMtr" placeholder="millimeter" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCentMtr" class="label">Centimeter</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCentMtr" placeholder="centimeter" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDeciMtr" class="label">Decimeter</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDeciMtr" placeholder="decimeter" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMeter" class="label">Meter</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMeter" placeholder="meter" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDecaMtr" class="label">Decameter</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDecaMtr" placeholder="decameter" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iHectMtr" class="label">Hectometer</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iHectMtr" placeholder="hectometer" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKiloMtr" class="label">Kilometer</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKiloMtr" placeholder="kilometer" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMile" class="label">Mile</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMile" placeholder="mile" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iYard" class="label">Yard</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iYard" placeholder="yard" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFoot" class="label">Foot</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFoot" placeholder="foot" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iInch" class="label">Inch</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iInch" placeholder="inch" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        switch (sFromUnit) {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8:
                //convert from other forms of meter
                iFrom = parseFloat(convertLengthToNumber(iFrom, sFromUnit));
                document.getElementById("iMeter").value = parseFloat((convertNumberToLength(iFrom, 5)).toFixed(10));
                document.getElementById("iMile").value = parseFloat((convertNumberToLength(iFrom, 9)).toFixed(10));
                document.getElementById("iYard").value = parseFloat((convertNumberToLength(iFrom, 10)).toFixed(10));
                document.getElementById("iFoot").value = parseFloat((convertNumberToLength(iFrom, 11)).toFixed(10));
                document.getElementById("iInch").value = parseFloat((convertNumberToLength(iFrom, 12)).toFixed(10));
                break;
            case 9:
                //convert from mile
                document.getElementById("iMeter").value = parseFloat((iFrom * 1609.34).toFixed(10));
                document.getElementById("iMile").value = parseFloat((iFrom * 1).toFixed(10));
                document.getElementById("iYard").value = parseFloat((iFrom * 1760).toFixed(10));
                document.getElementById("iFoot").value = parseFloat((iFrom * 5280).toFixed(10));
                document.getElementById("iInch").value = parseFloat((iFrom * 63360).toFixed(10));
                //mile to meter
                iFrom = parseFloat((iFrom * 1609.34).toFixed(10));
                break;
            case 10:
                //convert from yard
                document.getElementById("iMeter").value = parseFloat((iFrom * 0.9144).toFixed(10));
                document.getElementById("iMile").value = parseFloat((iFrom * 0.00056818).toFixed(10));
                document.getElementById("iYard").value = parseFloat((iFrom * 1).toFixed(10));
                document.getElementById("iFoot").value = parseFloat((iFrom * 3).toFixed(10));
                document.getElementById("iInch").value = parseFloat((iFrom * 36).toFixed(10));
                //mile to meter
                iFrom = parseFloat((iFrom * 0.9144).toFixed(10));
                break;
            case 11:
                //convert from foot
                document.getElementById("iMeter").value = parseFloat((iFrom * 0.3048).toFixed(10));
                document.getElementById("iMile").value = parseFloat((iFrom * 0.00018939).toFixed(10));
                document.getElementById("iYard").value = parseFloat((iFrom * 0.333333).toFixed(10));
                document.getElementById("iFoot").value = parseFloat((iFrom * 1).toFixed(10));
                document.getElementById("iInch").value = parseFloat((iFrom * 12).toFixed(10));
                //mile to meter
                iFrom = parseFloat((iFrom * 0.3048).toFixed(10));
                break;
            case 12:
                //convert from inch
                document.getElementById("iMeter").value = parseFloat((iFrom * 0.0254).toFixed(10));
                document.getElementById("iMile").value = parseFloat((iFrom * 1.5783e-5).toFixed(10));
                document.getElementById("iYard").value = parseFloat((iFrom * 0.0277778).toFixed(10));
                document.getElementById("iFoot").value = parseFloat((iFrom * 0.0833333).toFixed(10));
                document.getElementById("iInch").value = parseFloat((iFrom * 1).toFixed(10));
                //mile to meter
                iFrom = parseFloat((iFrom * 0.0254).toFixed(10));
                break;
        }
        document.getElementById("iNanoMtr").value = parseFloat((convertNumberToLength(iFrom, 0)).toFixed(10));
        document.getElementById("iMicrMtr").value = parseFloat((convertNumberToLength(iFrom, 1)).toFixed(10));
        document.getElementById("iMiliMtr").value = parseFloat((convertNumberToLength(iFrom, 2)).toFixed(10));
        document.getElementById("iCentMtr").value = parseFloat((convertNumberToLength(iFrom, 3)).toFixed(10));
        document.getElementById("iDeciMtr").value = parseFloat((convertNumberToLength(iFrom, 4)).toFixed(10));
        document.getElementById("iMeter").value = parseFloat((convertNumberToLength(iFrom, 5)).toFixed(10));
        document.getElementById("iDecaMtr").value = parseFloat((convertNumberToLength(iFrom, 6)).toFixed(10));
        document.getElementById("iHectMtr").value = parseFloat((convertNumberToLength(iFrom, 7)).toFixed(10));
        document.getElementById("iKiloMtr").value = parseFloat((convertNumberToLength(iFrom, 8)).toFixed(10));
    }
</script>
{{</rawhtml>}}

## How To Use

The Length Conversion is necessary for designing PCB, Pads, Foot-prints and Mechanical Casing etc. The length converter does general conversion of length. However you can find more tools useful for [PCB Designing](/tools/pcb-utils) under Tools. We have great tutorials on [PCB Designing](/learn/pcb) under learn. In our length converter we support conversion between **Nanometer, Micrometer, Millimeter, Centimeter, Decimeter, Meter, Decameter, Hectometer, Kilometer, Mile, Yard, Foot and Inch**.

You can select length unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

Converting length from one unit of measurement to other measuring unit is required when we come across different units used by people across industries. For example, as a PCB designer you may mention the PCB size in "mm"(millimeters). But PCB fabricator and mechanical enclosure designer required the PCB size in "inch" or "cm"(centimeter).

Using our length converter is not restricted to only PCB designers, fabricators and mechanical designers, you can use it for general conversions also. If you are testing any RF networks and you want to represent distance between two points in miles, kilometers and foot, our length converter will be very useful.

## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below table shows all multiplication factors used in our converter.</p>

{{<rawhtml>}}
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Length Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Nanometer</td>
            <td>Micrometer</td>
            <td>Millimeter</td>
            <td>Centimeter</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanometer</td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-7</sup></td>
        </tr>
        <tr>
            <td>Micrometer</td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Millimeter</td>
            <td>10<sup>6</sup></td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>0.1</td>
        </tr>
        <tr>
            <td>Centimeter</td>
            <td>10<sup>7</sup></td>
            <td>10<sup>4</sup></td>
            <td>10</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Decimeter</td>
            <td>10<sup>8</sup></td>
            <td>10<sup>5</sup></td>
            <td>100</td>
            <td>10</td>
        </tr>
        <tr>
            <td>Meter</td>
            <td>10<sup>9</sup></td>
            <td>10<sup>6</sup></td>
            <td>1000</td>
            <td>100</td>
        </tr>
        <tr>
            <td>Decameter</td>
            <td>10<sup>10</sup></td>
            <td>10<sup>7</sup></td>
            <td>10<sup>4</sup></td>
            <td>1000</td>
        </tr>
        <tr>
            <td>Hectometer</td>
            <td>10<sup>11</sup></td>
            <td>10<sup>8</sup></td>
            <td>10<sup>5</sup></td>
            <td>10<sup>4</sup></td>
        </tr>
        <tr>
            <td>Kilometer</td>
            <td>10<sup>12</sup></td>
            <td>10<sup>9</sup></td>
            <td>10<sup>6</sup></td>
            <td>10<sup>5</sup></td>
        </tr>
        <tr>
            <td>Mile</td>
            <td>1.60934x10<sup>12</sup></td>
            <td>1.60934x10<sup>9</sup></td>
            <td>1.60934x10<sup>6</sup></td>
            <td>1.60934x10<sup>5</sup></td>
        </tr>
        <tr>
            <td>Yard</td>
            <td>9.144x10<sup>8</sup></td>
            <td>9.144x10<sup>5</sup></td>
            <td>9144</td>
            <td>91.44</td>
        </tr>
        <tr>
            <td>Foot</td>
            <td>3.048x10<sup>8</sup></td>
            <td>3.048x10<sup>5</sup></td>
            <td>304.8</td>
            <td>30.48</td>
        </tr>
        <tr>
            <td>Inch</td>
            <td>2.54x10<sup>7</sup></td>
            <td>2.54x10<sup>4</sup></td>
            <td>25.4</td>
            <td>2.54</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Length Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Meter</td>
            <td>Decameter</td>
            <td>Hectometer</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanometer</td>
            <td>10<sup>-8</sup></td>
            <td>10<sup>-9</sup></td>
            <td>10<sup>-10</sup></td>
        </tr>
        <tr>
            <td>Micrometer</td>
            <td>10<sup>-5</sup></td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-7</sup></td>
        </tr>
        <tr>
            <td>Millimeter</td>
            <td>0.01</td>
            <td>0.001</td>
            <td>10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Centimeter</td>
            <td>0.1</td>
            <td>0.01</td>
            <td>0.001</td>
        </tr>
        <tr>
            <td>Decimeter</td>
            <td>1</td>
            <td>0.1</td>
            <td>0.01</td>
        </tr>
        <tr>
            <td>Meter</td>
            <td>10</td>
            <td>1</td>
            <td>0.1</td>
        </tr>
        <tr>
            <td>Decameter</td>
            <td>100</td>
            <td>10</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Hectometer</td>
            <td>1000</td>
            <td>100</td>
            <td>10</td>
        </tr>
        <tr>
            <td>Kilometer</td>
            <td>10<sup>4</sup></td>
            <td>1000</td>
            <td>100</td>
        </tr>
        <tr>
            <td>Mile</td>
            <td>1.60934x10<sup>4</sup></td>
            <td>1609.34</td>
            <td>160.934</td>
        </tr>
        <tr>
            <td>Yard</td>
            <td>9.144</td>
            <td>0.9144</td>
            <td>0.09144</td>
        </tr>
        <tr>
            <td>Foot</td>
            <td>3.048</td>
            <td>0.3048</td>
            <td>0.03048</td>
        </tr>
        <tr>
            <td>Inch</td>
            <td>0.254</td>
            <td>0.0254</td>
            <td>2.54x10<sup>-3</sup></td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Length Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Kilometer</td>
            <td>Mile</td>
            <td>Decimeter</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanometer</td>
            <td>10<sup>-11</sup></td>
            <td>10<sup>-12</sup></td>
            <td>6.2137x10<sup>-13</sup></td>
        </tr>
        <tr>
            <td>Micrometer</td>
            <td>10<sup>-8</sup></td>
            <td>10<sup>-9</sup></td>
            <td>6.2137x10<sup>-10</sup></td>
        </tr>
        <tr>
            <td>Millimeter</td>
            <td>10<sup>-5</sup></td>
            <td>10<sup>-6</sup></td>
            <td>6.2137x10<sup>-7</sup></td>
        </tr>
        <tr>
            <td>Centimeter</td>
            <td>10<sup>-4</sup></td>
            <td>10<sup>-5</sup></td>
            <td>6.2137x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Decimeter</td>
            <td>0.001</td>
            <td>10<sup>-4</sup></td>
            <td>6.2137x10<sup>-5</sup></td>
        </tr>
        <tr>
            <td>Meter</td>
            <td>0.01</td>
            <td>0.001</td>
            <td>6.2137x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Decameter</td>
            <td>0.1</td>
            <td>0.01</td>
            <td>6.2137x10<sup>-3</sup></td>
        </tr>
        <tr>
            <td>Hectometer</td>
            <td>1</td>
            <td>0.1</td>
            <td>6.2137x10<sup>-2</sup></td>
        </tr>
        <tr>
            <td>Kilometer</td>
            <td>10</td>
            <td>1</td>
            <td>0.6213</td>
        </tr>
        <tr>
            <td>Mile</td>
            <td>16.0934</td>
            <td>1.60934</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Yard</td>
            <td>9.144x10<sup>-3</sup></td>
            <td>9.144x10<sup>-4</sup></td>
            <td>5.6818x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Foot</td>
            <td>3.048x10<sup>-3</sup></td>
            <td>3.048x10<sup>-4</sup></td>
            <td>1.8939x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Inch</td>
            <td>2.54x10<sup>-4</sup></td>
            <td>2.54x10<sup>-5</sup></td>
            <td>1.5783x10<sup>-5</sup></td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="4">Multiplication Factor for Length Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="3">Convert to</th>
        </tr>
        <tr>
            <td>Yard</td>
            <td>Foot</td>
            <td>Inch</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanometer</td>
            <td>1.0936x10<sup>-9</sup></td>
            <td>3.2808x10<sup>-9</sup></td>
            <td>3.937x10<sup>-8</sup></td>
        </tr>
        <tr>
            <td>Micrometer</td>
            <td>1.0936x10<sup>-6</sup></td>
            <td>3.2808x10<sup>-6</sup></td>
            <td>3.937x10<sup>-5</sup></td>
        </tr>
        <tr>
            <td>Millimeter</td>
            <td>1.0936x10<sup>-3</sup></td>
            <td>3.2808x10<sup>-3</sup></td>
            <td>3.937x10<sup>-2</sup></td>
        </tr>
        <tr>
            <td>Centimeter</td>
            <td>0.010936</td>
            <td>0.032808</td>
            <td>0.393701</td>
        </tr>
        <tr>
            <td>Decimeter</td>
            <td>0.10936</td>
            <td>0.32808</td>
            <td>3.93701</td>
        </tr>
        <tr>
            <td>Meter</td>
            <td>1.09361</td>
            <td>3.2808</td>
            <td>39.3701</td>
        </tr>
        <tr>
            <td>Decameter</td>
            <td>10.9361</td>
            <td>32.808</td>
            <td>393.701</td>
        </tr>
        <tr>
            <td>Hectometer</td>
            <td>109.361</td>
            <td>328.08</td>
            <td>3937.01</td>
        </tr>
        <tr>
            <td>Kilometer</td>
            <td>1093.61</td>
            <td>3280.8</td>
            <td>39370.1</td>
        </tr>
        <tr>
            <td>Mile</td>
            <td>1760</td>
            <td>5279.93579</td>
            <td>63360</td>
        </tr>
        <tr>
            <td>Yard</td>
            <td>1</td>
            <td>2.99996</td>
            <td>36</td>
        </tr>
        <tr>
            <td>Foot</td>
            <td>0.3333</td>
            <td>1</td>
            <td>12</td>
        </tr>
        <tr>
            <td>Inch</td>
            <td>0.02778</td>
            <td>0.08333</td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of length is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 Meter to other Measurements

In the below example we will convert 1 Meter to its equivalent values of Kilometer, Mile, Foot, Yard and Inches.

**Convert from 1 Meter to Kilometer :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Kilometer = 0.001 x 1 Meter
    </h5>
    <h5 class="numforleft">
        Kilometer = 0.001
    </h5>
</div>
{{</formula>}}

**Convert from 1 Meter to Mile :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Mile = (6.2137 x 10<sup>-4</sup>) x 1 Meter
    </h5>
    <h5 class="numforleft">
        Mile = 0.00062137 x 1 Meter
    </h5>
    <h5 class="numforleft">
        Mile = 0.00062137
    </h5>
</div>
{{</formula>}}

**Convert from 1 Meter to Yard :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Yard = 1.09361 x 1 Meter
    </h5>
    <h5 class="numforleft">
        Yard = 1.09361
    </h5>
</div>
{{</formula>}}

**Convert from 1 Meter to Foot :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Foot = 3.2808 x 1 Meter
    </h5>
    <h5 class="numforleft">
        Foot = 3.2808
    </h5>
</div>
{{</formula>}}

**Convert from 1 Meter to Inch :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Inch = 39.3701 x 1 Meter
    </h5>
    <h5 class="numforleft">
        Inch = 39.3701
    </h5>
</div>
{{</formula>}}
