﻿---
title: Mass/Weight Converter
notoc: true
draft: true
summary: Convert between ug, mg, gram, Kg, tonne, pound & ounce.
---

{{<rawhtml>}}
<div class="row">
    <div class="body-content">
        <h3 class="section scrollspy">Mass Converter</h3>
        <form class="form-horizontal font-comic-sans">
            <div class="form-group">
                <label class="col-sm-2">From</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="from" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option>Microgram</option>
                        <option>Milligram</option>
                        <option selected="selected">Gram</option>
                        <option>Kilogram</option>
                        <option>Tonne</option>
                        <option>Pound</option>
                        <option>Ounce</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">To</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="to" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option>Microgram</option>
                        <option>Milligram</option>
                        <option>Gram</option>
                        <option>Kilogram</option>
                        <option>Tonne</option>
                        <option selected="selected">Pound</option>
                        <option>Ounce</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
{{</rawhtml>}}