﻿---
title: Time Converter
summary: Convert between ns, us, ms, seconds, minutes, hours, days, weeks, months, years & decades.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="1000" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option selected>Nanosecond</option>
                            <option>Microsecond</option>
                            <option>Millisecond</option>
                            <option>Second</option>
                            <option>Minute</option>
                            <option>Hour</option>
                            <option>Day</option>
                            <option>Week</option>
                            <option>Month</option>
                            <option>Year</option>
                            <option>Decade</option>
                            <option>Century</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iNanoSecond" class="label">Nanosecond</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iNanoSecond" placeholder="nanosecond" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMicroSecond" class="label">Microsecond</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMicroSecond" placeholder="microsecond" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMilliSecond" class="label">Millisecond</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMilliSecond" placeholder="millisecond" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iSecond" class="label">Second</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iSecond" placeholder="second" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMinute" class="label">Minute</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMinute" placeholder="minute" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iHour" class="label">Hour</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iHour" placeholder="hour" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDay" class="label">Day</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDay" placeholder="day" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iWeek" class="label">Week</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iWeek" placeholder="week" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMonth" class="label">Month</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMonth" placeholder="month" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iYear" class="label">Year</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iYear" placeholder="year" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDecade" class="label">Decade</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDecade" placeholder="decade" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCentury" class="label">Century</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCentury" placeholder="century" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert()
    {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        iFrom = parseFloat(convTimeToMinutes(iFrom, sFromUnit));
        document.getElementById("iNanoSecond").value = parseFloat(convMinutesToTime(iFrom, 0).toFixed(10));
        document.getElementById("iMicroSecond").value = parseFloat(convMinutesToTime(iFrom, 1).toFixed(10));
        document.getElementById("iMilliSecond").value = parseFloat(convMinutesToTime(iFrom, 2).toFixed(10));
        document.getElementById("iSecond").value = parseFloat(convMinutesToTime(iFrom, 3).toFixed(10));
        document.getElementById("iMinute").value = parseFloat(convMinutesToTime(iFrom, 4).toFixed(10));
        document.getElementById("iHour").value = parseFloat(convMinutesToTime(iFrom, 5).toFixed(10));
        document.getElementById("iDay").value = parseFloat(convMinutesToTime(iFrom, 6).toFixed(10));
        document.getElementById("iWeek").value = parseFloat(convMinutesToTime(iFrom, 7).toFixed(10));
        document.getElementById("iMonth").value = parseFloat(convMinutesToTime(iFrom, 8).toFixed(10));
        document.getElementById("iYear").value = parseFloat(convMinutesToTime(iFrom, 9).toFixed(10));
        document.getElementById("iDecade").value = parseFloat(convMinutesToTime(iFrom, 10).toFixed(10));
        document.getElementById("iCentury").value = parseFloat(convMinutesToTime(iFrom, 11).toFixed(10));
    }
    function convFromTime() {
        var from = document.getElementById("iFromTime").value;
        var fromMul = document.getElementById("sFromTimeUnit").selectedIndex;
        var toMul = document.getElementById("sToTimeUnit").selectedIndex;
        document.getElementById("iToTime").value = convMinutesToTime(convTimeToMinutes(from, fromMul), toMul).toPrecision(6);
    }
    function convToTime() {
        var to = document.getElementById("iToTime").value;
        var toMul = document.getElementById("sToTimeUnit").selectedIndex;
        var fromMul = document.getElementById("sFromTimeUnit").selectedIndex;
        document.getElementById("iFromTime").value = convMinutesToTime(convTimeToMinutes(to, toMul), fromMul).toPrecision(6);
    }
</script>
{{</rawhtml>}}

## How To Use

Please enter the value in the input field available and select you input type from the various options available next to input field.

## Conversion Explanation

Here we explain the necessary of conversion.

## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}} We will rectify.

Below table shows all multiplication factors used in our converter.

{{<rawhtml>}}
<table class="centered highlight responsive-table">
    <thead>
        <tr>
            <th style="text-align:center;" colspan="7">Multiplication Factor for Time Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="6">Convert to</th>
        </tr>
        <tr>
            <td>Nanosecond</td>
            <td>Microsecond</td>
            <td>Millisecond</td>
            <td>Second</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanosecond</td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-9</sup></td>
        </tr>
        <tr>
            <td>Microsecond</td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Millisecond</td>
            <td>10<sup>6</sup></td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
        </tr>
        <tr>
            <td>Second</td>
            <td>10<sup>9</sup></td>
            <td>10<sup>6</sup></td>
            <td>1000</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Minute</td>
            <td>6x10<sup>10</sup></td>
            <td>6x10<sup>7</sup></td>
            <td>6x10<sup>4</sup></td>
            <td>60</td>
        </tr>
        <tr>
            <td>Hour</td>
            <td>3.6x10<sup>12</sup></td>
            <td>3.6x10<sup>9</sup></td>
            <td>3.6x10<sup>6</sup></td>
            <td>3600</td>
        </tr>
        <tr>
            <td>Day</td>
            <td>8.64x10<sup>13</sup></td>
            <td>8.64x10<sup>10</sup></td>
            <td>8.64x10<sup>7</sup></td>
            <td>8.64x10<sup>4</sup></td>
        </tr>
        <tr>
            <td>Week</td>
            <td>6.048x10<sup>14</sup></td>
            <td>6.048x10<sup>11</sup></td>
            <td>6.048x10<sup>8</sup></td>
            <td>6.048x10<sup>5</sup></td>
        </tr>
        <tr>
            <td>Month</td>
            <td>2.628x10<sup>15</sup></td>
            <td>2.628x10<sup>12</sup></td>
            <td>2.628x10<sup>9</sup></td>
            <td>2.628x10<sup>6</sup></td>
        </tr>
        <tr>
            <td>Year</td>
            <td>3.154x10<sup>16</sup></td>
            <td>3.154x10<sup>13</sup></td>
            <td>3.154x10<sup>10</sup></td>
            <td>3.154x10<sup>7</sup></td>
        </tr>
        <tr>
            <td>Decade</td>
            <td>3.154x10<sup>17</sup></td>
            <td>3.154x10<sup>14</sup></td>
            <td>3.154x10<sup>11</sup></td>
            <td>3.154x10<sup>8</sup></td>
        </tr>
        <tr>
            <td>Century</td>
            <td>3.154x10<sup>18</sup></td>
            <td>3.154x10<sup>15</sup></td>
            <td>3.154x10<sup>12</sup></td>
            <td>3.154x10<sup>9</sup></td>
        </tr>
    </tbody>
</table>
<table class="centered highlight responsive-table">
    <thead>
        <tr>
            <th style="text-align:center;" colspan="7">Multiplication Factor for Time Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="6">Convert to</th>
        </tr>
        <tr>
            <td>Minute</td>
            <td>Hour</td>
            <td>Day</td>
            <td>Week</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanosecond</td>
            <td>1.6667x10<sup>-11</sup></td>
            <td>2.7778x10<sup>-13</sup></td>
            <td>1.1574x10<sup>-14</sup></td>
            <td>1.6534x10<sup>-15</sup></td>
        </tr>
        <tr>
            <td>Microsecond</td>
            <td>1.6667x10<sup>-8</sup></td>
            <td>2.7778x10<sup>-10</sup></td>
            <td>1.1574x10<sup>-11</sup></td>
            <td>1.6534x10<sup>-12</sup></td>
        </tr>
        <tr>
            <td>Millisecond</td>
            <td>1.6667x10<sup>-5</sup></td>
            <td>2.7778x10<sup>-7</sup></td>
            <td>1.1574x10<sup>-8</sup></td>
            <td>1.6534x10<sup>-9</sup></td>
        </tr>
        <tr>
            <td>Second</td>
            <td>1.6667x10<sup>-5</sup></td>
            <td>2.7778x10<sup>-7</sup></td>
            <td>1.1574x10<sup>-5</sup></td>
            <td>1.6534x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Minute</td>
            <td>0.01667</td>
            <td>2.7778x10<sup>-4</sup></td>
            <td>6.9444x10<sup>-4</sup></td>
            <td>9.9206x10<sup>-5</sup></td>
        </tr>
        <tr>
            <td>Hour</td>
            <td>60</td>
            <td>1</td>
            <td>4.1667x10<sup>-2</sup></td>
            <td>5.9552x10<sup>-3</sup></td>
        </tr>
        <tr>
            <td>Day</td>
            <td>1440</td>
            <td>24</td>
            <td>1</td>
            <td>0.14286</td>
        </tr>
        <tr>
            <td>Week</td>
            <td>10080</td>
            <td>168</td>
            <td>7</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Month</td>
            <td>4.38x10<sup>4</sup></td>
            <td>730.001</td>
            <td>30.4167</td>
            <td>4.34524</td>
        </tr>
        <tr>
            <td>Year</td>
            <td>5.256x10<sup>5</sup></td>
            <td>8760</td>
            <td>365</td>
            <td>52.1429</td>
        </tr>
        <tr>
            <td>Decade</td>
            <td>5.256x10<sup>6</sup></td>
            <td>8.760x10<sup>4</sup></td>
            <td>3650</td>
            <td>521.429</td>
        </tr>
        <tr>
            <td>Century</td>
            <td>5.256x10<sup>7</sup></td>
            <td>8.760x10<sup>5</sup></td>
            <td>36500</td>
            <td>5214.29</td>
        </tr>
    </tbody>
</table>
<table class="centered highlight responsive-table">
    <thead>
        <tr>
            <th style="text-align:center;" colspan="7">Multiplication Factor for Time Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="6">Convert to</th>
        </tr>
        <tr>
            <td>Month</td>
            <td>Year</td>
            <td>Decade</td>
            <td>Century</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nanosecond</td>
            <td>3.8052x10<sup>-16</sup></td>
            <td>3.1710x10<sup>-17</sup></td>
            <td>3.1710x10<sup>-18</sup></td>
            <td>3.1710x10<sup>-19</sup></td>
        </tr>
        <tr>
            <td>Microsecond</td>
            <td>3.8052x10<sup>-13</sup></td>
            <td>3.1710x10<sup>-14</sup></td>
            <td>3.1710x10<sup>-15</sup></td>
            <td>3.1710x10<sup>-16</sup></td>
        </tr>
        <tr>
            <td>Millisecond</td>
            <td>3.8052x10<sup>-10</sup></td>
            <td>3.1710x10<sup>-11</sup></td>
            <td>3.1710x10<sup>-12</sup></td>
            <td>3.1710x10<sup>-13</sup></td>
        </tr>
        <tr>
            <td>Second</td>
            <td>3.8052x10<sup>-7</sup></td>
            <td>3.1710x10<sup>-8</sup></td>
            <td>3.1710x10<sup>-9</sup></td>
            <td>3.1710x10<sup>-10</sup></td>
        </tr>
        <tr>
            <td>Minute</td>
            <td>2.2831x10<sup>-5</sup></td>
            <td>1.9026x10<sup>-6</sup></td>
            <td>1.9026x10<sup>-7</sup></td>
            <td>1.9026x10<sup>-8</sup></td>
        </tr>
        <tr>
            <td>Hour</td>
            <td>1.3698x10<sup>-3</sup></td>
            <td>1.1415x10<sup>-5</sup></td>
            <td>1.1415x10<sup>-5</sup></td>
            <td>1.1415x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Day</td>
            <td>0.03288</td>
            <td>2.7397x10<sup>-3</sup></td>
            <td>2.7397x10<sup>-4</sup></td>
            <td>2.7397x10<sup>-5</sup></td>
        </tr>
        <tr>
            <td>Week</td>
            <td>0.23014</td>
            <td>0.01918</td>
            <td>1.9178x10<sup>-3</sup></td>
            <td>1.9178x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Month</td>
            <td>1</td>
            <td>0.08334</td>
            <td>8.3334x10<sup>-3</sup></td>
            <td>8.3334x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Year</td>
            <td>12</td>
            <td>1</td>
            <td>0.1</td>
            <td>0.01</td>
        </tr>
        <tr>
            <td>Decade</td>
            <td>120</td>
            <td>10</td>
            <td>1</td>
            <td>0.1</td>
        </tr>
        <tr>
            <td>Century</td>
            <td>1200</td>
            <td>100</td>
            <td>10</td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of time is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 Day to other Measurements

In the below example we will convert 1 Day to its equivalent values of Second, Minute, Hour, Week, Month and Year.

**Convert from 1 Day to Seconds :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Second = (8.64 x 10<sup>4</sup>) x 1 Day
    </h5>
    <h5 class="numforleft">
        Second = 86400
    </h5>
</div>
{{</formula>}}

**Convert from 1 Day to Minute :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Minute = 1440 x 1 Day
    </h5>
    <h5 class="numforleft">
        Minute = 1440
    </h5>
</div>
{{</formula>}}

**Convert from 1 Day to Hour :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Hour = 24 x 1 Day
    </h5>
    <h5 class="numforleft">
        Hour = 24
    </h5>
</div>
{{</formula>}}

**Convert from 1 Day to Week :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Week = 0.14286 x 1 Day
    </h5>
    <h5 class="numforleft">
        Week = 0.14286
    </h5>
</div>
{{</formula>}}

**Convert from 1 Day to Month :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Month = 0.03288 x 1 Day
    </h5>
    <h5 class="numforleft">
        Month = 0.03288
    </h5>
</div>
{{</formula>}}

**Convert from 1 Day to Year :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Year = (2.7397 x 10<sup>-3</sup>) x 1 Day
    </h5>
    <h5 class="numforleft">
        Year = 0.0027397
    </h5>
</div>
{{</formula>}}
