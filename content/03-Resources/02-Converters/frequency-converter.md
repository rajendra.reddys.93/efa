﻿---
title: Frequency Converter
notoc: true
draft: true
summary: Convert between Hz, KHz, MHz, mHz & GHz.
---

{{<rawhtml>}}
<div class="row">
    <div class="body-content">
        <h3 class="section scrollspy">Frequency Converter</h3>
        <form class="form-horizontal font-comic-sans">
            <div class="form-group">
                <label class="col-sm-2">From</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="from" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option selected="selected">Hertz</option>
                        <option>Kilo Hertz</option>
                        <option>Mega Hertz</option>
                        <option>Giga Hertz</option>
                        <option>Milli Hertz</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">To</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="to" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option>Hertz</option>
                        <option selected="selected">Kilo Hertz</option>
                        <option>Mega Hertz</option>
                        <option>Giga Hertz</option>
                        <option>Milli Hertz</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
{{</rawhtml>}}