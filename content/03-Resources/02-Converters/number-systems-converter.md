﻿---
title: Number System
summary: Convert between octal, decimal, hexadecimal and binary.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>Binary</option>
                            <option>Octal</option>
                            <option selected>Decimal</option>
                            <option>Hexa-Decimal</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBin" class="label">Binary</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBin" placeholder="Binary" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iOct" class="label">Octal</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iOct" placeholder="Octal" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iDec" class="label">Decimal</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iDec" placeholder="Decimal" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iHex" class="label">Hexa-Decimal</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iHex" placeholder="Hexa-Decimal" value="" type="hexa">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        if (sFromUnit == 3) {
            //alternate is setAttribute
            //example
            //document.getElementById("iFrom").setAttribute('maxlength', 17);
            document.getElementById("iFrom").maxLength = "13";
        }
        else {
            if (sFromUnit == 0)
                document.getElementById("iFrom").maxLength = "53";
            if (sFromUnit == 1)
                document.getElementById("iFrom").maxLength = "17";
            if (sFromUnit == 2)
                document.getElementById("iFrom").maxLength = "15";
        }
        iFrom = document.getElementById("iFrom").value;
        switch (sFromUnit) {
            case 0:
                document.getElementById("iBin").value = parseInt(iFrom, 2);
                document.getElementById("iDec").value = convertBinToDec(parseInt(iFrom, 2));
                document.getElementById("iOct").value = convertBinToOct(parseInt(iFrom, 2));
                document.getElementById("iHex").value = convertBinToHex(parseInt(iFrom, 2));
                break;
            case 1:
                document.getElementById("iBin").value = convertOctToBin(parseInt(iFrom, 8));
                document.getElementById("iOct").value = parseInt(iFrom, 10);
                document.getElementById("iDec").value = convertOctToDec(parseInt(iFrom, 8));
                document.getElementById("iHex").value = convertOctToHex(parseInt(iFrom, 8));
                break;
            case 2:
                document.getElementById("iBin").value = convertDecToBin(parseInt(iFrom, 10));
                document.getElementById("iOct").value = convertDecToOct(parseInt(iFrom, 10));
                document.getElementById("iDec").value = parseInt(iFrom, 10);
                document.getElementById("iHex").value = convertDecToHex(parseInt(iFrom, 10));
                break;
            case 3:
                document.getElementById("iBin").value = convertHexToBin(parseInt(iFrom, 16));
                document.getElementById("iOct").value = convertHexToOct(parseInt(iFrom, 16));
                document.getElementById("iDec").value = convertHexToDec(parseInt(iFrom, 16));
                document.getElementById("iHex").value = parseInt(iFrom, 10);
                break;
            default:
                document.getElementById("iFrom").setAttribute('type', Text);
                break;
        }
    }
</script>
{{</rawhtml>}}

## How To Use

The Number System Conversion is the one of basic conversion required when ever you are coding for a controller. You can read more about [Number System](/Microcontroller/NumberSystem) in [Micro-controllers](/MicroController) under Education. In the above converter we support conversion between **Binary, Octal, Decimal and Hexadecimal**.

You can select number system from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter doesn't accepts fractional values. You can enter only "1" or "0" when "Binary" is selected, from "0" to "7" when "Octal" is selected, from "0" to "9" when "Decimal" is selected, and similarly alphabets from "A" or "a" to "F" or "f" in addition to numbers "0" to "9" when "Hexadecimal" is selected. You can enter any value of length 13, 15, 17 and 53 for Hexadecimal, Decimal, Octal and Binary respectively.

## Convertion Explanation

In our day to day life we use base 10 or decimal number for computation. For humans, decimal number are very easy to manipulate/calculate. But computers does not understand the decimal numbers as they only deal with binary numbers i.e. 1's and 0's. So it is required to convert decimal values to binary. Computers understand only binary numbers and it is very difficult to represent large numbers in binary form as it is difficult to read/understand by humans(programmers).

So the hexadecimal representation came into picture. The idea behind hexadecimal values is that, in common all processors or controllers uses minimum 4 bits for computations (no computer with less than 4 bit processor), so grouping 4 bits into single character. For example, representing 1011 as 0xB. By using hexadecimal values we can easily represent long binary values in more user friendly format. For example we can represent binary 1010 1010 1010 1010 as 0xAAAA which is human understandable format.

The octal number system is not much used in computers. It is required to learn the number system conversion for an software developer. You may not required to do the conversions on your own as your compiler will be doing it for you. When you are dealing with hardware dependent and memory Related application, you need to perform few conversions on your own. Whenever you need any conversion, you can use our on-line [number system converter]({{< ref number-systems-converter.md >}}).

## Formula Explanation

In the above converter rather than using any conversion formula, we used inbuilt library function such as parseInt(value, type) and value.toString(type). However you can find all conversion formula with examples in our [Number System](/Microcontroller/NumberSystem) article in [Micro-controller](/Microcontroller) under Education section.

## Example Conversion

All conversion examples with formula are discussed in our [Number System](/Microcontroller/NumberSystem) article in [Micro-controller](/Microcontroller) under Education section.
