﻿---
title: Area Converter
draft: true
notoc: true
summary: Convert between sq km, sq mile, sq yard, sq foot, sq inch, hectare.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">From</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="1000" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option selected>Square Meter</option>
                            <option>Square Kilometer</option>
                            <option>Square Mile</option>
                            <option>Square Yard</option>
                            <option>Square Foot</option>
                            <option>Square Inch</option>
                            <option>Hectare</option>
                            <option>Acre</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">To</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iTo" placeholder="To" value="" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sToUnit">
                            <option>Square Meter</option>
                            <option selected>Square Kilometer</option>
                            <option>Square Mile</option>
                            <option>Square Yard</option>
                            <option>Square Foot</option>
                            <option>Square Inch</option>
                            <option>Hectare</option>
                            <option>Acre</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
</form>
{{</rawhtml>}}