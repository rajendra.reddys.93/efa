﻿---
title: Data Transfer Rate Converter
summary: Convert between KBps, MBps, Kbps, Mbps, GBps & Gbps.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>Bits per Second</option>
                            <option>Bytes per Second</option>
                            <option selected>Kilobits per Second</option>
                            <option>Kilobytes per Second</option>
                            <option>Megabits per Second</option>
                            <option>Megabytes per Second</option>
                            <option>Gigabits per Second</option>
                            <option>Gigabytes per Second</option>
                            <option>Terabits per Second</option>
                            <option>Terabytes per Second</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBiPS" class="label">Bits/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBiPS" placeholder="bits per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iByPS" class="label">Bytes/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iByPS" placeholder="bytes per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKBiPS" class="label">KBits/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKBiPS" placeholder="kilo bits per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKByPS" class="label">KBytes/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKByPS" placeholder="kilo bytes per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMBiPS" class="label">MBits/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMBiPS" placeholder="mega bits per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMByPS" class="label">MBytes/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMByPS" placeholder="mega bytes per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iGBiPS" class="label">GBits/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iGBiPS" placeholder="giga bits per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iGByPS" class="label">GBytes/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iGByPS" placeholder="giga bytes per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iTBiPS" class="label">TBits/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iTBiPS" placeholder="tera bits per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iTByPS" class="label">TBytes/Sec</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iTByPS" placeholder="tera bytes per sec" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        iFrom = parseFloat(convDataRateToBytes(iFrom, sFromUnit));
        document.getElementById("iBiPS").value = parseFloat(convBytesToDataRate(iFrom, 0).toFixed(6));
        document.getElementById("iByPS").value = parseFloat(convBytesToDataRate(iFrom, 1).toFixed(6));
        document.getElementById("iKBiPS").value = parseFloat(convBytesToDataRate(iFrom, 2).toFixed(6));
        document.getElementById("iKByPS").value = parseFloat(convBytesToDataRate(iFrom, 3).toFixed(6));
        document.getElementById("iMBiPS").value = parseFloat(convBytesToDataRate(iFrom, 4).toFixed(6));
        document.getElementById("iMByPS").value = parseFloat(convBytesToDataRate(iFrom, 5).toFixed(6));
        document.getElementById("iGBiPS").value = parseFloat(convBytesToDataRate(iFrom, 6).toFixed(6));
        document.getElementById("iGByPS").value = parseFloat(convBytesToDataRate(iFrom, 7).toFixed(6));
        document.getElementById("iTBiPS").value = parseFloat(convBytesToDataRate(iFrom, 8).toFixed(6));
        document.getElementById("iTByPS").value = parseFloat(convBytesToDataRate(iFrom, 9).toFixed(6));
    }
</script>
{{</rawhtml>}}

## How To Use

In our Data Storage converter we support conversion between **Bits/Sec, Bytes/Sec, Kilobits/Sec, Kilobytes/Sec, Megabits/Sec, Megabytes/Sec, Gigabits/Sec, Gigabytes/Sec, Terabits/Sec and Terabytes/Sec**. You can select length unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below table shows all multiplication factors used in our converter.

{{<rawhtml>}}
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Storage Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Bits/Sec</td>
            <td>Bytes/Sec</td>
            <td>Kbits/Sec</td>
            <td>Kbytes/Sec</td>
            <td>Mbits/Sec</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Bits/Sec</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Bytes/Sec</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Kbits/Sec</td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
        </tr>
        <tr>
            <td>Kbytes/Sec</td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
        </tr>
        <tr>
            <td>Mbits/Sec</td>
            <td>10<sup>6</sup></td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Mbytes/Sec</td>
            <td>8x10<sup>6</sup></td>
            <td>10<sup>6</sup></td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
        </tr>
        <tr>
            <td>Gbits/Sec</td>
            <td>10<sup>9</sup></td>
            <td>1.25x10<sup>8</sup></td>
            <td>10<sup>6</sup></td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
        </tr>
        <tr>
            <td>Gbytes/Sec</td>
            <td>8x10<sup>9</sup></td>
            <td>10<sup>9</sup></td>
            <td>8x10<sup>6</sup></td>
            <td>10<sup>6</sup></td>
            <td>8000</td>
        </tr>
        <tr>
            <td>Tbits/Sec</td>
            <td>10<sup>12</sup></td>
            <td>1.25x10<sup>11</sup></td>
            <td>10<sup>9</sup></td>
            <td>1.25x10<sup>8</sup></td>
            <td>10<sup>6</sup></td>
        </tr>
        <tr>
            <td>Tbytes/Sec</td>
            <td>8x10<sup>12</sup></td>
            <td>10<sup>12</sup></td>
            <td>8x10<sup>9</sup></td>
            <td>10<sup>9</sup></td>
            <td>8x10<sup>6</sup></td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Storage Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Mbytes/Sec</td>
            <td>Gbits/Sec</td>
            <td>Gbytes/Sec</td>
            <td>Tbits/Sec</td>
            <td>Tbytes/Sec</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Bits/Sec</td>
            <td>1.25x10<sup>-7</sup></td>
            <td>10<sup>-9</sup></td>
            <td>1.25x10<sup>-10</sup></td>
            <td>10<sup>-12</sup></td>
            <td>1.25x10<sup>-13</sup></td>
        </tr>
        <tr>
            <td>Bytes/Sec</td>
            <td>1x10<sup>-6</sup></td>
            <td>8x10<sup>-9</sup></td>
            <td>1x10<sup>-9</sup></td>
            <td>8x10<sup>-12</sup></td>
            <td>1x10<sup>-12</sup></td>
        </tr>
        <tr>
            <td>Kbits/Sec</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
            <td>1.25x10<sup>-7</sup></td>
            <td>10<sup>-9</sup></td>
            <td>1.25x10<sup>-10</sup></td>
        </tr>
        <tr>
            <td>Kbytes/Sec</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
            <td>1x10<sup>-6</sup></td>
            <td>8x10<sup>-9</sup></td>
            <td>1x10<sup>-9</sup></td>
        </tr>
        <tr>
            <td>Mbits/Sec</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
            <td>1.25x10<sup>-7</sup></td>
        </tr>
        <tr>
            <td>Mbytes/Sec</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
            <td>1x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Gbits/Sec</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Gbytes/Sec</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
        </tr>
        <tr>
            <td>Tbits/Sec</td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
        </tr>
        <tr>
            <td>Tbytes/Sec</td>
            <td>1x10<sup>6</sup></td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of Data Storage is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 Kilobytes/Sec to other Measurements

In the below example we will convert 1 Kilobytes/Sec to its equivalent values of Kilobits/Sec, Bytes/Sec, Bits/Sec, Megabits/Sec and Gigabytes/Sec.

**Convert from 1 Kilobytes/Sec to Kilobits/Sec :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Kilobits/Sec = 8 x 1 Kilobytes/Sec
    </h5>
    <h5 class="numforleft">
        Kilobits/Sec = 8
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes/Sec to Bytes/Sec :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Bytes/Sec = 1000 x 1 Kilobytes/Sec
    </h5>
    <h5 class="numforleft">
        Bytes/Sec =1000
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes/Sec to Bits/Sec :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Bits/Sec = 8000 x 1 Kilobytes/Sec
    </h5>
    <h5 class="numforleft">
        Bits/Sec = 8000
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes/Sec to Megabits/Sec :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Megabits/Sec = 0.008 x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Megabits/Sec = 0.008
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes/Sec to Gigabytes/Sec :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Gigabytes/Sec = (1 x 10<sup>-6</sup>) x 1 Kilobytes/Sec
    </h5>
    <h5 class="numforleft">
        Gigabytes/Sec = (1 x 10<sup>-6</sup>)
    </h5>
</div>
{{</formula>}}
