﻿---
title: Resistance Converter
summary: Convert between &#x2126;, K&#x2126; & M&#x2126;
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>&#x2126;</option>
                            <option>K&#x2126;</option>
                            <option selected>M&#x2126;</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iOhm" class="label">&#x2126;</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iOhm" placeholder="Ohms" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKOhms" class="label">K&#x2126;</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKOhm" placeholder="KOhms" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMOhms" class="label">M&#x2126;</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMOhm" placeholder="MOhms" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        iFrom = parseFloat(convertResistanceToNumber(iFrom, sFromUnit));
        document.getElementById("iOhm").value = parseFloat(convertNumberToResistance(iFrom, 0).toFixed(10));
        document.getElementById("iKOhm").value = parseFloat(convertNumberToResistance(iFrom, 1).toFixed(10));
        document.getElementById("iMOhm").value = parseFloat(convertNumberToResistance(iFrom, 2).toFixed(10));
    }
</script>
{{</rawhtml>}}

## How To Use

Resistors are one of the most used passive elements of any electrical or electronic circuit. It's necessary to convert the resistance values among other units of measurement whenever you are using different resistance values in your circuits. You can read more about [Resistors](/learn/electronics/basics/resistor/) in Education. We have [Series](/tools/calculators/series-resistance-calculator) and [Parallel](/tools/calculators/series-resistance-calculator) resistance calculator listed in our [Calculators](/tools/calculators) section. In our resistance converter we support conversion between **&Omega;, K&Omega; and M&Omega;**.

You can select resistance unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

Conversions among the resistance values is essential as we will be using the different resistor values with different units. For example, you are using 3 different resistors of values 470&Omega;, 820&Omega; and 220&Omega; respectively in series. The total resistance of these three resistors will be 1510&Omega;, which is actually 1.51K&Omega;. You can achieve this by using our resistance converter.
            
You can use our resources such as [standard resistor values](/resources/standards/standard-resistor-values), [SMD Packages](/resources/standards/smd-passive-packages) and [SMD resistor value finder](/tools/calculators/smd-resistor-calculator) tools to simplify your work during designing circuits.

## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below table shows all multiplication factors used in our converter.
Multiplication Factor for Resistance Conversion

{{<rawhtml>}}
<table class="centered highlight responsive-table">
    <thead>
        <tr>
            <th style="text-align:center;" colspan="4"></th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="3">Convert to</th>
        </tr>
        <tr>
            <td>&Omega;</td>
            <td>K&Omega;</td>
            <td>M&Omega;</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>&Omega;</td>
            <td>1</td>
            <td>10<sup>3</sup></td>
            <td>10<sup>6</sup></td>
        </tr>
        <tr>
            <td>K&Omega;</td>
            <td>10<sup>-3</sup></td>
            <td>1</td>
            <td>10<sup>3</sup></td>
        </tr>
        <tr>
            <td>M&Omega;</td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-3</sup></td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of resistance is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 &Omega; to other Measurements

In the below example we will convert 1 &Omega; to its equivalent values of K&Omega; and M&Omega;.

**Convert from 1 &Omega; to K&Omega; :**

{{<rawhtml>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        K&Omega; = (1 x 10<sup>-3</sup>) x 1 &Omega;
    </h5>
    <h5 class="numforleft">
        K&Omega; = 0.001
    </h5>
</div>
{{</rawhtml>}}

**Convert from 1 &Omega; to M&Omega; :**

{{<rawhtml>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        M&Omega; = (1 x 10<sup>-6</sup>) x 1 &Omega;
    </h5>
    <h5 class="numforleft">
        M&Omega; = 0.000001
    </h5>
</div>
{{</rawhtml>}}