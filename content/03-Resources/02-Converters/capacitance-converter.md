﻿---
title: Capacitance Converter
summary: Convert between pF, nF, uF, mF & F.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">From</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="1000" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>F</option>
                            <option>mF</option>
                            <option>uF</option>
                            <option selected>nF</option>
                            <option>pF</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFarad" class="label">Farad</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFarad" placeholder="farad" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMilliFarad" class="label">Millifarad</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMilliFarad" placeholder="millifarad" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMicroFarad" class="label">Microfarad</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMicroFarad" placeholder="microfarad" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iNanoFarad" class="label">Nanofarad</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iNanoFarad" placeholder="nanofarad" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iPicoFarad" class="label">Picoarad</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iPicoFarad" placeholder="picofarad" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        iFrom = parseFloat(convertCapacitanceToNumber(iFrom, sFromUnit));
        document.getElementById("iFarad").value = parseFloat(convertNumberToCapacitance(iFrom, 0).toFixed(10));
        document.getElementById("iMilliFarad").value = parseFloat(convertNumberToCapacitance(iFrom, 1).toFixed(10));
        document.getElementById("iMicroFarad").value = parseFloat(convertNumberToCapacitance(iFrom, 2).toFixed(10));
        document.getElementById("iNanoFarad").value = parseFloat(convertNumberToCapacitance(iFrom, 3).toFixed(10));
        document.getElementById("iPicoFarad").value = parseFloat(convertNumberToCapacitance(iFrom, 4).toFixed(10));
    }
</script>
{{</rawhtml>}}

## How To Use

Capacitors are one of the most used passive elements of any electrical or electronic circuit. It's necessary to convert the capacitance values among other units of measurement whenever you are using different capacitor values in your circuits. You can read more about [Capacitor](/learn/electronics/basics/capacitor) in Education. We have [Series](/tools/calculators/series-capacitance-calculator) and [Parallel](/tools/calculators/parallel-capacitance-calculator) capacitance calculator listed in our [Calculators](/tools/calculators) section. In our capacitance converter we support conversion between **Farad, Millifarad, Microfarad, Nanofarad and Picofarad.**

You can select capacitance unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

Conversions among the capacitor values is essential as we will be using the different capacitor values with different units. For example, we may use 100nF capacitor in our circuit with other components. Which we can convert to 0.1&micro;F to make our circuit debugging process easier.

Below are few popular capacitor values used in the circuits with different units.

> 100pF = 0.1nF
>
> 1000pF = 1nF
>
> 100nF = 0.1&micro;F

During designing a circuit, by using our capacitance converter you can reduce the chances of confusion that occur when using capacitor with different multipliers of values.

## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below table shows all multiplication factors used in our converter.</p>

{{<rawhtml>}}
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Capacitance Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Farad</td>
            <td>Millifarad</td>
            <td>Microfarad</td>
            <td>Nanofarad</td>
            <td>Picofarad</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Farad</td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-9</sup></td>
            <td>10<sup>-12</sup></td>
        </tr>
        <tr>
            <td>Millifarad</td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
            <td>10<sup>-9</sup></td>
        </tr>
        <tr>
            <td>Microfarad</td>
            <td>10<sup>6</sup></td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
            <td>10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Nanofarad</td>
            <td>10<sup>9</sup></td>
            <td>10<sup>6</sup></td>
            <td>10<sup>3</sup></td>
            <td>1</td>
            <td>10<sup>-3</sup></td>
        </tr>
        <tr>
            <td>Picofarad</td>
            <td>10<sup>12</sup></td>
            <td>10<sup>9</sup></td>
            <td>10<sup>6</sup></td>
            <td>10<sup>3</sup></td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of capacitance is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 Farad to other Measurements

In the below example we will convert 1 Farad to its equivalent values of Millifarad, Microfarad, Nanofarad, and Picofarad.

**Convert from 1 Farad to Millifarad :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Millifarad = (1 x 10<sup>-3</sup>) x 1 Farad
    </h5>
    <h5 class="numforleft">
        Millifarad = 0.001
    </h5>
</div>
{{</formula>}}

**Convert from 1 Farad to Microfarad :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Microfarad = (1 x 10<sup>-6</sup>) x 1 Farad
    </h5>
    <h5 class="numforleft">
        Microfarad = 0.000001
    </h5>
</div>
{{</formula>}}

**Convert from 1 Farad to Nanofarad :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Nanofarad = (1 x 10<sup>-9</sup>) x 1 Farad
    </h5>
    <h5 class="numforleft">
        Nanofarad = 0.000000001
    </h5>
</div>
{{</formula>}}

**Convert from 1 Farad to Picofarad :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Picofarad = (1 x 10<sup>-12</sup>) x 1 Farad
    </h5>
    <h5 class="numforleft">
        Picofarad = 0.000000000001
    </h5>
</div>
{{</formula>}}