﻿---
title: Energy Converter
notoc: true
draft: true
summary: Convert between kilojoule, kilowatt, electron volt & watt hour.
---

{{<rawhtml>}}
<div class="row">
    <div class="body-content">
        <h3 class="section scrollspy">Energy Converter</h3>
        <form class="form-horizontal font-comic-sans">
            <div class="form-group">
                <label class="col-sm-2">From</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="from" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option selected="selected">Joule</option>
                        <option>Kilojoule</option>
                        <option>Gram Calorie</option>
                        <option>Kilo Calorie</option>
                        <option>Watt hour</option>
                        <option>Kilowatt hour</option>
                        <option>Electron Volt</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">To</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="to" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option>Joule</option>
                        <option>Kilojoule</option>
                        <option selected="selected">Gram Calorie</option>
                        <option>Kilo Calorie</option>
                        <option>Watt hour</option>
                        <option>Kilowatt hour</option>
                        <option>Electron Volt</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
{{</rawhtml>}}