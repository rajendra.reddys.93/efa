﻿---
title: Digital Storage Converter
wrong: true
summary: Convert between KB, MB, Kb, Mb, GB & Gb.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>Bits</option>
                            <option>Bytes</option>
                            <option selected>Kilobits</option>
                            <option>Kilobytes</option>
                            <option>Megabits</option>
                            <option>Megabytes</option>
                            <option>Gigabits</option>
                            <option>Gigabytes</option>
                            <option>Terabits</option>
                            <option>Terabytes</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBits" class="label">Bits</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBits" placeholder="bits" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iBytes" class="label">Bytes</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iBytes" placeholder="bytes" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKBits" class="label">Kilobits</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKBits" placeholder="kilobits" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKBytes" class="label">Kilobytes</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKBytes" placeholder="kilobytes" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMBits" class="label">Megabits</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMBits" placeholder="megabits" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iMBytes" class="label">Megabytes</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iMBytes" placeholder="megabytes" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iGBits" class="label">Gigabits</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iGBits" placeholder="gigabits" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iGBytes" class="label">Gigabytes</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iGBytes" placeholder="gigabytes" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iTBits" class="label">Terabits</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iTBits" placeholder="terabits" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iTBytes" class="label">Terabytes</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iTBytes" placeholder="terabytes" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        iFrom = parseFloat(convDataRateToBytes(iFrom, sFromUnit));
        document.getElementById("iBits").value = parseFloat(convBytesToDataRate(iFrom, 0).toFixed(10));
        document.getElementById("iBytes").value = parseFloat(convBytesToDataRate(iFrom, 1).toFixed(10));
        document.getElementById("iKBits").value = parseFloat(convBytesToDataRate(iFrom, 2).toFixed(10));
        document.getElementById("iKBytes").value = parseFloat(convBytesToDataRate(iFrom, 3).toFixed(10));
        document.getElementById("iMBits").value = parseFloat(convBytesToDataRate(iFrom, 4).toFixed(10));
        document.getElementById("iMBytes").value = parseFloat(convBytesToDataRate(iFrom, 5).toFixed(10));
        document.getElementById("iGBits").value = parseFloat(convBytesToDataRate(iFrom, 6).toFixed(10));
        document.getElementById("iGBytes").value = parseFloat(convBytesToDataRate(iFrom, 7).toFixed(10));
        document.getElementById("iTBits").value = parseFloat(convBytesToDataRate(iFrom, 8).toFixed(10));
        document.getElementById("iTBytes").value = parseFloat(convBytesToDataRate(iFrom, 9).toFixed(10));
    }
</script>
{{</rawhtml>}}

## How To Use

In our Data Storage converter we support conversion between **Bits, Bytes, Kilobits, Kilobytes, Megabits, Megabytes, Gigabits, Gigabytes, Terabits and Terabytes.** You can select length unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation


## Formula Explanation

We are using below multiplication factors in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

Below table shows all multiplication factors used in our converter.
{{<rawhtml>}}
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Storage Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Bits</td>
            <td>Bytes</td>
            <td>Kilobits</td>
            <td>Kilobytes</td>
            <td>Megabits</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Bits</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Bytes</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Kilobits</td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
        </tr>
        <tr>
            <td>Kilobytes</td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
        </tr>
        <tr>
            <td>Megabits</td>
            <td>10<sup>6</sup></td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
        </tr>
        <tr>
            <td>Megabytes</td>
            <td>8x10<sup>6</sup></td>
            <td>10<sup>6</sup></td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
        </tr>
        <tr>
            <td>Gigabits</td>
            <td>10<sup>9</sup></td>
            <td>1.25x10<sup>8</sup></td>
            <td>10<sup>6</sup></td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
        </tr>
        <tr>
            <td>Gigabytes</td>
            <td>8x10<sup>9</sup></td>
            <td>10<sup>9</sup></td>
            <td>8x10<sup>6</sup></td>
            <td>10<sup>6</sup></td>
            <td>8000</td>
        </tr>
        <tr>
            <td>Terabits</td>
            <td>10<sup>12</sup></td>
            <td>1.25x10<sup>11</sup></td>
            <td>10<sup>9</sup></td>
            <td>1.25x10<sup>8</sup></td>
            <td>10<sup>6</sup></td>
        </tr>
        <tr>
            <td>Terabytes</td>
            <td>8x10<sup>12</sup></td>
            <td>10<sup>12</sup></td>
            <td>8x10<sup>9</sup></td>
            <td>10<sup>9</sup></td>
            <td>8x10<sup>6</sup></td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th style="text-align:center;" colspan="6">Multiplication Factor for Storage Conversion</th>
        </tr>
        <tr>
            <th style="text-align:center;" colspan="1" rowspan="2">Convert from</th>
            <th style="text-align:center;" colspan="5">Convert to</th>
        </tr>
        <tr>
            <td>Megabytes</td>
            <td>Gigabits</td>
            <td>Gigabytes</td>
            <td>Terabits</td>
            <td>Terabytes</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Bits</td>
            <td>1.25x10<sup>-7</sup></td>
            <td>10<sup>-9</sup></td>
            <td>1.25x10<sup>-10</sup></td>
            <td>10<sup>-12</sup></td>
            <td>1.25x10<sup>-13</sup></td>
        </tr>
        <tr>
            <td>Bytes</td>
            <td>1x10<sup>-6</sup></td>
            <td>8x10<sup>-9</sup></td>
            <td>1x10<sup>-9</sup></td>
            <td>8x10<sup>-12</sup></td>
            <td>1x10<sup>-12</sup></td>
        </tr>
        <tr>
            <td>Kilobits</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
            <td>1.25x10<sup>-7</sup></td>
            <td>10<sup>-9</sup></td>
            <td>1.25x10<sup>-10</sup></td>
        </tr>
        <tr>
            <td>Kilobytes</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
            <td>1x10<sup>-6</sup></td>
            <td>8x10<sup>-9</sup></td>
            <td>1x10<sup>-9</sup></td>
        </tr>
        <tr>
            <td>Megabits</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
            <td>10<sup>-6</sup></td>
            <td>1.25x10<sup>-7</sup></td>
        </tr>
        <tr>
            <td>Megabytes</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
            <td>8x10<sup>-6</sup></td>
            <td>1x10<sup>-6</sup></td>
        </tr>
        <tr>
            <td>Gigabits</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
            <td>0.001</td>
            <td>1.25x10<sup>-4</sup></td>
        </tr>
        <tr>
            <td>Gigabytes</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
            <td>0.008</td>
            <td>0.001</td>
        </tr>
        <tr>
            <td>Terabits</td>
            <td>1.25x10<sup>5</sup></td>
            <td>1000</td>
            <td>125</td>
            <td>1</td>
            <td>0.125</td>
        </tr>
        <tr>
            <td>Terabytes</td>
            <td>1x10<sup>6</sup></td>
            <td>8000</td>
            <td>1000</td>
            <td>8</td>
            <td>1</td>
        </tr>
    </tbody>
</table>
{{</rawhtml>}}

## Example Conversion

The conversion between different measuring units of Data Storage is straight forward. That is you can use multiplication factor listed in the above table for conversion among other units. We will explain how to use multiplication factor with an example.

### Convert from 1 Kilobytes to other Measurements

In the below example we will convert 1 Kilobytes to its equivalent values of Kilobits, Bytes, Bits, Megabits and Gigabytes.

**Convert from 1 Kilobytes to Kilobits :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Kilobits = 8 x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Kilobits = 8
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes to Bytes :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Bytes = 1000 x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Bytes =1000
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes to Bits :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Bits = 8000 x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Bits = 8000
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes to Megabits :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Megabits = 0.008 x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Megabits = 0.008
    </h5>
</div>
{{</formula>}}

**Convert from 1 Kilobytes to Gigabytes :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        Gigabytes = (1 x 10<sup>-6</sup>) x 1 Kilobytes
    </h5>
    <h5 class="numforleft">
        Gigabytes = (1 x 10<sup>-6</sup>)
    </h5>
</div>
{{</formula>}}