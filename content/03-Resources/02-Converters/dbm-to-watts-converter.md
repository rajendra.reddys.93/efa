﻿---
title: dBm-Watts Converter
summary: Convert between dBm & Watts.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="10" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option>dBm</option>
                            <option>Watts</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="idBm" class="label">dBm</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="idBm" placeholder="dBm" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iWatts" class="label">Watts</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iWatts" placeholder="watts" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        switch (sFromUnit) {
            case 0:
                document.getElementById("idBm").value = parseFloat(iFrom.toFixed(10));
                document.getElementById("iWatts").value = parseFloat(Math.pow(10, ((iFrom - 30) / 10)).toFixed(10));
                break;
            case 1:
                document.getElementById("idBm").value = parseFloat(((10 * Math.log10((iFrom / 1))) + 30).toFixed(10));
                document.getElementById("iWatts").value = parseFloat(iFrom.toFixed(10));
                break;
        }
    }
</script>
{{</rawhtml>}}

## How To Use

The dBm to Watts converter is essentially a great tool to convert "power" value from one unit of measurement to other. In the above calculator we support conversion between **dBm and Watts**. You can select measurement unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

You can convert power between two measuring units "dBm" and "Watts" using our tool.

## Formula Explanation

We are using below conversion formula in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.

### Convert from dBm to other measurements

The below formula show how to convert from dBm to other power measurements.

**Convert from dBm to Watts :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>W</sub> = <table class="frac"><tr><td>10<table class="supsub"><tr><td><div><table class=comb><tr><td class=bigmiddle>(<td class=bincoef><table class="frac"><tr><td>P(dBm)<br />&#x2013;&#x2013;&#x2013;<br />10</td></tr></table><br />&nbsp;<td class=bigmiddle>)</table></div><br />&nbsp;</td></tr></table><br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />1000</td></tr></table>
    </h5>
</div>
{{</formula>}}

{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>W</sub> = 10<table class="supsub"><tr><td><div><table class=comb><tr><td class=bigmiddle>(<td class=bincoef><table class="frac"><tr><td>P(dBm) - 30<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />10</td></tr></table><br />&nbsp;<td class=bigmiddle>)</table></div><br />&nbsp;</td></tr></table>
    </h5>
</div>
{{</formula>}}
            
### Convert from Watts to other Measurements

The below formula show how to convert from Watts to other power measurements.

**Convert from Watts to dBm :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>dBm</sub> = 10 x log<sub>10</sub><table class="comb"><tr><td class="bigmiddle">(<td class="bincoef"><table class="frac"><tr><td>1000 x P<sub>W</sub><br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />1W</td></tr></table><br />&nbsp;<td class="bigmiddle">)</table>
    </h5>
</div>
{{</formula>}}

{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>dBm</sub> = 10 x log<sub>10</sub><table class="comb"><tr><td class="bigmiddle">(<td class="bincoef"><table class="frac"><tr><td>P<sub>W</sub><br />&#x2013;&#x2013;&#x2013;<br />1W</td></tr></table><br />&nbsp;<td class="bigmiddle">)</table> + 30
    </h5>
</div>
{{</formula>}}
            
> In all above formula, where
>
> dBm - Decibel Milli Watt
> W - Watts

## Example Conversion

Let us play with our conversions taking some examples.

### Convert from 40 dBm to other measurements

The below formula show how to convert from 40 dBm to other power measurements.

**Convert from 40 dBm to Watts :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>W</sub> = 10<table class="supsub"><tr><td><div><table class=comb><tr><td class=bigmiddle>(<td class=bincoef><table class="frac"><tr><td>40 dBm - 30<br />&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;&#x2013;<br />10</td></tr></table><br />&nbsp;<td class=bigmiddle>)</table></div><br />&nbsp;</td></tr></table>
    </h5>
</div>
<div class="inlineformula numfor">
    <h5 class="numforleft">
        P<sub>W</sub> = 10 W
    </h5>
</div>
{{</formula>}}

### Convert from 10 Watts to other Measurements

The below formula show how to convert from 10 Watts to other power measurements.

**Convert from 10 Watts to dBm :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft" style="margin-top:-1.5em;">
        P<sub>dBm</sub> = 10 x log<sub>10</sub><table class="comb"><tr><td class="bigmiddle">(<td class="bincoef"><table class="frac"><tr><td>10 W<br />&#x2013;&#x2013;&#x2013;<br />1W</td></tr></table><br />&nbsp;<td class="bigmiddle">)</table> + 30
    </h5>
</div>
<div class="inlineformula numfor">
    <h5 class="numforleft">
        P<sub>dBm</sub> = 40 dBm
    </h5>
</div>
{{</formula>}}
