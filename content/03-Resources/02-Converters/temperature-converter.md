﻿---
title: Temperature Converter
summary: Convert between F, C & K.
---

{{<rawhtml>}}
<form>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFrom" class="label">Convert</label>
		</div>
	  	<div class="field-body">
            <div class="field has-addons">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFrom" placeholder="From" oninput="convert()" onchange="convert()" value="100" type="number">
                </p>
                <p class="control">
                    <span class="select is-info">
                        <select id="sFromUnit" onchange="convert()">
                            <option selected>Fahrenheit (F)</option>
                            <option>Celsius (C)</option>
                            <option>Kelvin (K)</option>
                        </select>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iFah" class="label">Fahrenheit (F)</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iFah" placeholder="fahrenheit" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iCel" class="label">Celsius (C)</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iCel" placeholder="celcius" value="" type="number">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
	  	<div class="field-label is-normal">
        	<label for="iKel" class="label">Kelvin (K)</label>
		</div>
        <div class="field-body">
            <div class="field">
				<p class="control is-expanded">
                    <input class="input is-info" id="iKel" placeholder="kelvin" value="" type="number">
                </p>
            </div>
        </div>
    </div>
</form>
<script>
    function convert() {
        var iFrom;
        var sFromUnit;
        sFromUnit = document.getElementById("sFromUnit").selectedIndex;
        iFrom = parseFloat(document.getElementById("iFrom").value);
        switch (sFromUnit) {
            case 0:
                document.getElementById("iFah").value = parseFloat(iFrom);
                document.getElementById("iCel").value = parseFloat(convFahToCel(iFrom, 10).toFixed(10));
                document.getElementById("iKel").value = parseFloat(convFahToKel(iFrom, 10).toFixed(10));
                break;
            case 1:
                document.getElementById("iFah").value = parseFloat(convCelToFah(iFrom, 10).toFixed(10));
                document.getElementById("iCel").value = parseFloat(iFrom);
                document.getElementById("iKel").value = parseFloat(convCelToKel(iFrom, 10).toFixed(10));
                break;
            case 2:
                document.getElementById("iFah").value = parseFloat(convKelToFah(iFrom, 10).toFixed(10));
                document.getElementById("iCel").value = parseFloat(convKelToCel(iFrom, 10).toFixed(10));
                document.getElementById("iKel").value = parseFloat(iFrom);
                break;
            default:
                document.getElementById("iFrom").setAttribute('type', Number);
                break;
        }
    }
</script>
{{</rawhtml>}}

## How To Use

The Temperature converter is essentially a great tool to convert "temperature" value from one unit of measurement to other. In the above calculator we support conversion between **&deg;C (Celsius), &deg;F (Fahrenheit) and K (Kelvin).** You can select measurement unit from the drop-down list and enter the value to be converted in the "Convert From" text input field. As you type in the input it will show the converted values opposite to the other measuring unit.

The converter accepts scientific notations such as "e" or "E" (meaning exponent) and converts immediately. Since, it is not possible to show the results as accurate as natural, we are rounding off the results to 10 decimal places. This means that the results will be rounded to avoid numbers getting too long.

## Conversion Explanation

In any electronics device or circuit, temperature is major consideration for performance  analysis. The rise in temperature above the recommended values of component used, will harm that component. Each and every component will have different temperature threshold values. You can find the thresholds in the technical data-sheets.

Below are general temperature grades used across different product category. However few manufacturers use their own standards. Be sure to check the technical data-sheets of any component before using in your project.

> Commercial : 0&deg;C to 85&deg;C 
>
> Industrial : -40&deg;C to 100&deg;C
>
> Automotive : -40&deg;C to 125&deg;C
>
> Extended : -40&deg;C to 125&deg;C
>
> Military : -55&deg;C to 125&deg;C

Our [Temperature Converter]({{< ref temperature-converter.md >}}) will come in handy whenever you need any conversions between different temperature measuring units.

## Formula Explanation

We are using below conversion formula in our converter. To the best of our knowledge, we hope that the conversion we are performing are accurate. If you find any errors, please write to us at {{<mail>}}. We will rectify.
            
### Convert from Fahrenheit to other Measurements

The below formula show how to convert from Fahrenheit to other temperature measurements.

**Convert from Fahrenheit to Celsius :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;C = (&deg;F - 32) &frasl; 32
    </h5>
</div>
{{</formula>}}

**Convert from Fahrenheit to Kelvin :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        K = ((&deg;F - 32) &frasl; 1.8) + 273.15
    </h5>
</div>
{{</formula>}}

### Convert from Celsius to other Measurements

The below formula show how to convert from Celsius to other temperature measurements.

**Convert from Celsius to Fahrenheit :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
            &deg;F = (&deg;C * 1.8) + 32
    </h5>
</div>
{{</formula>}}

**Convert from Celsius to Kelvin :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        K = &deg;C + 273.15
    </h5>
</div>
{{</formula>}}

### Convert from Kelvin to other Measurements

The below formula show how to convert from Kelvin to other temperature measurements.

**Convert from Kelvin to Fahrenheit :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;F = ((K - 273.15) * 1.8) + 32
    </h5>
</div>
{{</formula>}}

**Convert from Kelvin to Celsius :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;C = K - 273.15
    </h5>
</div>
{{</formula>}}

> In all above formula, where
>
> C - Celsius<br />
>
> F - Fahrenheit, and<br />
> 
> K - Kelvin

## Example Conversion

Let us play with our conversions taking some examples.

### Convert from 80&deg;Fahrenheit to other Measurements

In the below example we will convert 80&deg;Fahrenheit to its equivalent values of &deg;Celsius and Kelvin.

**Convert from 80&deg;Fahrenheit to Celsius :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;C = (&deg;F - 32) &frasl; 32
    </h5>
    <h5 class="numforleft">
        &deg;C = (80&deg;F - 32) &frasl; 32
    </h5>
    <h5 class="numforleft">
        &deg;C = 26.6667
    </h5>
</div>
{{</formula>}}

**Convert from 80&deg;Fahrenheit to Kelvin :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        K = ((&deg;F - 32) &frasl; 1.8) + 273.15
    </h5>
    <h5 class="numforleft">
        K = ((80&deg;F - 32) &frasl; 1.8) + 273.15
    </h5>
    <h5 class="numforleft">
        K = 299.8167
    </h5>
</div>
{{</formula>}}

### Convert from 35.4&deg;Celsius to other Measurements</h5>

In the below example we will convert 35.4&deg;Celsius to its equivalent values of &deg;Fahrenheit and Kelvin.

**Convert from 35.4&deg;Celsius to &deg;Fahrenheit :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;F = (&deg;C * 1.8) + 32
    </h5>
    <h5 class="numforleft">
        &deg;F = (35.4&deg;C * 1.8) + 32
    </h5>
    <h5 class="numforleft">
        &deg;F = 95.72
    </h5>
</div>
{{</formula>}}

**Convert from 35.4&deg;Celsius to Kelvin :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        K = &deg;C + 273.15
    </h5>
    <h5 class="numforleft">
        K = 35.4&deg;C + 273.15
    </h5>
    <h5 class="numforleft">
        K = 308.55
    </h5>
</div>
{{</formula>}}

### Convert from 300Kelvin to other Measurements

In the below example we will convert 300Kelvin to its equivalent values of &deg;Fahrenheit and &deg;Celsius.
                
**Convert from 300Kelvin to &deg;Fahrenheit :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;F = ((K - 273.15) * 1.8) + 32
    </h5>
    <h5 class="numforleft">
        &deg;F = ((300Kelvin - 273.15) * 1.8) + 32
    </h5>
    <h5 class="numforleft">
        &deg;F = 80.33
    </h5>
</div>
{{</formula>}}

**Convert from 300Kelvin to &deg;Celsius :**
{{<formula>}}
<div class="inlineformula numfor">
    <h5 class="numforleft">
        &deg;C = K - 273.15
    </h5>
    <h5 class="numforleft">
        &deg;C = 300Kelvin - 273.15
    </h5>
    <h5 class="numforleft">
        &deg;C = 26.85
    </h5>
</div>
{{</formula>}}
