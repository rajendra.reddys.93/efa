﻿---
title: Speed Converter
notoc: true
draft: true
summary: Convert between Kmps, Miles per second, foot per second.
---

{{<rawhtml>}}
<div class="row">
    <div class="body-content">
        <h3 class="section scrollspy">Speed Converter</h3>
        <form class="form-horizontal font-comic-sans">
            <div class="form-group">
                <label class="col-sm-2">From</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="from" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option selected="selected">Meters per Second</option>
                        <option>Kilometers per Second</option>
                        <option>Miles per Second</option>
                        <option>Foot per Second</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">To</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="to" aria-label="...">
                </div>
                <div class="col-sm-4">
                    <select class="form-control font-lucida-sans">
                        <option>Meters per Second</option>
                        <option selected="selected">Kilometers per Second</option>
                        <option>Miles per Second</option>
                        <option>Foot per Second</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>
{{</rawhtml>}}