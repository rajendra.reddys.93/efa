﻿---
title: IP Marking
weight: 11
notoc: true
summary: Ingress Protection marking with brief explanation.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="5">Code breakdown</th>
                </tr>
                <tr>
                    <th style="text-align:center;">IP indication</th>
                    <th style="text-align:center;">Solid particle protection</th>
                    <th style="text-align:center;">Liquid ingress protection</th>
                    <th style="text-align:center;">Mechanical impact resistance</th>
                    <th style="text-align:center;">Other protections</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>IP</td>
                    <td>Single numerical (0-6)</td>
                    <td>Single numerical (0-9)</td>
                    <td>Single numerical (0-9)</td>
                    <td>Single letter</td>
                </tr>
                <tr>
                    <td>Mandatory</td>
                    <td>Mandatory</td>
                    <td>Mandatory</td>
                    <td>No longer used</td>
                    <td>Optional</td>
                </tr>
            </tbody>
        </table>
        <br />
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="5">First digit: Solid Particle Protection</th>
                </tr>
                <tr>
                    <th style="text-align:center;">Level sized</th>
                    <th style="text-align:center;">Effective against</th>
                    <th style="text-align:center;">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>-</td>
                    <td>No protection against contact and ingress of objects</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>>50 mm</td>
                    <td>Any large surface of the body, such as the back of a hand, but no protection against deliberate contact with a body part</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>>12.5 mm</td>
                    <td>Fingers or similar objects</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>>2.5 mm</td>
                    <td>Tools, thick wires, etc.</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>>1 mm</td>
                    <td>Most wires, slender screws, large ants etc.</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Dust protected</td>
                    <td>Ingress of dust is not entirely prevented, but it must not enter in sufficient quantity to interfere with the satisfactory operation of the equipment.</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Dust tight</td>
                    <td>No ingress of dust; complete protection against contact (dust tight). A vacuum must be applied. Test duration of up to 8 hours based on air flow.</td>
                </tr>
            </tbody>
        </table>
        <br />
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="5">Second digit: Liquid Ingress Protection</th>
                </tr>
                <tr>
                    <th style="text-align:center;">Level</th>
                    <th style="text-align:center;">Protection against</th>
                    <th style="text-align:center;">Effective against</th>
                    <th style="text-align:center;">Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>None</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Dripping water</td>
                    <td>Dripping water (vertically falling drops) shall have no harmful effect on the specimen when mounted in an upright position onto a turntable and rotated at 1 RPM.</td>
                    <td>Test duration: 10 minutes, Water equivalent to 1 mm rainfall per minute</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Dripping water when tilted at 15°</td>
                    <td>Vertically dripping water shall have no harmful effect when the enclosure is tilted at an angle of 15° from its normal position. A total of four positions are tested within two axes.</td>
                    <td>Test duration: 2.5 minutes for every direction of tilt (10 minutes total), Water equivalent to 3 mm rainfall per minute</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Spraying water</td>
                    <td>
                        Water falling as a spray at any angle up to 60° from the vertical shall have no harmful effect, utilizing either: a) an oscillating fixture, or b) A spray nozzle with a counterbalanced shield.
                        <br />
                        <br />Test a) is conducted for 5 minutes, then repeated with the specimen tilted 90° for the second 5-minute test.
                        <br />Test b) is conducted (with shield in place) for 5 minutes minimum.
                    </td>
                    <td>
                        For a Spray Nozzle:
                        <br />Test duration: 1 minute per square meter for at least 5 minutes
                        <br />Water volume: 10 litres per minute
                        <br />Pressure: 50–150 kPa
                        <br />
                        <br />For an oscillating tube:
                        <br />Test duration: 10 minutes
                        <br />Water Volume: 0.07 l/min per hole
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Splashing of water</td>
                    <td>
                        Water splashing against the enclosure from any direction shall have no harmful effect, utilizing either: a) an oscillating fixture, or b) A spray nozzle with no shield.
                        <br />
                        <br />Test a) is conducted for 10 minutes.
                        <br />Test b) is conducted (without shield) for 5 minutes minimum.
                    </td>
                    <td>Oscillating tube: Test duration: 10 minutes, or spray nozzle (same as IPX3 spray nozzle with the shield removed)</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Water jets</td>
                    <td>Water projected by a nozzle (6.3 mm) against enclosure from any direction shall have no harmful effects.</td>
                    <td>
                        Test duration: 1 minute per square meter for at least 3 minutes
                        <br />Water volume: 12.5 litres per minute
                        <br />Pressure: 30 kPa at distance of 3 m
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Powerful water jets</td>
                    <td>Water projected in powerful jets (12.5 mm nozzle) against the enclosure from any direction shall have no harmful effects.</td>
                    <td>
                        Test duration: 1 minute per square meter for at least 3 minutes
                        <br />Water volume: 100 litres per minute
                        <br />Pressure: 100 kPa at distance of 3 m
                    </td>
                </tr>
                <tr>
                    <td>6K</td>
                    <td>Powerful water jets with increased pressure</td>
                    <td>Water projected in powerful jets (6.3 mm nozzle) against the enclosure from any direction, under elevated pressure, shall have no harmful effects. Found in DIN 40050, and not IEC 60529.</td>
                    <td>
                        Test duration: at least 3 minutes
                        <br />Water volume: 75 litres per minute
                        <br />Pressure: 1000 kPa at distance of 3 m
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Immersion, up to 1 m depth</td>
                    <td>Ingress of water in harmful quantity shall not be possible when the enclosure is immersed in water under defined conditions of pressure and time (up to 1 m of submersion).</td>
                    <td>
                        Test duration: 30 minutes - ref IEC 60529, table 8.
                        <br />Tested with the lowest point of the enclosure 1000 mm below the surface of the water, or the highest point 150 mm below the surface, whichever is deeper.
                    </td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Immersion, 1 m or more depth</td>
                    <td>The equipment is suitable for continuous immersion in water under conditions which shall be specified by the manufacturer. However, with certain types of equipment, it can mean that water can enter but only in such a manner that it produces no harmful effects. The test depth and/or duration is expected to be greater than the requirements for IPx7, and other environmental effects may be added, such as temperature cycling before immersion.</td>
                    <td>Test duration: Agreement with Manufacturer<br />Depth specified by manufacturer, generally up to 3 m</td>
                </tr>
                <tr>
                    <td>9K</td>
                    <td>Powerful high temperature water jets</td>
                    <td>Protected against close-range high pressure, high temperature spray downs.<br />Smaller specimens rotate slowly on a turntable, from 4 specific angles. Larger specimens are mounted upright, no turntable required, and are tested freehand for at least 3 minutes at distance of 0.15–0.2 m.<br />There are specific requirements for the nozzle used for the testing.<br />This test is identified as IPx9 in IEC 60529.</td>
                    <td>Test duration: 30 seconds in each of 4 angles (2 minutes total)<br />Water volume: 14–16 litres per minute<br />Pressure: 8–10 MPa (80–100 bar) at distance of 0.10–0.15 m<br />Water temperature: 80 °C</td>
                </tr>
            </tbody>
        </table>
        <br />
        <p>(All tests with the letter "K" are defined by ISO 20653 (replacing DIN 40050-9) and are not found in IEC 60529, except for IPx9 which is the same as the IP69K water test.)</p>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="5">Additional letters</th>
                </tr>
                <tr>
                    <th style="text-align:center;">Letter</th>
                    <th style="text-align:center;">Meaning</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>IP</td>
                    <td>Single numerical (0-6)</td>
                </tr>
                <tr>
                    <td>f</td>
                    <td>Oil resistant</td>
                </tr>
                <tr>
                    <td>H</td>
                    <td>High voltage device</td>
                </tr>
                <tr>
                    <td>M</td>
                    <td>Device moving during water test</td>
                </tr>
                <tr>
                    <td>S</td>
                    <td>Device standing still during water test</td>
                </tr>
                <tr>
                    <td>W</td>
                    <td>Weather conditions</td>
                </tr>
            </tbody>
        </table>
        <br />
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="5">Mechanical impact resistance</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Dropped from the 3rd edition of IEC 60529 onwards</td>
                </tr>
            </tbody>
        </table>
        <br />
{{</rawhtml>}}