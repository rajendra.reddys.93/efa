﻿---
title: Logic Gates
weight: 10
notoc: true
summary: Basic logic gates with symbol and truth-tables.
---

{{<rawhtml>}}
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">Type</th>
                    <th style="text-align:center;">Symbol/Shape</th>
                    <th style="text-align:center;">Boolean algebra</th>
                    <th style="text-align:center;">Truth table</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>AND</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-AND.png"></td>
                    <td>A.B or A&amp;B</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A AND B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>OR</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-OR.png"></td>
                    <td>A+B</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A OR B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>NOT</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-NOT.png"></td>
                    <td>~A</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">NOT A</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>NAND</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-NAND.png"></td>
                    <td>&not;(A.B) or A&uarr;B</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUTS</th>
                                    <th style="text-align:center;">OUTPUTS</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A NAND B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>NOR</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-NOR.png"></td>
                    <td>&not;(A+B) or A-B</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A NOR B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>XOR</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-XOR.png"></td>
                    <td>A&oplus;B</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A XOR B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>XNOR</td>
                    <td><img class="img-100px" src="/images/Resources/LogicGates/logic-gate-XNOR.png"></td>
                    <td>&not;(A&oplus;B)</td>
                    <td>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center;" colspan="2">INPUT</th>
                                    <th style="text-align:center;">OUTPUT</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">A</th>
                                    <th style="text-align:center;">B</th>
                                    <th style="text-align:center;">A XNOR B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>1</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
{{</rawhtml>}}