﻿---
title: SMD Passive Packages
weight: 4
notoc: true
summary: SMD package code with sizes for passive components. 
---

{{<img src="/images/Resources/ResisterPackages/resistor-sizes.png" alt="smd-footprint" cap="SMD Footprint">}}
        {{<rawhtml>}}
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="2">Type</th>
                    <th style="text-align:center;" colspan="2">Length (l)</th>
                    <th style="text-align:center;" colspan="2">Width (w)</th>
                    <th style="text-align:center;" colspan="2">Height (h)</th>
                    <th style="text-align:center;" colspan="2">Power</th>
                </tr>
                <tr>
                    <th style="text-align:center;">Imperical</th>
                    <th style="text-align:center;">Metric</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                    <th style="text-align:center;">Watt</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0201</td>
                    <td>0603</td>
                    <td>0.024</td>
                    <td>0.6</td>
                    <td>0.012</td>
                    <td>0.3</td>
                    <td>0.01</td>
                    <td>0.25</td>
                    <td>1/20 (0.05)</td>
                </tr>
                <tr>
                    <td>0402</td>
                    <td>1005</td>
                    <td>0.04</td>
                    <td>1.0</td>
                    <td>0.02</td>
                    <td>0.5</td>
                    <td>0.014</td>
                    <td>0.35</td>
                    <td>1/16 (0.062)</td>
                </tr>
                <tr>
                    <td>0603</td>
                    <td>1608</td>
                    <td>0.06</td>
                    <td>1.55</td>
                    <td>0.03</td>
                    <td>0.85</td>
                    <td>0.018</td>
                    <td>0.45</td>
                    <td>1/10 (0.10)</td>
                </tr>
                <tr>
                    <td>0805</td>
                    <td>2012</td>
                    <td>0.08</td>
                    <td>2.0</td>
                    <td>0.05</td>
                    <td>1.2</td>
                    <td>0.018</td>
                    <td>0.45</td>
                    <td>1/8 (0.125)</td>
                </tr>
                <tr>
                    <td>1206</td>
                    <td>3216</td>
                    <td>0.12</td>
                    <td>3.2</td>
                    <td>0.06</td>
                    <td>1.6</td>
                    <td>0.022</td>
                    <td>0.55</td>
                    <td>1/4 (0.25)</td>
                </tr>
                <tr>
                    <td>1210</td>
                    <td>3225</td>
                    <td>0.12</td>
                    <td>3.2</td>
                    <td>0.10</td>
                    <td>2.5</td>
                    <td>0.022</td>
                    <td>0.55</td>
                    <td>1/2 (0.50)</td>
                </tr>
                <tr>
                    <td>1218</td>
                    <td>3246</td>
                    <td>0.12</td>
                    <td>3.2</td>
                    <td>0.18</td>
                    <td>4.6</td>
                    <td>0.022</td>
                    <td>0.55</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>2010</td>
                    <td>5025</td>
                    <td>0.20</td>
                    <td>5.0</td>
                    <td>0.10</td>
                    <td>2.5</td>
                    <td>0.024</td>
                    <td>0.6</td>
                    <td>3/4 (0.75)</td>
                </tr>
                <tr>
                    <td>2512</td>
                    <td>6332</td>
                    <td>0.25</td>
                    <td>6.3</td>
                    <td>0.12</td>
                    <td>3.2</td>
                    <td>0.024</td>
                    <td>0.6</td>
                    <td>1</td>
                </tr>
            </tbody>
        </table>
        </div>
        {{</rawhtml>}}
        {{<img src="/images/Resources/ResisterPackages/smd-land-pattern.png" alt="smd-land-pattern" cap="SMD Land Pattern">}}
        {{<rawhtml>}}
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;" colspan="2">Type</th>
                    <th style="text-align:center;" colspan="2">Pad Length (a)</th>
                    <th style="text-align:center;" colspan="2">Pad Width (b)</th>
                    <th style="text-align:center;" colspan="2">Gap (c)</th>
                </tr>
                <tr>
                    <th style="text-align:center;">Imperical</th>
                    <th style="text-align:center;">Metric</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                    <th style="text-align:center;">inch</th>
                    <th style="text-align:center;">mm</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0201</td>
                    <td>0603</td>
                    <td>0.012</td>
                    <td>0.3</td>
                    <td>0.012</td>
                    <td>0.3</td>
                    <td>0.012</td>
                    <td>0.3</td>
                </tr>
                <tr>
                    <td>0402</td>
                    <td>1005</td>
                    <td>0.24</td>
                    <td>0.6</td>
                    <td>0.20</td>
                    <td>0.5</td>
                    <td>0.020</td>
                    <td>0.5</td>
                </tr>
                <tr>
                    <td>0603</td>
                    <td>1608</td>
                    <td>0.035</td>
                    <td>0.9</td>
                    <td>0.024</td>
                    <td>0.6</td>
                    <td>0.035</td>
                    <td>0.9</td>
                </tr>
                <tr>
                    <td>0805</td>
                    <td>2012</td>
                    <td>0.051</td>
                    <td>1.3</td>
                    <td>0.028</td>
                    <td>0.7</td>
                    <td>0.047</td>
                    <td>1.2</td>
                </tr>
                <tr>
                    <td>1206</td>
                    <td>3216</td>
                    <td>0.063</td>
                    <td>1.6</td>
                    <td>0.035</td>
                    <td>0.9</td>
                    <td>0.079</td>
                    <td>2.0</td>
                </tr>
                <tr>
                    <td>1210</td>
                    <td>3225</td>
                    <td>0.063</td>
                    <td>1.6</td>
                    <td>0.035</td>
                    <td>0.9</td>
                    <td>0.079</td>
                    <td>2.0</td>
                </tr>
                <tr>
                    <td>1218</td>
                    <td>3246</td>
                    <td>0.19</td>
                    <td>4.8</td>
                    <td>0.035</td>
                    <td>0.9</td>
                    <td>0.079</td>
                    <td>2.0</td>
                </tr>
                <tr>
                    <td>2010</td>
                    <td>5025</td>
                    <td>0.11</td>
                    <td>2.8</td>
                    <td>0.059</td>
                    <td>0.9</td>
                    <td>0.15</td>
                    <td>3.8</td>
                </tr>
                <tr>
                    <td>2512</td>
                    <td>6332</td>
                    <td>0.14</td>
                    <td>3.5</td>
                    <td>0.063</td>
                    <td>1.6</td>
                    <td>0.15</td>
                    <td>3.8</td>
                </tr>
            </tbody>
        </table>
        </div>
{{</rawhtml>}}
