﻿---
title: Measuring Units
weight: 2
notoc: true
summary: List of Measuring units of different parameters with unit symbol.
---

{{<table cap="Measuring Units">}}
| Measuring Unit        | Unit Symbol | Electrical Parameter                                        |
| --------------------- | ----------- | ----------------------------------------------------------- |
| Volt                  | V           | Voltage(V, E)Electromotive Force(E)Potential Difference(Δφ) |
| Amphere               | A           | Electric Current(I)                                         |
| Ohm                   | Ω           | Resistance(R)                                               |
| Watt                  | W           | Electric Power                                              |
| Decible-milliwatt     | dBm         | Electric Power(P)                                           |
| Decible-Watt          | dBW         | Electric Power(P)                                           |
| Volt-Amphere-Reactive | var         | Reactive Power(Q)                                           |
| Volt-Amphere          | VA          | Apparent Power(S)                                           |
| Farad                 | F           | Capacitance(C)                                              |
| Henry                 | H           | Inductance(L)                                               |
| Siemens / Mho         | S           | Conductance(G)Admittance                                    |
| Coulomb               | C           | Electric Charge(Q)                                          |
| Amphere-Hour          | Ah          | Electric Charge(L)                                          |
| Joule                 | J           | Energy(E)                                                   |
| Kilo-Watt Hour        | kWh         | Energy(E)                                                   |
| Electron Volt         | eV          | Energy(E)                                                   |
| Ohm-meter             | Ωm          | Resistivity(ρ)                                              |
| Siemens per Meter     | S/m         | Conductivity(σ)                                             |
| Volts per Meter       | V/m         | Electric Field(E)                                           |
| Newtons per Coulomb   | N/C         | Electric Field(E)                                           |
| Volt-Meter            | Vm          | Electric Flux(Φe)                                           |
| Tesla                 | T           | Magnetic Field(B)                                           |
| Gauss                 | G           | Magnetic Field(B)                                           |
| Webber                | Wb          | Magnetic Flux(Φm)                                           |
| Hertz                 | Hz          | Frequency(f)                                                |
| Second                | s           | Time(t)                                                     |
| Meter                 | m           | Length(l)                                                   |
| Square-meter          | m2          | Area(A)                                                     |
| Decible               | dB          |                                                             |
| Part per Million      | ppm         |                                                             |
{{</table>}}
