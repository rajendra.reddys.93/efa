﻿---
title: Constants
weight: 3
notoc: true
summary: Different constants with symbol, value and unit.
---

{{<table cap="Contants">}}
| Name                     | Symbol | Value             | Unit    |
| ------------------------ | ------ | ----------------- | ------- |
| Electron Volt            | eV     | 1.60217653x10-19  | J       |
| Elementary Charge        | e      | 1.60217653x10-19  | C       |
| Speed of Light in Vacuum | c      | 299,792,458       | m/s     |
| Gravitational Constant   | G      | 6.67384x10-11     | Nm2/kg2 |
| Planck's Constant        | h      | 4.135667516x10-15 | eVs     |
| Planck's Constant        | h      | 6.62606957x10-34  | Js      |
{{</table>}}