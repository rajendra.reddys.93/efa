---
title: "Linux"
date: 2021-04-29
weight: 21
summary: Linux OS Concepts
categories: ["Linux", "OS", "Software"]
slug: linux
draft: true
type: Software
menu:
    main:
        parent: "Programming"
---

Linux OS Concepts
