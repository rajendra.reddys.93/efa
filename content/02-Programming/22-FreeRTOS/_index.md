---
title: "FreeRTOS"
date: 2021-04-29
weight: 22
summary: FreeRTOS Concepts
categories: ["FreeRTOS", "OS", "Software"]
slug: freertos
draft: true
type: Software
menu:
    main:
        parent: "Programming"
---

FreeRTOS Concepts
