---
title: "Design Single Linked List"
date: 2019-12-19T20:05:48+05:30
weight: 1
summary: Design a single linked list
link: https://leetcode.com/explore/learn/card/linked-list/209/singly-linked-list/1290/
difficulty: medium
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/209/singly-linked-list/1290/](https://leetcode.com/explore/learn/card/linked-list/209/singly-linked-list/1290/)
>
> **Difficulty**: Medium

## Solution 1: Single Linked List

```c
typedef struct {
    int data;
    struct MyLinkedList *next;
} MyLinkedList;

/** Initialize your data structure here. */
MyLinkedList* createNode(int val){
    MyLinkedList *node = (MyLinkedList *)malloc(sizeof(MyLinkedList));
    node->data = val;
    node->next = NULL;
    return node;
}

void printList(MyLinkedList *head) {
    MyLinkedList *travNode = head;
    while (travNode)
    {
        printf("%d ", travNode->data);
        travNode = travNode->next;
    }
    printf("\n");
}

MyLinkedList* myLinkedListCreate() {
    MyLinkedList *head = createNode(0);
    return head;
}

/** Get the value of the index-th node in the linked list.
If the index is invalid, return -1. */
int myLinkedListGet(MyLinkedList* obj, int index) {
    MyLinkedList *travNode = obj->next;

    if (travNode == NULL)
        return -1;
    
    while (travNode->next && index--)
        travNode = travNode->next;
    if (index > 0)
        return -1;
    
    return travNode->data;
}

/** Add a node of value val before the first element of the linked list. 
After the insertion, the new node will be the first node of the linked list. */
void myLinkedListAddAtHead(MyLinkedList* obj, int val) {
    MyLinkedList *node = createNode(val);

    node->next = obj->next;
    obj->next = node;
    //printList(obj->next);
}

/** Append a node of value val to the last element of the linked list. */
void myLinkedListAddAtTail(MyLinkedList* obj, int val) {
    MyLinkedList *node = createNode(val);
    MyLinkedList *travNode = obj->next;

    if (travNode == NULL)
        return myLinkedListAddAtHead(obj, val);
    
    while (travNode->next)
        travNode = travNode->next;
    
    travNode->next = node;
    //printList(obj->next);
}

/** Add a node of value val before the index-th node in the linked list. 
If index equals to the length of linked list, the node will be appended 
to the end of linked list. If index is greater than the length, the node 
will not be inserted. */
void myLinkedListAddAtIndex(MyLinkedList* obj, int index, int val) {
    MyLinkedList *node = createNode(val);
    MyLinkedList *travNode = obj->next;

    if (travNode == NULL || index == 0)
        return myLinkedListAddAtHead(obj, val);
    
    while (travNode->next && --index)
        travNode = travNode->next;
    if (index > 1)
        return;
    
    node->next = travNode->next;
    travNode->next = node;
    //printList(obj->next);
}

/** Delete the index-th node in the linked list, if the index is valid. */
void myLinkedListDeleteAtIndex(MyLinkedList* obj, int index) {
    MyLinkedList *travNode = obj->next;
    MyLinkedList *tempNode;
    MyLinkedList *prevNode;

    if (travNode == NULL)
        return;
    
    if (index == 0)
    {
        tempNode = travNode;
        obj->next = travNode->next;
        free(tempNode);
        //printList(obj->next);
        return;
    }
    
    while (travNode->next && index--)
    {
        prevNode = travNode;
        travNode = travNode->next;
    }
    
    if (index > 0)
        return;
    
    tempNode = travNode;
    prevNode->next = travNode->next;
    free(tempNode);
    //printList(obj->next);
}

void myLinkedListFree(MyLinkedList* obj) {
    MyLinkedList *travNode = obj->next;
    MyLinkedList *tempNode;
    if (!travNode)
        return;
    while (travNode->next)
    {
        tempNode = travNode;
        travNode = travNode->next;
        free(tempNode);
    }
    obj->next = NULL;
    //printList(obj->next);
}

/**
 * Your MyLinkedList struct will be instantiated and called as such:
 * MyLinkedList* obj = myLinkedListCreate();
 * int param_1 = myLinkedListGet(obj, index);
 
 * myLinkedListAddAtHead(obj, val);
 
 * myLinkedListAddAtTail(obj, val);
 
 * myLinkedListAddAtIndex(obj, index, val);
 
 * myLinkedListDeleteAtIndex(obj, index);
 
 * myLinkedListFree(obj);
*/
```
