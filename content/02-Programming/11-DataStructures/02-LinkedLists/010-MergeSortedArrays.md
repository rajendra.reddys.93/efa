---
title: "Merge Sorted Arrays"
date: 2019-12-19T20:05:48+05:30
weight: 10
summary: Merge two sorted arrays
link: https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1227/
difficulty: medium
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1227/](https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1227/)
>
> **Difficulty**: Medium

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2)
{
    struct ListNode *resHead, *resHeadTrav;
    resHead = NULL;
    resHeadTrav = NULL;

    // corner cases
    if (l1 == NULL && l2 == NULL)
    {
        return NULL;
    }
    else if (l1 == NULL)
    {
        return l2;
    }
    else if (l2 == NULL)
    {
        return l1;
    }
    
    // base of list
    if (l1->val <= l2->val)
    {
        resHead = l1;
        l1 = l1->next;
    }
    else
    {
        resHead = l2;
        l2 = l2->next;
    }
    
    resHeadTrav = resHead;
    
    while (l1 && l2)
    {
        if (l1->val < l2->val)
        {
            resHeadTrav->next = l1;
            resHeadTrav = l1;
            l1 = l1->next;
        }
        else
        {
            resHeadTrav->next = l2;
            resHeadTrav = l2;
            l2 = l2->next;
        }
    }
    
    if (!l1 && l2)
        resHeadTrav->next = l2;
    else if (!l2 && l1)
        resHeadTrav->next = l1;
    
    return resHead;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
