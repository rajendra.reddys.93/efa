---
title: "Adding Two Numbers"
date: 2019-12-19T20:05:48+05:30
weight: 11
summary: Add two numbers represented in linked list
link: https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1228/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1228/](https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1228/)
>
> **Difficulty**: Easy

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

void insertNode(struct ListNode **tail, struct ListNode *node) {
    if (*tail)
        (*tail)->next = node;        
    
    *tail = node;
}

struct ListNode* createNode(int val) {
    struct ListNode *node = (struct ListNode *)malloc(sizeof(struct ListNode));
    node->next = NULL;
    node->val = val;
    return node;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    
    /* base case */
    if (!l1 && !l2)
        return NULL;
    
    if (!l1)
        return l2;
    
    if (!l2)
        return l1;
    
    int sum = 0;
    int carry = 0;
    int num1 = 0;
    int num2 = 0;
    struct ListNode *resTail = NULL;
    struct ListNode *resHead = NULL;
    
    while (l1 || l2) {
        if (l1) {
            num1 = l1->val;
            l1 = l1->next;
        }
        else {
            num1 = 0;
        }
    
        if (l2) {
            num2 = l2->val;
            l2 = l2->next;
        }
        else {
            num2 = 0;
        }
        
        sum = carry + num1 + num2;
        if (sum > 9) {
            carry = sum / 10;
            sum %= 10;
        } else {
            carry = 0;
        }
        
        struct ListNode *node = createNode(sum);
        insertNode(&resTail, node);
        if (resHead == NULL)
            resHead = node;

    }
    if (carry) {
        struct ListNode *node = createNode(carry);
        insertNode(&resTail, node);
    }
    return resHead;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(n).
