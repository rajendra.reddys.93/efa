---
title: "Intersection of 2 Lists"
date: 2019-12-19T20:05:48+05:30
weight: 3
summary: Find Intersection point of 2 lists if any
link: https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/](https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/)
>
> **Difficulty**: Easy

## Solution 1: Traverse and compare

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
    if (!headA || !headB)
        return NULL;
    
    struct ListNode *travA = headA;
    struct ListNode *travB = headB;
    while (travA)
    {        
        while (travB)
        {
            if (travA == travB)
                return travA;
            
            travB = travB->next;
        }
        travA = travA->next;
        travB = headB;
    }
    return NULL;
}
```

> **Time Complexity:** O(m*n), where m & n are lengths of lists.
>
> **Auxiliary Space:** O(1).

## Solution 2: Size difference and Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
    if (!headA || !headB)
        return NULL;
    
    struct ListNode *travA = headA;
    struct ListNode *travB = headB;
    int aSize = 0, bSize = 0;
    
    /* find size of A */
    while (travA->next) {
        travA = travA->next;
        aSize++;
    }
    
    /* find size of B */
    while (travB->next) {
        travB = travB->next;
        bSize++;
    }
    
    /* find size difference and reset to heads */
    int sizeDiff = aSize - bSize;
    travA = headA;
    travB = headB;
    
    /* based on size difference position travNodes to common idex from intersection */
    if (sizeDiff > 0) {
        while (travA->next && sizeDiff) {
            travA = travA->next;
            sizeDiff--;
        }
    } else if (sizeDiff < 0) {
        while (travB->next && sizeDiff) {
            travB = travB->next;
            sizeDiff++;
        }
    }
    
    while (travA != travB) { // && travA->next && travB->next) {
        travA = travA->next;
        travB = travB->next;
    }
    
    if (travA == travB)
        return travA;
    
    return NULL;
}
```

> **Time Complexity:** O(n). O((m+n+k) - precise, where m & n are lengths of lists, k is position of intersection point form longest list.
>
> **Auxiliary Space:** O(1).

## Solution 3: Using stack

> 1. Push all elements of both lists on to stack.
> 2. Pop and compare as long as they are equal.
> 3. When they differ return previous node.
