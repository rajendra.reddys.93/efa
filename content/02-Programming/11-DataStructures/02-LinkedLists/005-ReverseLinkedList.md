---
title: "Reverse linked list"
date: 2019-12-19T20:05:48+05:30
weight: 5
summary: Reverse Linked list
link: https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1205/
difficulty: medium
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1205/](https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1205/)
>
> **Difficulty**: Medium

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

// 1->2->3->4->5
struct ListNode* reverseList(struct ListNode* head)
{
    if (!head)
        return head;
    
    if (!head->next)
        return head;
    
    struct ListNode* prev, *cur, *next = head;
    if (!head->next->next)
    {
        prev = head;
        cur = head->next;
        prev->next = NULL;
        cur->next = prev;
        head = cur;
        return cur;
    }
    
    prev = head;
    cur = head->next;
    prev->next = NULL;
    next = cur->next;
    while (next->next)
    {
        cur->next = prev;
        prev = cur;
        cur = next;
        next = next->next;
    }
    
    cur->next = prev;
    next->next = cur;
    head = next;
    
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
