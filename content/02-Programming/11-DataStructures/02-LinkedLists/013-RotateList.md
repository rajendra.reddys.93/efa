---
title: "Rotate List"
date: 2019-12-19T20:05:48+05:30
weight: 13
summary: Rotate given list in-place by k places
link: https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1295/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1295/](https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1295/)
>
> **Difficulty**: Easy

## Solution 1: Rearrange pointers

Rearrange pointers at kth, head and tail nodes

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

/* helper */
void printList(struct ListNode *head) {
    while (head) {
        printf ("%d ", head->val);
        head = head->next;
    }
    printf("\n");
}

struct ListNode* rotateRight(struct ListNode* head, int k) {
    
    /* base case */
    if (!head || k == 0)
        return head;

    int len = 0;
    struct ListNode *travNode = head;
    struct ListNode *tempNode = NULL;
    
    /* find length */
    while (travNode) {
        len++;
        travNode = travNode->next;
    }
    
    /* count k to traverse */
    k = k % len;
    
    if (k == 0)
        return head;
    
    k = len - k;
    travNode = head;
    
    /* traverse till k */
    while (--k) {
        travNode = travNode->next;
    }
    
    /* make k as end of list */
    tempNode = travNode->next;
    travNode->next = NULL;
    
    /* make end of list point to head */
    /* make k+1 as new head */
    travNode = tempNode;
    while (travNode) {
        if (!travNode->next) {
            travNode->next = head;
            head = tempNode;
            break;
        }
        travNode = travNode->next;
    }
    
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
