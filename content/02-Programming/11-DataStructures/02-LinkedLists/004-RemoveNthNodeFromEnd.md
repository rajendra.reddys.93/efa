---
title: "Remove Nth node form end"
date: 2019-12-19T20:05:48+05:30
weight: 4
summary: Given head of the linked list remove nth node from end
link: https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1296/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1296/](https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1296/)
>
> **Difficulty**: Easy

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 1->2->3->4->5, n = 4
1->2->3->4->5, n = 1
1->2->3->4->5, n = 5
1->2, n = 1
1, n = 1
[]
 */
struct ListNode* removeNthFromEnd(struct ListNode* head, int n)
{
    if (n == 1 && !head->next)
    {
        free(head);
        return NULL;
    }
    
    struct ListNode* travNode = head;
    struct ListNode* nthNode = head;
    int listLength = 1;
    while(travNode->next)
    {
        if (listLength > n)
            nthNode = nthNode->next;
        travNode = travNode->next;
        listLength++;
    }
    
    if (nthNode == head && listLength <= n)
    {
        struct ListNode* tempNode = head;
        head = head->next;
        free(tempNode);
    }
    else
    {
        struct ListNode* tempNode = nthNode->next;
        nthNode->next = tempNode->next;
        free(tempNode);
    }
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
