---
title: "Linked List Cycle"
date: 2019-12-19T20:05:48+05:30
weight: 2
summary: Find if any cycle present in Linked list
link: https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/](https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1212/)
>
> **Difficulty**: Easy

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
bool hasCycle(struct ListNode *head) {
    if (head == NULL || head->next == NULL)
        return false;
    
    struct ListNode *slowNode = head;
    struct ListNode *fastNode = head->next;
    while (slowNode != fastNode)
    {
        if (fastNode == NULL || fastNode->next == NULL)
            return false;
        
        slowNode = slowNode->next;
        fastNode = fastNode->next->next;
    }
    return true;
}
```

> **Time Complexity:** O(n). O(n+k) - precise, where k is length of cycle
>
> **Auxiliary Space:** O(1).

## Solution 2: Data overwrite (not recommended)

```c
bool hasCycle(struct ListNode *head) {
    while (head)
    {
        if(head -> val == NULL)
        {
            return true;
        }
        head -> val = NULL;
        head = head -> next;
    }
    return false;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
