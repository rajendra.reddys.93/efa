---
title: "Palindrome List"
date: 2019-12-19T20:05:48+05:30
weight: 7
summary: find if given list is a palindrome
link: https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1209/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1209/](https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1209/)
>
> **Difficulty**: Easy

## Solution 1: Reverse till mid and compare

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


bool isPalindrome(struct ListNode* head) {
    
    /* base case - single element */
    if (!head->next)
        return true;
    
    struct ListNode *tempNode;
    struct ListNode *prevNode = NULL;
    struct ListNode *crntNode = head;
    struct ListNode *fastNode = head;

    /* reverse 1st half of linked list */
    while (fastNode) {
        /* logic to traverse till mid */
        if (fastNode->next)
            fastNode = fastNode->next->next;
        else
            break;
        
        /* logic to reverse 1st half */
        tempNode = prevNode;
        prevNode = crntNode;
        crntNode = crntNode->next;
        prevNode->next = tempNode;
    }
    
    if (fastNode)
        crntNode = crntNode->next;

    while (prevNode && crntNode) {
        if (prevNode->val != crntNode->val) {
            return false;
        }
        
        prevNode = prevNode->next;
        crntNode = crntNode->next;
    }
    
    return true;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
