---
title: "Copy List With Random Pointer"
date: 2019-12-19T20:05:48+05:30
weight: 12
summary: Copy list with random pointer to NULL or other element
link: https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1229/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1229/](https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1229/)
>
> **Difficulty**: Easy

## Solution 1: Two loops

```c
/**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *next;
 *     struct Node *random;
 * };
 */

struct Node* createNode(int val) {
    struct Node *node = malloc(sizeof(struct Node));
    node->val = val;
    node->next = NULL;
    node->random = NULL;
    return node;
}

void insertTail(struct Node **tail, struct Node *node) {
    if (*tail)
        (*tail)->next = node;
    
    *tail = node;
}

/* helper */
void printList(struct Node *head) {
    while (head) {
        printf ("%d ", head->val);
        head = head->next;
    }
    printf("\n");
}

struct Node* copyRandomList(struct Node* head) {
    /* base case */
    if (!head)
        return NULL;
    
    struct Node *travNode = head;
    struct Node *travNode1 = head;
    struct Node *resHead = NULL;
    struct Node *resTail = NULL;
    struct Node *resTravNode = NULL;
    
    /* create new list with NULL random pointer */
    while (travNode) {
        struct Node *newNode = createNode(travNode->val);
        insertTail(&resTail, newNode);
        if (resHead == NULL)
            resHead = newNode;
        
        travNode = travNode->next;
    }
    
    /* for each random pointer, traverse and make random pointer */
    travNode = head;
    resTravNode = resHead;
    while (travNode && resTravNode) {
        if (travNode->random) {
            travNode1 = head;
            int index = 0;
            while (travNode1) {
                if (travNode->random == travNode1)
                    break;
                index++;
                travNode1 = travNode1->next;
            }
            travNode1 = resHead;
            while (index--) {
                travNode1 = travNode1->next;
            }
            resTravNode->random = travNode1;
        }
        travNode = travNode->next;
        resTravNode = resTravNode->next;
    }
    
    return resHead;
}
```

> **Time Complexity:** O(n^2).
>
> **Auxiliary Space:** O(n).

## Solution 2: Optimized version (modifies input)

```c
/**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *next;
 *     struct Node *random;
 * };
 */

struct Node* createNode(int val) {
    struct Node *node = malloc(sizeof(struct Node));
    node->val = val;
    node->next = NULL;
    node->random = NULL;
    return node;
}

void insertTail(struct Node **tail, struct Node *node) {
    if (*tail)
        (*tail)->next = node;
    
    *tail = node;
}

struct Node* copyRandomList(struct Node* head) {
    if (!head)
        return NULL;
    
    struct Node *travNode = head;
    struct Node *resHead = NULL;
    struct Node *resTail = NULL;
    
    /* create new list with modified random pointer */
    while (travNode) {
        struct Node *newNode = createNode(travNode->val);
        newNode->random = travNode->random;
        travNode->random = newNode;
        insertTail(&resTail, newNode);
        if (resHead == NULL)
            resHead = newNode;
        
        travNode = travNode->next;
    }
    
    /* rectify the random pointer */
    travNode = resHead;
    while (travNode) {
        if (travNode->random)
            travNode->random = travNode->random->random;
        
        travNode = travNode->next;
    }
    
    return resHead;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(n).
