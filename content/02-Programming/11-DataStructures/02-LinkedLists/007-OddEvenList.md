---
title: "Odd Even List"
date: 2019-12-19T20:05:48+05:30
weight: 7
summary: Rearrange List keeping odd & even elements together
link: https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/](https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/)
>
> **Difficulty**: Easy

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* oddEvenList(struct ListNode* head) {
    
    /* base case - empty list | single element | only 2 elements */
    if (!head || !head->next || !head->next->next)
        return head;
    
    struct ListNode *evenHead = head->next;
    struct ListNode *evenTravNode = head->next;
    struct ListNode *oddTravNode = head;
    
    while (oddTravNode->next && evenTravNode->next) {
        oddTravNode->next = evenTravNode->next;
        oddTravNode = evenTravNode->next;
        evenTravNode->next = oddTravNode->next;
        evenTravNode = oddTravNode->next;
    }
    oddTravNode->next = evenHead;
    
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
