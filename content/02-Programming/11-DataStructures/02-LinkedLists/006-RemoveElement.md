---
title: "Remove Elements"
date: 2019-12-19T20:05:48+05:30
weight: 6
summary: Remove all occurances of the element
link: https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1207/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1207/](https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1207/)
>
> **Difficulty**: Easy

## Solution 1: Two pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* removeElements(struct ListNode* head, int val) {
    
    struct ListNode *travNode;
    struct ListNode *tempNode;
    struct ListNode *prevNode;
    
    /* base case - empty list */
    if (!head)
        return head;
    
    /* base case - single element and matches val */
    if ( (head->next == NULL) && (val == head->val) ) {
        free(head);
        head = NULL;
        return head;
    }
    
    /* general case */
    travNode = head;
    prevNode = travNode;
    while (travNode) {
        if ( (travNode->val == val) && (travNode == head) ) {
            head = travNode->next;
            tempNode = travNode;
            travNode = travNode->next;
            prevNode = travNode;
            free(tempNode);
        } else if (travNode->val == val) {
            tempNode = travNode;
            prevNode->next = travNode->next;
            travNode = travNode->next;
            free(tempNode);
        } else {
            prevNode = travNode;
            travNode = travNode->next;
        }
    }
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## Solution 2: Single pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* removeElements(struct ListNode* head, int val) {
    
    struct ListNode *travNode;
    struct ListNode *tempNode;
    
    /* base case - empty list */
    if (!head)
        return head;
    
    /* general case */
    travNode = head;
    while (travNode->next) {
        if ( (travNode->val == val) && (travNode == head) ) {
            head = travNode->next;
            tempNode = travNode;
            travNode = travNode->next;
            free(tempNode);
        } else if (travNode->next->val == val) {
            tempNode = travNode->next;
            travNode->next = travNode->next->next;
            free(tempNode);
        } else {
            travNode = travNode->next;
        }
    }
    
    if ( (head->next == NULL) && (val == head->val) ) {
        free(head);
        head = NULL;
        return head;
    }
    
    return head;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
