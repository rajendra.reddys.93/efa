---
title: "Queue Using Stack"
date: 2021-04-11T11:55:52+05:30
weight: 6
summary: Implement Queue using Stack
link: https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1386/
difficulty: Medium
categories: ["Stack", "Queue", "Leetcode"]
slug: queue-using-stack
type: coding
---

Implement a first in first out (FIFO) queue using only two stacks. The implemented queue should support all the functions of a normal queue (`push`, `peek`, `pop`, and `empty`).

**Implement the MyQueue class:**

`void push(int x)` Pushes element x to the back of the queue.

`int pop()` Removes the element from the front of the queue and returns it.

`int peek()` Returns the element at the front of the queue.

`boolean empty()` Returns true if the queue is empty, false otherwise.

### Notes

- You must use only standard operations of a stack, which means only push to top, peek/pop from top, size, and is empty operations are valid.
- Depending on your language, the stack may not be supported natively. You may simulate a stack using a list or deque (double-ended queue) as long as you use only a stack's standard operations.

### Follow-up

Can you implement the queue such that each operation is amortized O(1) time complexity? In other words, performing n operations will take overall O(n) time even if one of those operations may take longer.

### Example 1

```c
Input
["MyQueue", "push", "push", "peek", "pop", "empty"]
[[], [1], [2], [], [], []]
Output
[null, null, null, 1, 1, false]

Explanation
MyQueue myQueue = new MyQueue();
myQueue.push(1); // queue is: [1]
myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
myQueue.peek(); // return 1
myQueue.pop(); // return 1, queue is [2]
myQueue.empty(); // return false
```

### Constraints

> 1 <= x <= 9
>
> At most 100 calls will be made to push, pop, peek, and empty.
>
> All the calls to pop and peek are valid.

## Solution 1: Costly DeQueue

```c
typedef struct {
    int top;
    int size;
    int *data;
} Stack;

Stack* createStack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = -1;
    stack->size = 1;
    stack->data = malloc(sizeof(int) * stack->size);
    return stack;
}

void push(Stack* obj, int val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(int) * obj->size);
    }
    obj->data[++obj->top] = val;
}

void pop(Stack* obj) {
    obj->top--;
}

int top(Stack* obj) {
    if (obj->top >= 0)
        return obj->data[obj->top];
    return -1;
}

void freeStack(Stack* obj) {
    free(obj->data);
    free(obj);
}

typedef struct {
    Stack *s1, *s2;
    //Stack *s2;
} MyQueue;

/** Initialize your data structure here. */

MyQueue* myQueueCreate() {
    MyQueue* queue = malloc(sizeof(MyQueue));
    queue->s1 = createStack();
    queue->s2 = createStack();
    return queue;
}

/** Push element x to the back of queue. */
void myQueuePush(MyQueue* obj, int x) {
    push(obj->s1, x);
}

int myQueueTop(MyQueue* obj, bool remove) {
    int val = 0;
    int res = 0;
    while ( (val = top(obj->s1)) != -1) {
        push(obj->s2, val);
        pop(obj->s1);
    }
    res = top(obj->s2);
    if (remove)
        pop(obj->s2);
    while ( (val = top(obj->s2)) != -1) {
        push(obj->s1, val);
        pop(obj->s2);
    }
    return res;
}
/** Removes the element from in front of queue and returns that element. */
int myQueuePop(MyQueue* obj) {
    return myQueueTop(obj, true);
}

/** Get the front element. */
int myQueuePeek(MyQueue* obj) {
    return myQueueTop(obj, false);
}

/** Returns whether the queue is empty. */
bool myQueueEmpty(MyQueue* obj) {
    return ((top(obj->s1) == -1) ? true : false);
}

void myQueueFree(MyQueue* obj) {
    freeStack(obj->s1);
    freeStack(obj->s2);
    free(obj);
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);
 
 * int param_2 = myQueuePop(obj);
 
 * int param_3 = myQueuePeek(obj);
 
 * bool param_4 = myQueueEmpty(obj);
 
 * myQueueFree(obj);
*/
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(n).

## Solution 2: Costly EnQueue

```c
typedef struct {
    int top;
    int size;
    int *data;
} Stack;

Stack* createStack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = -1;
    stack->size = 1;
    stack->data = malloc(sizeof(int) * stack->size);
    return stack;
}

void push(Stack* obj, int val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(int) * obj->size);
    }
    obj->data[++obj->top] = val;
}

void pop(Stack* obj) {
    obj->top--;
}

int top(Stack* obj) {
    if (obj->top >= 0)
        return obj->data[obj->top];
    return -1;
}

void freeStack(Stack* obj) {
    free(obj->data);
    free(obj);
}

typedef struct {
    Stack *s1, *s2;
    //Stack *s2;
} MyQueue;

/** Initialize your data structure here. */

MyQueue* myQueueCreate() {
    MyQueue* queue = malloc(sizeof(MyQueue));
    queue->s1 = createStack();
    queue->s2 = createStack();
    return queue;
}

/** Push element x to the back of queue. */
void myQueuePush(MyQueue* obj, int x) {
    int val = 0;
    while ( (val = top(obj->s1)) != -1) {
        push(obj->s2, val);
        pop(obj->s1);
    }
    push(obj->s2, x);
    while ( (val = top(obj->s2)) != -1) {
        push(obj->s1, val);
        pop(obj->s2);
    }
}

int myQueueTop(MyQueue* obj, bool remove) {
    int res = 0;
    res = top(obj->s1);
    if (remove)
        pop(obj->s1);
    return res;
}
/** Removes the element from in front of queue and returns that element. */
int myQueuePop(MyQueue* obj) {
    return myQueueTop(obj, true);
}

/** Get the front element. */
int myQueuePeek(MyQueue* obj) {
    return myQueueTop(obj, false);
}

/** Returns whether the queue is empty. */
bool myQueueEmpty(MyQueue* obj) {
    return ((top(obj->s1) == -1) ? true : false);
}

void myQueueFree(MyQueue* obj) {
    freeStack(obj->s1);
    freeStack(obj->s2);
    free(obj);
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);
 
 * int param_2 = myQueuePop(obj);
 
 * int param_3 = myQueuePeek(obj);
 
 * bool param_4 = myQueueEmpty(obj);
 
 * myQueueFree(obj);
*/
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(n).
