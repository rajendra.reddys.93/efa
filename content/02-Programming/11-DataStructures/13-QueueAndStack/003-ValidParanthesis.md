---
title: "Valid Paranthesis"
date: 2019-12-19T20:05:48+05:30
weight: 3
summary: Check if the string have valid paranthesis pair
link: https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1361/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1361/](https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1361/)
>
> **Difficulty**: Easy

## Solution 1: Using Stack

```c
typedef struct {
    int top;
    int size;
    char *data;
} Stack;

Stack* createStack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = -1;
    stack->size = 10;
    stack->data = malloc(sizeof(char) * stack->size);
    return stack;
}

void push(Stack* obj, char val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(char) * obj->size);
    }
    obj->data[++obj->top] = val;
}

void pop(Stack* obj) {
    obj->top--;
}

char top(Stack* obj) {
    if (obj->top >= 0)
        return obj->data[obj->top];
    return -1;
}

void freeStack(Stack* obj) {
    free(obj->data);
    free(obj);
}

bool isValid(char * s){
    bool isValid = true;
    Stack *stack = createStack();
    
    while (*s && isValid) {
        if (*s == '{' || *s == '(' || *s == '[') {
            push(stack, *s);
        } else {
            if ( (*s == ')' && top(stack) == '(') ||
                (*s == '}' && top(stack) == '{') ||
                (*s == ']' && top(stack) == '[') )
                pop(stack);
            else
                isValid = false;
        }
        s++;
    }
    
    /* fewer closing brackets */
    if (top(stack) != -1)
        isValid = false;
    
    freeStack(stack);
    
    return isValid;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
