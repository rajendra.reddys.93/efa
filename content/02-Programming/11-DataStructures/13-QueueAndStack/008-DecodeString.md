---
title: "Decode String"
date: 2021-04-11T23:21:25+05:30
draft: true
weight: 8
summary: Given an encoded string, return its decoded string
link: https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1379/
difficulty: Easy
categories: ["Arrays", "Queue", "Leetcode"]
slug: decode-string
type: coding
---

Given an encoded string, return its decoded string.

The encoding rule is: `k[encoded_string]`, where the `encoded_string` inside the square brackets is being repeated exactly `k` times. Note that `k` is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, `k`. For example, there won't be input like `3a` or `2[4]`.

### Example 1

```c
Input: s = "3[a]2[bc]"
Output: "aaabcbc"
```

### Example 2

```c
Input: s = "3[a2[c]]"
Output: "accaccacc"
```

### Example 3

```c
Input: s = "2[abc]3[cd]ef"
Output: "abcabccdcdcdef"
```

### Example 4

```c
Input: s = "abc3[cd]xyz"
Output: "abccdcdcdxyz"
```

### Constraints

> `1 <= s.length <= 30`
>
> `s` consists of lowercase English letters, digits, and square brackets `'[]'`.
>
> `s` is guaranteed to be a valid input.
>
> All the integers in `s` are in the range `[1, 300]`.

## Solution 1: Heading

```c
```

> **Time Complexity:** O(nlogn).
>
> **Auxiliary Space:** O(1).
