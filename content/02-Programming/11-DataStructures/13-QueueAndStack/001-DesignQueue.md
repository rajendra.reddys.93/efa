---
title: "Design a Queue"
date: 2019-12-19T20:05:48+05:30
weight: 1
summary: Design a Queue
link: https://leetcode.com/explore/learn/card/queue-stack/228/first-in-first-out-data-structure/1337/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/queue-stack/228/first-in-first-out-data-structure/1337/](https://leetcode.com/explore/learn/card/queue-stack/228/first-in-first-out-data-structure/1337/)
>
> **Difficulty**: Easy

## Solution 1: Queue

```c
typedef struct {
    int front;
    int rear;
    int size;
    int *data;
} MyCircularQueue;

bool myCircularQueueIsFull(MyCircularQueue* obj);
bool myCircularQueueIsEmpty(MyCircularQueue* obj);

/* helper */
void printQ(MyCircularQueue* obj) {
    for (int i = 0; i < obj->size; i++)
        printf("%d ", obj->data[i]);
    printf("\n");
}

MyCircularQueue* myCircularQueueCreate(int k) {
    if (k == 0)
        return NULL;
    
    MyCircularQueue *queue = malloc(sizeof(MyCircularQueue));
    queue->data = (int *)malloc(k * sizeof(int));
    queue->size = k;
    queue->front = -1;
    queue->rear = -1;
    return queue;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    if (myCircularQueueIsFull(obj))
        return false;
    
    /* first time use or empty queue */
    if (myCircularQueueIsEmpty(obj)) obj->front = 0;
    
    /* append data to rear */
    obj->rear = (obj->rear + 1) % obj->size;
    obj->data[obj->rear] = value;
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return false;
    
    /* take element from front */
    if (obj->front == obj->rear) {
        obj->front = -1;
        obj->rear = -1;
    } else {
        obj->front = (obj->front + 1) % obj->size;
    }
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->data[obj->front];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    if (myCircularQueueIsEmpty(obj))
        return -1;
    return obj->data[obj->rear];
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    if (obj->front == -1)
        return true;
    return false;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    if (obj->front == ((obj->rear + 1) % obj->size))
        return true;
    return false;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    while (obj->front != -1)
        myCircularQueueDeQueue(obj);
}

/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);
 
 * bool param_2 = myCircularQueueDeQueue(obj);
 
 * int param_3 = myCircularQueueFront(obj);
 
 * int param_4 = myCircularQueueRear(obj);
 
 * bool param_5 = myCircularQueueIsEmpty(obj);
 
 * bool param_6 = myCircularQueueIsFull(obj);
 
 * myCircularQueueFree(obj);
*/
```
