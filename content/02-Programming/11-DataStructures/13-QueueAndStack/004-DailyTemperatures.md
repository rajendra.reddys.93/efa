---
title: "Daily Temperatures"
date: 2020-04-09
weight: 4
summary: Days for next warmer day
link: https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1363
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1363](https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1363)
>
> **Difficulty**: Easy

## Solution 1: Using Loops

```c
int* dailyTemperatures(int* T, int TSize, int* returnSize) {
    *returnSize = TSize;
    int *resArr = malloc(TSize * sizeof(int));
    for (int i = 0; i < TSize; i++) {
        int j = i;
        for (; j < TSize; j++) {
            if (T[j] > T[i])
                break;
        }
        if (j == TSize)
            resArr[i] = 0;
        else
            resArr[i] = j - i;
    }
    return resArr;
}
```

> **Time Complexity:** O(n^2).
>
> **Auxiliary Space:** O(1).

## Solution 2: Using Stack

```c
```
