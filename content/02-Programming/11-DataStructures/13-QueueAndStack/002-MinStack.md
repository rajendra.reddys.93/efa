---
title: "Design a Stack"
date: 2019-12-19T20:05:48+05:30
weight: 2
summary: Design a Stack with min value
link: https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1360/
difficulty: easy
---

> **Link**: [https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1360/](https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1360/)
>
> **Difficulty**: Easy

## Solution 1: Using Linked list

```c

typedef struct {
    int val;
    struct stackNode *next;
} stackNode;

typedef struct {
    struct stackNode *top;
} MinStack;

void printStack(stackNode *head) {
    while (head) {
        printf("%d ", head->val);
        head = head->next;
    }
    printf("\n");
}

/** initialize your data structure here. */

MinStack* minStackCreate() {
    MinStack *stack = malloc(sizeof(MinStack));
    stack->top = NULL;
    return stack;
}

void minStackPush(MinStack* obj, int val) {
    stackNode *node = malloc(sizeof(stackNode));
    node->val = val;
    node->next = obj->top;
    obj->top = node;
    //printStack(obj->top);
}

void minStackPop(MinStack* obj) {
    if (obj->top) {
        stackNode *temp = obj->top;
        obj->top = temp->next;
        free(temp);
        //printStack(obj->top);
    }
}

int minStackTop(MinStack* obj) {
    if (obj->top) {
        stackNode *temp = obj->top;
        return temp->val;
    }
    return -1;
}

int minStackGetMin(MinStack* obj) {
    if (obj->top) {
        stackNode *travNode = obj->top;
        int min = travNode->val;
        while (travNode) {
            if (travNode->val < min)
                min = travNode->val;
            travNode = travNode->next;
        }
        return min;
    }
    return -1;
}

void minStackFree(MinStack* obj) {
    stackNode *travNode = obj->top;
    stackNode *tempNode;
    while (travNode) {
        tempNode = travNode;
        travNode = travNode->next;
        free(tempNode);
    }
    obj->top = NULL;
}

/**
 * Your MinStack struct will be instantiated and called as such:
 * MinStack* obj = minStackCreate();
 * minStackPush(obj, val);
 
 * minStackPop(obj);
 
 * int param_3 = minStackTop(obj);
 
 * int param_4 = minStackGetMin(obj);
 
 * minStackFree(obj);
*/
```

## Solution 2: Using dynamic array

```c
typedef struct {
    int min;
    int top;
    int size;
    int *data;
} MinStack;

void printStack(int *head, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", head[i]);
    }
    printf("\n");
}

/** initialize your data structure here. */
MinStack* minStackCreate() {
    MinStack *stack = malloc(sizeof(MinStack));
    stack->top = -1;
    stack->size = 10;
    stack->data = malloc(sizeof(int) * stack->size);
    stack->min = INT_MAX;
    return stack;
}

void minStackPush(MinStack* obj, int val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(int) * obj->size);
    }
    obj->data[++obj->top] = val;
    if (obj->min > val)
        obj->min = val;
    //printStack(obj->top);
}

void minStackPop(MinStack* obj) {
    if (obj->min == obj->data[obj->top]) {
        obj->min = obj->data[0];
        if (obj->top == 0)
            obj->min = INT_MAX;
        for (int i = 0; i < obj->top; i++) {
            if (obj->min > obj->data[i])
                obj->min = obj->data[i];
        }
    }
    obj->top--;
}

int minStackTop(MinStack* obj) {
    return obj->data[obj->top];
}

int minStackGetMin(MinStack* obj) {
    return obj->min;
}

void minStackFree(MinStack* obj) {
    free(obj->data);
    free(obj);
}

/**
 * Your MinStack struct will be instantiated and called as such:
 * MinStack* obj = minStackCreate();
 * minStackPush(obj, val);
 
 * minStackPop(obj);
 
 * int param_3 = minStackTop(obj);
 
 * int param_4 = minStackGetMin(obj);
 
 * minStackFree(obj);
*/
```
