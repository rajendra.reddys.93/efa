---
title: "Evaluate RPN"
date: 2021-04-11T11:34:54+05:30
weight: 5
summary: Evaluare Reverse Polish Notation
link: https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1394/
difficulty: Easy
categories: ["Stack", "Leetcode"]
slug: evaluate-rpn
type: coding
---

Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, and /. Each operand may be an integer or another expression.

Note that division between two integers should truncate toward zero.

It is guaranteed that the given RPN expression is always valid. That means the expression would always evaluate to a result, and there will not be any division by zero operation.

### Example 1

```c
Input: tokens = ["2","1","+","3","*"]
Output: 9
Explanation: ((2 + 1) * 3) = 9
```

### Example 2

```c
Input: tokens = ["4","13","5","/","+"]
Output: 6
Explanation: (4 + (13 / 5)) = 6
```

### Example 3

```c
Input: tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
Output: 22
Explanation: ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
= ((10 * (6 / (12 * -11))) + 17) + 5
= ((10 * (6 / -132)) + 17) + 5
= ((10 * 0) + 17) + 5
= (0 + 17) + 5
= 17 + 5
= 22
```

### Constraints

> 1 <= tokens.length <= 104
>
> tokens[i] is either an operator: "+", "-", "*", or "/", or an integer in the range [-200, 200].

## Solution 1: Stack

```c

typedef struct {
    int top;
    int size;
    int *data;
} Stack;

Stack* createStack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = -1;
    stack->size = 10;
    stack->data = malloc(sizeof(int) * stack->size);
    return stack;
}

void push(Stack* obj, int val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(int) * obj->size);
    }
    obj->data[++obj->top] = val;
}

void pop(Stack* obj) {
    obj->top--;
}

int top(Stack* obj) {
    if (obj->top >= 0)
        return obj->data[obj->top];
    return -1;
}

void freeStack(Stack* obj) {
    free(obj->data);
    free(obj);
}

int evalRPN(char ** tokens, int tokensSize){
    Stack *stack = createStack();
    int i = 0;
    int val1 = 0, val2 = 0, res = 0;
    while (i < tokensSize) {
        if ( (strcmp(tokens[i], "+") == 0) || (strcmp(tokens[i], "-") == 0) ||
            (strcmp(tokens[i], "*") == 0) || (strcmp(tokens[i], "/") == 0) ) {
            /* do math */
            val2 = top(stack);
            pop(stack);
            val1 = top(stack);
            pop(stack);
            switch (tokens[i][0]) {
                case '+': res = val1 + val2; break;
                case '-': res = val1 - val2; break;
                case '*': res = val1 * val2; break;
                case '/': res = val1 / val2; break;
            }
            push(stack, res);
        } else {
            val1 = atoi(tokens[i]);
            push(stack, val1);
        }
        i++;
    }
    res = top(stack);
    free(stack);
    return res;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(m), m no of values in input.

## Solution 2: Optimized

```c

typedef struct {
    int top;
    int size;
    int *data;
} Stack;

Stack* createStack() {
    Stack *stack = malloc(sizeof(Stack));
    stack->top = -1;
    stack->size = 10;
    stack->data = malloc(sizeof(int) * stack->size);
    return stack;
}

void push(Stack* obj, int val) {
    if (obj->top + 1 == obj->size) {
        obj->size *= 2;
        obj->data = realloc(obj->data, sizeof(int) * obj->size);
    }
    obj->data[++obj->top] = val;
}

void pop(Stack* obj) {
    obj->top--;
}

int top(Stack* obj) {
    if (obj->top >= 0)
        return obj->data[obj->top];
    return -1;
}

void freeStack(Stack* obj) {
    free(obj->data);
    free(obj);
}

int evalRPN(char ** tokens, int tokensSize){
    Stack *stack = createStack();
    int i = 0;
    int val1 = 0, res = 0;
    while (i < tokensSize) {
        if ( (strcmp(tokens[i], "+") == 0) || (strcmp(tokens[i], "-") == 0) ||
            (strcmp(tokens[i], "*") == 0) || (strcmp(tokens[i], "/") == 0) ) {
            pop(stack);
            val1 = top(stack);
            pop(stack);
            switch (tokens[i][0]) {
                case '+': res = val1 + res; break;
                case '-': res = val1 - res; break;
                case '*': res = val1 * res; break;
                case '/': res = val1 / res; break;
            }
            push(stack, res);
        } else {
            res = atoi(tokens[i]);
            push(stack, res);
        }
        i++;
    }
    free(stack);
    return res;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(m), m no of values in input.
