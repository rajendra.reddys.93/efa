---
title: "Remove Duplicates"
date: 2019-12-19T20:05:48+05:30
weight: 17
summary: Remove all duplicates in a sorted array
link: https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3248/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3248/](https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3248/)
>
> **Difficulty:** Easy

## Solution 1: Travers and keep unique

```c
int removeDuplicates(int* nums, int numsSize)
{
    int i = 0, j = 0;
    for (i = 0; i < numsSize - 1; i++)
    {
        if (nums[i] == nums[i + 1])
            continue;
        nums[j++] = nums[i];
    }
    if (i == numsSize - 1)
        nums[j++] = nums[i];
    return j;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

## Soluytion 2: Two Pointers

```c
int removeDuplicates(int* nums, int numsSize)
{
    if (numsSize == 0) return 0;
    
    int i = 0, j;
    for (j = 1; j < numsSize; j++)
    {
        if (nums[j] != nums[i])
        {
            nums[++i] = nums[j];
        }
    }
    return i + 1;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

C++

```c++
int removeDuplicates(vector<int>& nums) {
    auto const numsSize = nums.size();
    auto resultIndex = std::size_t{};
    for (auto index = std::size_t{}; index < numsSize; index++) {
        if (nums[resultIndex] < nums[index]) {
            resultIndex++;
            nums[resultIndex] = nums[index];
        }
    }
    return resultIndex + 1;        
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).
