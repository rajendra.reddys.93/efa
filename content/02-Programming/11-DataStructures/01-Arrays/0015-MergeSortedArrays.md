---
title: "Merge Sorted Arrays"
date: 2019-12-19T20:05:48+05:30
weight: 15
summary: Merge 2 sorted arrays
link: https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3253/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3253/](https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3253/)
>
> **Difficulty:** Easy

## Solution 1: Merge element by element

```c
/*
[1,2,3,0,0,0], 3
[2,5,6], 3
[1,2,2,3,5,6], 6
[3,6] - [1,2,3,0,0,6]
[3,5] - [1,2,3,0,5,6]
[3,2] - [1,2,3,3,5,6]
[2,2] - [1,2,2,3,5,6]
[1,2] - [1,2,2,3,5,6]
[1,x] - [1,2,2,3,5,6]
*/

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n)
{
    int resIndex = nums1Size - 1;
    int i = m - 1;
    int j = n - 1;
    while ( i >= 0 && j >= 0 )
    {
        if (nums1[i] > nums2[j])
        {
            nums1[resIndex--] = nums1[i];
            i--;
        }
        else
        {
            nums1[resIndex--] = nums2[j];
            j--;
        }
    }
    while(j >= 0)
    {
        nums1[resIndex--] = nums2[j];
        j--;
    }
}
```

> **Time Complexity:** O(n). O(m+n) - Precise, where m - length of nums1, n - length of nums2.
>
> **Auxiliary Space:** O(3).

## Solution 2: Merge & Sort

```c
int cmp(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n)
{
    int i = 0;
    int j = 0;
    while ( i < nums1Size && j < n )
    {
        nums1[i + m] = nums2[j++];
        i++;
    }
    qsort(nums1, nums1Size, sizeof(int), cmp);
}
```

> **Time Complexity:** O(nlogn). O((m+n)log(m+n) + n) - Precise, where m - length of nums1, n - length of nums2.
>
> **Auxiliary Space:** O(2).
