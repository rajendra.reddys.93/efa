---
title: "Replace with Right Greatest"
date: 2019-12-19T20:05:48+05:30
weight: 20
summary: Replace Elements with Greatest Element on Right Side
link: https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3259/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3259/](https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3259/)
>
> **Difficulty:** Easy

## Solution 1: Traversal

```c
int* replaceElements(int* arr, int arrSize, int* returnSize)
{
    *returnSize = arrSize;
    
    if (arrSize == 1)
    {
        arr[0] = -1;
        return arr; // base case
    }
    
    for (int i = 0; i < arrSize-1; i++)
    {
        int maxIndex = i+1;
        for (int j = i+2; j < arrSize; j++)
        {
            if (arr[j] > arr[maxIndex])
                maxIndex = j;
        }
        arr[i] = arr[maxIndex];
    }
    arr[arrSize-1] = -1;
    return arr;
}
```

> **Time Complexity:** O(nlogn).
>
> **Auxiliary Space:** O(3).

## Solution 2: Reverse Traversal

```c
//[17,17,5,17,6,1]
//[18,6,6,6,1,-1]

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* replaceElements(int* arr, int arrSize, int* returnSize)
{
    *returnSize = arrSize;
    
    int maxVal = arr[arrSize-1];
    for (int i = arrSize-2; i >= 0; i--)
    {
        if (arr[i] > maxVal)
        {
            int temp = arr[i];
            arr[i] = maxVal;
            maxVal = temp;
        }
        else
        {
            arr[i] = maxVal;
        }
    }
    
    arr[arrSize-1] = -1;
    return arr;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(3).


```c++
vector<int> replaceElements(vector<int>& arr) {
    auto const arrSize = arr.size();
    auto maxValue = arr[arrSize - 1];
    // auto, std::size_t or uint for index is not working, why?
    for (int index = (arrSize - 2); index >= 0; index--) {
        if (arr[index] > maxValue) {
            auto const temp = arr[index];
            arr[index] = maxValue;
            maxValue = temp;
        } else {
            arr[index] = maxValue;
        }
    }
    arr[arrSize - 1] = -1;
    return arr;
}
```
