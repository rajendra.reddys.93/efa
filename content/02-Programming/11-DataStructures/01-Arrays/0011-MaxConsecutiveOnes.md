---
title: "Max Consecutive Ones"
date: 2019-12-19T20:05:48+0530
weight: 11
summary: Find max no of consecutive once in te given array
link: https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/
difficulty: easy
categories: ["Arrays", "Leetcode"]
---

> **Link**: [https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/](hhttps://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/)
>
> **Difficulty**: Easy

## Solution 1: Traversal

```c
int findMaxConsecutiveOnes(int* nums, int numsSize)
{
    int consecOnesCount = 0;
    int maxConsecOnesCount = 0;

    for (int i = 0; i < numsSize; i++)
    {
        if (nums[i] == 1)
        {
            consecOnesCount++;
        }
        else
        {
            if (consecOnesCount > maxConsecOnesCount)
                maxConsecOnesCount = consecOnesCount;
    
            consecOnesCount = 0;
        }
    }
    if (consecOnesCount > maxConsecOnesCount)
        maxConsecOnesCount = consecOnesCount;

    return maxConsecOnesCount;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## In C++
```cpp
class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
        auto consecOnes = 0;
        auto count = 0;
        for (auto i : nums) {
            if (i == 1) {
                count++;
            } else {
                if (count > consecOnes) {
                    consecOnes = count;
                }
                count = 0;
            }
        }
        return ((count > consecOnes) ? count : consecOnes);
    }
};
```

## Using Lambda
```cpp
class Solution {
public:
    
    int findMaxConsecutiveOnes(vector<int>& nums) {
        auto consecOnes = 0;
        auto count = 0;
        auto calc = [&](int v) {
            if (v == 1) {
                count++;
            } else {
                if (count > consecOnes) {
                    consecOnes = count;
                }
                count = 0;
            }
        };
        for_each(nums.begin(), nums.end(), calc);
        return ((count > consecOnes) ? count : consecOnes);
    }
};
```

## Using `std::max`
```cpp
class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
        
        int maxConsecutiveOnes = 0;
        int ConsecutiveCounter = 0;

        std::for_each(nums.begin(), nums.end(), [&](auto num) {
            if (num == 1) {
                ConsecutiveCounter++;
            } else {
                maxConsecutiveOnes = std::max(maxConsecutiveOnes, ConsecutiveCounter);
                ConsecutiveCounter = 0;
            }
        });
        
        return std::max(maxConsecutiveOnes, ConsecutiveCounter);
    }
};
```
