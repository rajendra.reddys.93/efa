---
title: "Remove Element"
date: 2019-12-19T20:05:48+05:30
weight: 16
summary: Remove all occurances of an element
link: https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3247/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3247/](https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3247/)
>
> **Difficulty:** Easy

## Solution 1: Place element from end

```c
int removeElement(int* nums, int numsSize, int val)
{
    int length = numsSize;
    for (int i = 0; i < length; i++)
    {
        if (nums[i] == val)
        {
            while (nums[length - 1] == val)
            {
                length--;
                if (length == i)
                    return length;
            }
            nums[i] = nums[length - 1];
            length--;
        }
    }
    return length;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

## Solution 2: Two Pointers

```c
int removeElement(int* nums, int numsSize, int val)
{
    int i = 0;
    for (int j = 0; j < numsSize; j++)
    {
        if (nums[j] == val)
        {
            continue;
        }
        nums[i++] = nums[j];
    }
    return i;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).
