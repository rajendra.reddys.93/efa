---
title: "Sort Array by Parity"
date: 2019-12-19T20:05:48+05:30
weight: 22
summary: Sort Array by Parity
link: https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/](https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/)
>
> **Difficulty:** Easy

## Solution 1: Two pointers

```c
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int* sortArrayByParity(int* A, int ASize, int* returnSize)
{
    *returnSize = ASize;
    int left = 0;
    int right = ASize - 1;
    
    while (left <= right)
    {
        if (A[left] % 2 == 0)
            left++;
        else if (A[right] % 2 != 0)
            right--;
        else
            swap(&A[left++], &A[right--]);
    }
    
    return A;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

C++
```c++
class Solution {
public:
    inline void swap(int *a, int *b) {
        int temp = *a;
        *a = *b;
        *b = temp;
    }
    
    inline bool isEven(int const value) {
        return value % 2 == 0;
    }
        
    vector<int> sortArrayByParity(vector<int>& nums) {
        // shinking pointers
        auto reverseIndex = nums.size() - 1;
        auto forwardIndex = std::size_t{};
        while (forwardIndex < reverseIndex) {
            if (isEven(nums[forwardIndex])) {
                forwardIndex++;
            } else if (!isEven(nums[reverseIndex])) {
                reverseIndex--;
            } else {
                swap(&nums[forwardIndex++], &nums[reverseIndex--]);
            }
        }
        return nums;
    }
};
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).
