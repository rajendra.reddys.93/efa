---
title: "Duplicate Zero"
date: 2019-12-19T20:05:48+05:30
weight: 14
summary: Add a extra 0 for every 0 in the array
link: https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3245/
difficulty: medium
---

> **Link**: [https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3245/](https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3245/)
>
> **Difficulty**: Medium

## Solution 1: Traversal

```c
void duplicateZeros(int* arr, int arrSize)
{
    for (int i = 0; i < arrSize; i++)
    {
        if (arr[i] == 0)
        {
            for (int j = arrSize - 2; j >= i; j--)
            {
                arr[j + 1] = arr[j];
            }
            i++;
        }
    }
}
```

> **Time Complexity:** O(n^2).
>
> **Auxiliary Space:** O(2).

## Solution 2: Reverse Traversal

```c
void duplicateZeros(int* arr, int arrSize)
{
    for (int i = arrSize - 1; i >= 0; i--)
    {
        if (arr[i] == 0)
        {
            for (int j = arrSize - 2; j >= i; j--)
            {
                arr[j + 1] = arr[j];
            }
        }
    }
}
```

> **Time Complexity:** O(n^2).
>
> **Auxiliary Space:** O(2).

## Solution 3: Count 0s

```c
void duplicateZeros(int *arr, int arrSize)
{
    int possibleDups = 0;
    int lastIndex = arrSize - 1;

    for (int left = 0; left <= lastIndex - possibleDups; left++)
    {
        // Count the zeros
        if (arr[left] == 0)
        {
            if (left == lastIndex - possibleDups)
            {
                arr[lastIndex] = 0;
                lastIndex -= 1;
                break;
            }
            possibleDups++;
        }
    }

    int last = lastIndex - possibleDups;

    // Copy zero twice, and non zero once.
    for (int i = last; i >= 0; i--)
    {
        if (arr[i] == 0)
        {
            arr[i + possibleDups] = 0;
            possibleDups--;
            arr[i + possibleDups] = 0;
        }
        else
        {
            arr[i + possibleDups] = arr[i];
        }
    }
}
```

> **Time Complexity:** O(n). O(2n) - Precise
>
> **Auxiliary Space:** O(5).
