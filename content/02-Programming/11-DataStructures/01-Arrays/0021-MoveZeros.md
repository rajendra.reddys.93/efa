---
title: "Move Zeros"
date: 2019-12-19T20:05:48+05:30
weight: 21
summary: Move zeros without altering the sequence of other numbers
link: https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3157/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3157/](https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3157/)
>
> **Difficulty:** Easy

## Solution 1: Two pointers

```c
void moveZeroes(int* nums, int numsSize)
{
    int i = 0;
    for (int j = 0; j < numsSize; j++)
    {
       if (nums[j] != 0)
           nums[i++] = nums[j];
    }
    for (int j = i; j < numsSize; j++)
        nums[j] = 0;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

C++
```c++
void moveZeroes(vector<int>& nums) {
    // sliding window
    auto const numsSize = nums.size();
    auto resultIndex = std::size_t{};
    for (auto index = std::size_t{}; index < numsSize; index++) {
        if (nums[index] != 0) {
            nums[resultIndex++] = nums[index];
        }
    }
    for (auto index = resultIndex; index < numsSize; index++) {
        nums[index] = 0;
    }
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).
