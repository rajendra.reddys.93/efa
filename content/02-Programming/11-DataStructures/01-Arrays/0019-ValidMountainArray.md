---
title: "Valid Mountain Array"
date: 2019-12-19T20:05:48+05:30
weight: 19
summary: Check if given array is valid mountain
link: https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3251/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3251/](https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3251/)
>
> **Difficulty:** Easy

## Solution 1: Find mid and continue descending

```c
bool validMountainArray(int* arr, int arrSize)
{ 
    if ( (arrSize < 3) || (arr[1] < arr[0]) ) return false;
    
    bool isMoveUp = true;
    for (int i = 0; i < arrSize - 1; i++)
    {
        if (arr[i+1] == arr[i]) return false;
        
        if (isMoveUp)
        {
            if (arr[i+1] < arr[i]) isMoveUp = false;
        }
        else if (arr[i+1] > arr[i])
        {
            return false;
        }
    }
    return (isMoveUp == true) ? false : true;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

## Solution 2: Move Up and Down

```c
bool validMountainArray(int* arr, int arrSize)
{ 
    int i = 0;
    
    while ( (i+1 < arrSize) && (arr[i] < arr[i+1]) )
        i++;
    
    if ( (i == 0) || (i == arrSize - 1) )
        return false;
    
    while ( (i+1 < arrSize) && (arr[i] > arr[i+1]) )
        i++;
    
    return (i == arrSize-1);
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
