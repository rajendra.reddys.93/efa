---
title: "Sorted Squares"
date: 2019-12-19T20:05:48+05:30
weight: 13
summary: Sorted Squares in non-decreasing order
link: https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/
difficulty: easy
---

**Link :** [https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/](https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/)

**Difficulty :** Easy

## Solution 1

```c
int cmp(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

int* sortedSquares(int* nums, int numsSize, int* returnSize)
{
    int *squareNums = (int *)malloc(numsSize * sizeof(int));
    for (int i = 0; i < numsSize; i++)
    {
        squareNums[i] = (nums[i] * nums[i]);
    }
    qsort(squareNums, numsSize, sizeof(int), cmp);
    *returnSize = numsSize;
    return squareNums;
}
```

> **Time Complexity :** O(n). O(2n) - Precise.
>
> **Auxiallry Space :** O(1)

## Solution 2

```c
int* sortedSquares(int* nums, int numsSize, int* returnSize)
{
    int *squareNums = (int *)malloc(numsSize * sizeof(int));
    int left = 0, right = numsSize - 1, i = numsSize - 1;
    while (left <= right)
    {
        int leftPow = (nums[left] * nums[left]);
        int rightPow = (nums[right] * nums[right]);
        if (leftPow < rightPow)
        {
            squareNums[i] = rightPow;
            right--;
        }
        else
        {
            squareNums[i] = leftPow;
            left++;
        }
        i--;
    }
    *returnSize = numsSize;
    return squareNums;
}
```

> **Time Complexity :** O(n).
>
> **Auxiallry Space :** O(5).

## Solution 3

```c
int* sortedSquares(int* nums, int numsSize, int* returnSize)
{
    int *squareNums = (int *)malloc(numsSize * sizeof(int));
    int left = 0, right = numsSize - 1, i = numsSize - 1;
    while (left <= right)
    {
        if (abs(nums[left]) < abs(nums[right]))
        {
            squareNums[i] = pow(nums[right], 2);
            right--;
        }
        else
        {
            squareNums[i] = pow(nums[left], 2);
            left++;
        }
        i--;
    }
    *returnSize = numsSize;
    return squareNums;
}
```

> **Time Complexity :** O(n).
>
> **Auxiallry Space :** O(3)
