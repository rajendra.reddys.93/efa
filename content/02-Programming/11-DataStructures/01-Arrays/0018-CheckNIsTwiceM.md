---
title: "Check for N is Twice M"
date: 2019-12-19T20:05:48+05:30
weight: 18
summary: Check If N and Its Double Exist
link: https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/](https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/)
>
> **Difficulty:** Easy

## Solution 1: Traverse and find the N == 2 * M

```c
bool checkIfExist(int* arr, int arrSize)
{
    for (int i = 0; i < arrSize; i++)
    {
        for (int j = 0; j < arrSize; j++)
        {
            if (i != j && arr[i] == 2 * arr[j])
                return true;
        }
    }
    return false;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

## Solution 2: Improvement on top of Solution 1

```c
bool checkIfExist(int* arr, int arrSize)
{
    for (int i = 0; i < arrSize; i++)
    {
        if (arr[i] % 2 == 0)
        {
            for (int j = 0; j < arrSize; j++)
            {
                if (i != j && arr[i] == 2 * arr[j])
                    return true;
            }
        }
    }
    return false;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).
