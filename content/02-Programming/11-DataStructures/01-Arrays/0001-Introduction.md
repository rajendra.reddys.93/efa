---
title: Introduction to Arrays
date: 2021-05-02T20:05:48+0530
weight: 1
summary: What is an array?
slug: introduction
categories: [Arrays]
chapter: Arrays
---

An array is a collection of same data types. The collection is stored in a block of contegious memory.

An element of an array can be easily accessed using it's index. The index is the relative memory location
from the starting element in the array.
