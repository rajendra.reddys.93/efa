---
title: "Count even digits numbers"
date: 2019-12-19T20:05:48+05:30
weight: 12
summary: Count even digits numbers count in an array
link: https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/
difficulty: easy
---

> **Link:** [https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/](https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/)
>
> **Difficulty:** Easy

## Solution 1: Counting Digits

```c
int countDigits(int num)
{
    int digitsCount = 0;
    while (num)
    {
        num = num / 10;
        digitsCount++;
    }
    return digitsCount;
}

int findNumbers(int* nums, int numsSize)
{
    int evenDigitsNumsCount = 0;
    for (int i = 0; i < numsSize; i++)
    {
        if ( !(countDigits(nums[i]) % 2) )
        {
            evenDigitsNumsCount++;
        }
    }
    return evenDigitsNumsCount;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(2).

### C++
```cpp
class Solution {
public:
    auto countDigits(int num) {
        auto digits = 0;
        while (num) {
            num /= 10;
            digits++;
        }
        return digits;
    }
    int findNumbers(vector<int>& nums) {
        auto countNums = 0;
        for (auto i : nums) {
            if (countDigits(i) % 2 == 0)
                countNums++;
        }
        return countNums;
    }
};
```

### Using Lambda
```cpp
class Solution {
public:
    constexpr static auto countDigits = [](int num) {
        auto digits = 0;
        while (num) {
            num /= 10;
            digits++;
        }
        return digits;
    };
    
    int findNumbers(vector<int>& nums) {
        auto countNums = 0;
        for (auto i : nums) {
            if (countDigits(i) % 2 == 0)
                countNums++;
        }
        return countNums;
    }
};
```

### Using Lambda with `for_each`
```cpp
class Solution {
public:
    int findNumbers(vector<int>& nums) {
        auto countNums = 0;
        static auto evenDigitsCheck = [&](int num) {
            auto digits = 0;
            while (num) {
                num /= 10;
                digits++;
            }
            if ((digits % 2) == 0)
                countNums++;
        };
        for_each(nums.begin(), nums.end(), evenDigitsCheck);
        return countNums;
    }
};
```

## Solution 2: Logrithamic

```c
int findNumbers(int* nums, int numsSize)
{
    int evenDigitsNumsCount = 0;
    for (int i = 0; i < numsSize; i++)
    {
        if ( (((int)log10(nums[i]) + 1) % 2) == 0)
        {
            evenDigitsNumsCount++;
        }
    }
    return evenDigitsNumsCount;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

### C++ - Using Lambda with `for_each`
```cpp
class Solution {
public:
    int findNumbers(vector<int>& nums) {
        auto countNums = 0;
        for_each(nums.begin(), nums.end(), [&](int num) {
            if ( ((static_cast<int>(log10(num)) + 1) % 2) == 0)
                countNums++;
        });
        return countNums;
    }
};
```
