---
title: std::array Implementation
date: 2021-05-02T20:05:48+0530
weight: 2
summary: What is an array?
slug: introduction
categories: [Arrays]
chapter: Arrays
---


```cpp
#include <cstdint>
#include <iterator>

template<typename T, std::size_t Size>
struct array {
    T data[Size];
    using reference = T&;
    using const_reference = const T&;
    using iterator = T*;
    using const_iterator = const T*;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using size_type = std::size_t;

    iterator begin() { return data; }
    iterator end() { return data + Size; }
    
    const_iterator begin() const { return data; }
    const_iterator end() const { return data + Size; }

    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }

    reverse_iterator rbegin() { return end(); }
    reverse_iterator rend() { return begin(); }

    const_reverse_iterator rbegin() const { return end(); }
    const_reverse_iterator rend() const { return begin(); }

    const_reverse_iterator crbegin() const { return end(); }
    const_reverse_iterator crend() const { return begin(); }

    size_type size() const { return Size; }
    size_type max_size() const { return Size; }
    bool empty() const { return Size == 0; }

    const_reference operator[](size_type index) const {
        if (index >= Size) throw;
        return data[index]; 
    }
    reference operator[](size_type index) {
        if (index >= Size) throw;
        return data[index];     
    }

    const_reference at(size_type index) const {
        if (index >= Size) throw;
        return data[index]; 
    }
    reference at(size_type index) {
        if (index >= Size) throw;
        return data[index];
    }
};

int main () {
    const static array<int, 10> data{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int sum = 0;
    for (auto const element : data) {
        sum += element;
    }

    return sum;
}
```
