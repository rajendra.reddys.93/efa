---
title: "Data Structures"
date: 2021-04-29
weight: 11
summary: Array, Linked List, Stack, Queue, etc...
categories: ["Data Structures", "Competetive", "Software"]
slug: data-structures
type: Software
menu:
    main:
        parent: "Programming"
---

Array, Linked List, Stack, Queue, etc...
