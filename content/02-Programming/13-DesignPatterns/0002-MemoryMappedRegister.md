---
title: Memory Mapped Register Access
date: 2021-12-3T20:10:10+05:30
weight: 2
slug: memory-mapped-register-access
chapter: IO
level: Advanced
---

We will explore different ways of setting and clearing different bits of control register of a micro-controller.

# Direct manipulation with pointer

```cpp
*((volatile unsigned int *)0x1234) = 0xAA;
while (*((volatile unsigned int *)0x5678) & 0x02);
```

Here we are directly writing certain values to the memory. It is hard to understand what is beeing done with these 2 statements. The above code is neither readable nor maintainable. This can be made more readable using macros.

# Using macro for address

```cpp
#define UART_TX     0x1234;
#define UART_CTRL   0x5678;

*((volatile unsigned int *)UART_TX) = 0xAA;
while (*((volatile unsigned int *)UART_CTRL) & 0x02);
```

```cpp
#define UART_TX     (volatile unsigned int *)0x1234;
#define UART_CTRL   (volatile unsigned int *)0x5678;

*UART_TX = 0xAA;
while(*UART_CTRL & 0x02);
```

In the above code, one could make out that the programmer is trying to access UART related register and cannot know if he changes values how it could behave.

# Using macros for bit positions

```cpp
#define UART_TX     (volatile unsigned int *)0x1234;
#define UART_CTRL   (volatile unsigned int *)0x5678;
#define UART_TX_IN_PROGRESS                0x1 << 1;

*UART_TX = 0xAA;
while (UART_CTRL & UART_TX_IN_PROGRESS);
```

# Define register type

```cpp
typedef (volatile unsigned int) reg;
#define UART_TX     (reg *)0x1234;
#define UART_CTRL   (reg *)0x5678;
#define UART_TX_IN_PROGRESS                0x1 << 1;

*UART_TX = 0xAA;
while (UART_CTRL & UART_TX_IN_PROGRESS);
```

# Using `union` & `struct` to define registers

```cpp
typedef (volatile unsigned int) reg;
#define UART_TX     (reg *)0x1234;
#define UART_CTRL   (reg *)0x5678;

typedef union uart_ctrl_t {
    unsigned int CTRL;
    struct bits {
        unsigned int READY_TO_SEND: 1;
        unsigned int TX_IN_PROGRESS: 1;
        unsigned int UNUSED: 6;
    }
} uart_ctrl;

typedef struct uart_device_t { 
    // TODO: check const qualifier position in below statements
    volatile uart_ctrl *const ctrl;
    reg *const tx;
    reg *const rx;
} uart_device;

uart_device serial;
serial.ctrl = UART_CTRL;
serial.tx = UART_TX;

*serial.tx = 0xAA;
while((*serial.ctrl)->TX_IN_PROGRESS);
```

Here it is easy to understand what is being done.

# doing it in C++ way

```cpp
typedef (volatile unsigned int) reg;
#define UART_TX     (reg *)0x1234;
#define UART_CTRL   (reg *)0x5678;

typedef union uart_ctrl_t {
    unsigned int CTRL;
    struct bits {
        unsigned int READY_TO_SEND: 1;
        unsigned int TX_IN_PROGRESS: 1;
        unsigned int UNUSED: 6;
    }
} uart_ctrl;

typedef struct uart_device_t { 
    // TODO: check const qualifier position in below statements
    volatile uart_ctrl *const ctrl;
    reg *const tx;
    reg *const rx;
} uart_device;

uart_device serial;
serial.ctrl = reinterpret_cast<uart_ctrl *>UART_CTRL;
serial.tx = reinterpret_cast<reg *>UART_TX;

*serial.tx = 0xAA;
while((*serial.ctrl)->TX_IN_PROGRESS);
```

# Using reference for address

```cpp
// TODO: Check if this works?
typedef (volatile unsigned int) reg;
#define UART_TX     (reg *)0x1234;
#define UART_CTRL   (reg *)0x5678;

typedef union uart_ctrl_t {
    unsigned int CTRL;
    struct bits {
        unsigned int READY_TO_SEND: 1;
        unsigned int TX_IN_PROGRESS: 1;
        unsigned int UNUSED: 6;
    }
} uart_ctrl;

typedef struct uart_device_t { 
    // TODO: check const qualifier position in below statements
    volatile uart_ctrl &const ctrl;
    reg &const tx;
    reg &const rx;
} uart_device;

uart_device serial;
serial.ctrl = reinterpret_cast<uart_ctrl *>UART_CTRL;
serial.tx = reinterpret_cast<reg *>UART_TX;

*serial.tx = 0xAA;
while((*serial.ctrl)->TX_IN_PROGRESS);
```

