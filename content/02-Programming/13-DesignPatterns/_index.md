---
title: "Design Patterns"
date: 2021-04-29
weight: 13
summary: Template Programming, State Machines
categories: ["Algorithms", "Meta Programming", "Software"]
slug: cpp-meta-programming
draft: true
type: Software
menu:
    main:
        parent: "Programming"
---

Big O Notation, Analysis & Methods
