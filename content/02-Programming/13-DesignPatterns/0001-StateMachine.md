---
title: State Machine Using boost.sml
date: 2021-11-26T20:10:10+05:30
weight: 1
slug: state-machine-using-boost-sml
chapter: SML
level: Advanced
---

```c++
#include <https://raw.githubusercontent.com/boost-ext/sml/master/include/boost/sml.hpp>
#include <iostream>
namespace sml = boost::sml;

namespace System {

    struct my_logger {
        template <class SM, class TEvent>
        void log_process_event(const TEvent&) {
            printf("[%s] \n", sml::aux::get_type_name<TEvent>());
        }

        template <class SM, class TGuard, class TEvent>
        void log_guard(const TGuard&, const TEvent&, bool result) {
            printf("%s \n", (result ? "[OK]" : "[Reject]"));
        }

        template <class SM, class TAction, class TEvent>
        void log_action(const TAction&, const TEvent&) {
            printf("%s \n", sml::aux::get_type_name<TAction>());
        }

        template <class SM, class TSrcState, class TDstState>
        void log_state_change(const TSrcState& src, const TDstState& dst) {
            printf("%s -> %s\n", src.c_str(), dst.c_str());
        }
    };

    // events
    struct event1 {};
    struct event2 {};
    struct event3 {};
    struct event4 {};

    // guards
    auto guard1 = []{ puts("guard1"); return true; };
    auto guard2 = []{ puts("guard2"); return true; };
    auto guard3 = []{ puts("guard3"); return false; };
    auto guard4 = []{ puts("guard4"); return true; };

    // actions
    auto action1 = []{ puts("action1"); };
    auto action2 = []{ puts("action2"); };
    auto action3 = []{ puts("action3"); };
    auto action4 = []{ puts("action4"); };

    struct System1 {
        auto operator()() const noexcept {
            using namespace sml;
            return make_transition_table(
                * "STATE1"_s / action3 = "STATE2"_s,
                "STATE2"_s + event<event3> = "STATE2"_s,
                "STATE2"_s + event<event1> [guard1 and guard2] / action1 = "STATE3"_s,
                "STATE3"_s + event<event2> / action2 = "STATE3"_s,
                "STATE3"_s + event<event4> [guard3] / action4 = X
            );
        }
    };

}

int main() {
    using namespace sml;
    using namespace System;
    my_logger logger;
    sml::sm<System1, sml::logger<my_logger>> sm{logger};
    //sm<System1> sm;
    sm.process_event(event2{});
    sm.process_event(event3{});
    sm.process_event(event1{});
    sm.process_event(event2{});
    sm.process_event(event4{});
    return 0;
}
```