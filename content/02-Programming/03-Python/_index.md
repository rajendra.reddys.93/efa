---
title: "Python"
date: 2021-04-29
weight: 3
summary: Python Programming Language
categories: ["Python", "Language", "Software"]
slug: python
draft: true
type: Software
menu:
    main:
        parent: "Programming"
---

Python Programming Language
