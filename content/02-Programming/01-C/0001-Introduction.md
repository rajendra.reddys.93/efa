---
title: Introduction to Programming
date: 2021-05-09T20:10:10+05:30
weight: 1
slug: introduction-to-programming
chapter: Introduction
level: Beginner
---

A computer program is a software or an application which is a set of instruction given to achieve a certain task. These instructions are then loaded into computer memory and executed sequentially. In todays world, a programmer typically writes code in a human readable programming language (**[C](/02-programming/01-c)**, **[C++](/02-programming/02-cpp)** etc) which then complied down to machine executable format (bin, exe etc).

Below are the different types of programming languages.

## Low-level language

These languages are hard to understand and platform dependent. There are 2 low-level language types.

### Machine language

Machine language is a low level programming language. Now a days no software engineer develops using machine language. However, the computer doesn't understand the code a programmer written in the C/C++ or any programming language for that matter. It only knows 0s and 1s.

For Example: `11001010 11100101`

For every instruction a machine can execute, there exist a unique sequency of 0s and 1s as above. These binary digits vary across different CPUs. So any instructions written for one CPU type are interpreted differently when executed on other CPU type.

### Assembly language

It is hard to memorize the machine language instructions for a normal human being. To make machine code easy to remember, assembly language was invented. In assembly language short abbreviations, numbers are used instead of binary sequence. These are called as `opcodes`.

For Example: `mov a, 0xFF`

Though assembly language is relatively easy to read and understand, it has limitations.

1. Even to achieve a simple task we need to input multiple instructions.
2. Computers doesn't understand the assembly language. With the help of assembler, the code must be converted to machine code before it can execute on computer.
3. As Machine language, the code written using assemble language is CPU type dependent. Code developed for a CPU type can't be executed on other CPU types.

The Machine language and assembly languages are called as low level languages as they are tightly tied to the platform they execute on. Below are the High level languages which can be run on any platform without modification.

In order to develop platform independent code, there is need for general purpose programming language. These languages are of classified as Mid level and High level languages.

## Mid level language

These languages have easy to read and understand. They take less lines of code to implement even complex task. Since these languages allow programmer to access the low level hardware they are more powerful in developing a robust applications. **[C](/02-programming/01-c)** & **[C++](/02-programming/02-cpp)** are two mid level languages.

Like assembly languages, code developed in mid level languages must be converted to machine code to execute it on a machine. This is achieved by using compiler.

A **compiler** is a software that converts the source code in to executable. Compilation is one time process. A source code compiled can run on the target machine any number of times. However a code compiled for a specific CPU type can't be executed on other CPU type.

{{<img alt="mid-level language" src="/images/c-programming/mid-level-language.png" cap="Mid-level Language" width="573" height="53">}}

> When any platform, hardware or compiler dependent code in included, code developed using mid level languages becomes less portable.

{{<img alt="compilation of mid-level language" src="/images/c-programming/compilation-mid-level-language.png" cap="Compilation of Mid-level Language" width="398" height="353">}}

## High level language

These are very top level languages, which are completely machine independent. A code once developed using these languages can be run on any machine provided the necessary environment is available. **[Python](/02-programming/03-python)** is the good example.

Like mid level languages, code developed in high level languages must be converted to machine code to run on a machine. This is achieved by using interpreter.

A **interpreter** is a software that executes the source code on target machine on the go without needing for compiled them to form single executable. The interpretation is required every time the code is running. The code can be executed only if the machine has required interpreter.

{{<img alt="high-level language" src="/images/c-programming/high-level-language.png" cap="high-level Language" width="553" height="53">}}
