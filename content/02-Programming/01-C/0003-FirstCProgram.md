---
title: First C Program
date: 2021-05-11T08:50:10+05:30
weight: 3
slug: first-c-program
chapter: Introduction
level: Beginner
---

As we know by now that C is a structured language, which means all programs written in C follows certain structure. Let us consider a basic C program as below to understand it better.

```c
#include <stdio.h>
int main(void)
{
    printf("Hello C!");
    return 0;
}
```

In the above code we have...

### Header file inclusion

A header file is with extension .h and it contains declaration of functions and macros. Such files must be included before using any functions in that file. In this case `printf()` is part of `stdio.h`. The header file inclusion done using `#include` directive. Example of other header files are

- `string.h` - string related functions.
- `math.h` - mathematical related functions.

We can even have our own/custom header files included. Which will be discussed in later chapter.

### `main()` function

A C program must have main function. A `main` function is where the execution of the starting point of the program. The `void` within `()` after `main` indicates that it doesn't accept any parameters. Different variation of `main()` are discussed in later chapter. The rest of the code is included inside the main().

### Statements within `{}`

Any application code will go inside `{}` after `main()`. Here we are printing `Hello C!` using `printf()` which is a builtin functionality of C.

### `return` statement

The C code will end with return statement. This ends the execution of code and returns the control to the execution platform. A program can skip return if `void main(void) {}` is used.
