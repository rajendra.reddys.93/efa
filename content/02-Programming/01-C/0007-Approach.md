---
title: Problem Solving Approach
date: 2021-05-13T14:32:06+05:30
weight: 7
slug: approach
mermaid: true
chapter: Introduction
expertise: Beginner
---

Let us discuss on how to solve a problem as a programmer. This approach is general and can be applied to a whole system, a part of a system, a small program or a single function.

{{<mermaid>}}
graph TB
    A(Iterate on problem statement .)
    B(Clarify what you understand .)
    C(Design a solution .)
    D(Run through example .)
    E(Implement code .)
    F(Test and debug .)
    G(Release .)
    A --> B --> C --> D --> E --> F --> G
    D -- Optimise. --> C
    F -- Rectify --> E
    G -- Improvise. --> C
{{</mermaid>}}

## Iterate over problem statement

Iterate over the given problem statement and understand the requirement, environment/platform, target audience. You need to know what problem does your program solve.

## Clarify what you understand

Ask questions and clarify on the problem statement. Get details about the constraints, different inputs and outputs, interfaces.

> **Example**: Clarify how input is given: possibilities - file, console, interrupt.

## Design a solution

Design a solution based on all inputs. Take out a paper and pen, write down your solution and explain it to yourself.

## Run through example

Take an example input and wake through the solution you designed. Observe what your solution does to your input. Does your solution generate the desired output?

## Optimise your solution

Optimise your solution based on the observations post running though the example. Optimize your design till all corner conditions are met.

## Implement code

Implement code as per the solution.

## Test and debug

Test your code and debug if you find any issues arise during testing.

## Rectify error

Rectify if you find any bugs in your code during testing.

## Improvise

Do improvise code to make it adaptable for other platforms/environments.
