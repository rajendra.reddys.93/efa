---
title: "C Programming"
date: 2021-04-29
weight: 1
categories: ["C", "Language", "Software"]
slug: c
type: book
menu:
    main:
        parent: "Programming"
---

C is a general purpose, structured, procedural & middle-level programming language used mainly in embedded software development.
