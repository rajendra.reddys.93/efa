---
title: Introduction to C
date: 2021-05-09T16:50:10+05:30
weight: 2
slug: introduction-to-c
chapter: Introduction
level: Beginner
---

C is a general purpose, structured, procedural & Middle level programming language used mainly in the development of embedded software, operating systems, compilers, interpreters database, device driver, real time applications and so on.

## History

C was first implemented by **Dennis Ritchie** in 1970s at Bell Labs. It was developed to overcome the difficulties found in other older languages such as BPCL, BASIC and B.

In 1983 a committee was formed to create ANSI (American National Standards Institute) to standardize the C language. A standard version (C89) was adopted by 1989 as per ANSI C standard. The C89 is used as base Later more revision were released with new features and improvements/fixes to existing implementation.

## Why C?

### C is Middle level language

C combines the control and flexibility of assembly language with features of high-level language. With C one can control the low-level hardware, manipulate memory directly, use pointer to access the addresses easily. The software written for one platform can be easily adapted to work on other platform.

C doesn't have any run-time error checking. It doesn't check for boundary of an array, size of a string. It is responsibility of the programmer to have these types of checks. C doesn't demand the strict type compatibility. A argument of one type can be passed to different type of parameter. This is not possible in few high-level languages.

With all these, C is best suitable for system-level programming.

### C is a Structured Language

The programming language exist prior to C were not structured. The structured language is highly readable and easy to understand. A instruction related to specific functionality can be grouped under a function or a block. C allow breaking down a complex code into multiple small blocks, with data being passed between these blocks. It supports several looping constructs such as for, while, do-while which didn't exist in earlier languages.

### Reliable, simple, and easy to use

C is one of oldest programming language and it is used in development of linux kernel, git and python interpreter. The deterministic usage of system resources and memory makes it more reliable. It uses only 32 keywords, structured by using blocks and functions which makes it easy to read and understand.

### Highly portable

It can be compiled on a variety of computer platforms. There are lot of compiler available to compile the C code to run on most of the platforms available. `gcc` & `clang` are most widely used.

### Programmer's language

It gives the flexibility to the programmer. Programmers can easily exploit the lower level system components and access the memory directly. The programmer must know what he is doing before he does. It works on a principle that **The programmer knows everything**.

### Efficient and fast

Since it allow direct access to lower level components and memory it is more efficient and fast. The rich library makes the development process easy with less ETA.

## Where is C used

1. C is most popular choice for embedded systems.
2. It is also used in developing Unix/Linux OS, Git, Python interpreter.
3. Used in development of device drivers, data base systems, compilers and assemblers, games.
4. It is more suitable for system programming.
5. Implementing data structures and algorithms.

## Limitations of C

1. C doesn't handle exceptions such as divide-by-zero, out-bound-access, etc...
2. No built-in garbage collector. Memory management is programmer's task.
3. Since it exploits the low level system components, it can open up for security vulnerabilities if not handled as intended.
