---
title: Left Rotate
date: 2019-12-19T20:05:48+05:30
weight: 7
summary: Rotate the given array left by n times
link: https://www.hackerrank.com/challenges/array-left-rotation/problem
difficulty: Easy
categories: [Arrays]
slug: left-rotate-array
type: coding
sources: [HackerRank]
---

**Link :** [https://www.hackerrank.com/challenges/array-left-rotation/problem](https://www.hackerrank.com/challenges/array-left-rotation/problem)

**Difficulty :** Easy

## Solution 1: Rotate element by element left d times

```c
void roateLeftByOne(int *arr, int arr_count)
{
    int temp = arr[0];
    int i;
    for (i = 0; i < arr_count - 1; i++)
    {
        arr[i] = arr[i + 1];
    }
    arr[i] = temp;
}

int* rotateLeft(int d, int arr_count, int* arr, int* result_count)
{
    // brute force
    for (int i = 0; i < d; i++)
    {
        roateLeftByOne(arr, arr_count);
    }
    *result_count = arr_count;
    return arr;
}
```

**Time Complexity :** O(n). O(dn) - precise, where d is no of rotations.

**Auxiallry Space :** O(1)

## Solution 2: Rotate d elements left once, n/d times

```c
int* rotateLeft(int d, int arr_count, int* arr, int* result_count)
{
    // greedy
    int tempArr[d], i, j;
    for (i = 0; i < d; i++)
    {
        tempArr[i] = arr[i];
    }
    for (i = 0; i < (arr_count - d); i += d)
    {
        for (j = 0; j < d; j++)
        {
            arr[i + j] = arr[i + j + d];
        }
    }
    j = 0;
    for (i = (arr_count - d); i < arr_count; i++)
    {
        arr[i] = tempArr[j++];
    }
    *result_count = arr_count;
    return arr;
}
```

**Time Complexity :** O(n). O(d + n) - precise, where d is no of rotations.

**Auxiallry Space :** O(d)

## Solution 3: Rotate i element of d blocks left once, n/d times

```c
int* rotateLeft(int d, int arr_count, int* arr, int* result_count)
{
    // greedy
    int tempArr[d], i, j;
    for (i = 0; i < d; i++)
    {
        tempArr[i] = arr[i];
    }
    for (i = 0; i < d; i ++)
    {
        for (j = 0; j < arr_count - d; j += d)
        {
            arr[i + j] = arr[i + j + d];
        }
    }
    j = 0;
    for (i = (arr_count - d); i < arr_count; i++)
    {
        arr[i] = tempArr[j++];
    }
    *result_count = arr_count;
    return arr;
}
```

**Time Complexity :** O(n). O(d + n) - precise, where d is no of rotations.

**Auxiallry Space :** O(d)

## Solution 4: Reverse array

```c
void reverseArray(int *arr, int left, int right)
{
    int temp, i, mid;
    if (left == right) return;
    while(left < right)
    {
        temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
        left++;
        right--;
    }
}

int* rotateLeft(int d, int arr_count, int* arr, int* result_count)
{
    // optimum
    d = d % arr_count;
    reverseArray(arr, 0, d - 1);
    reverseArray(arr, d, arr_count - 1);
    reverseArray(arr, 0, arr_count - 1);
    *result_count = arr_count;
    return arr;
}
```

**Time Complexity :** O(n). O(2n) - precise.

**Auxiallry Space :** O(1)
