---
title: 2D Array
date: 2019-12-19T20:05:48+05:30
weight: 4
summary: Reverse the given array
link: https://www.hackerrank.com/challenges/2d-array/problem
difficulty: Easy
categories: [Arrays]
slug: 2d-array
type: coding
sources: [HackerRank]
---

> **Link:** [https://www.hackerrank.com/challenges/2d-array/problem](https://www.hackerrank.com/challenges/2d-array/problem)
>
> **Difficulty:** Easy

## Solution 1

```c
// 1 1 1
//   1
// 1 1 1 will return sum of elements at theses indexes
// given the start index of row & col
int getSum(int rowIndex, int colIndex, int **arr)
{
    int i = rowIndex, j = colIndex;
    int sum = 0;
    for (i = rowIndex; i < rowIndex+3; i++)
    {
        for (j = colIndex; j < colIndex+3; j++)
        {
            if (i == rowIndex)
                sum += arr[i][j];
            else if (i == rowIndex + 2)
                sum += arr[i][j];
            else if (i - rowIndex == j - colIndex)
                sum += arr[i][j];
        }
    }
    return sum;
}

int hourglassSum(int arr_rows, int arr_columns, int** arr)
{
    int i = 0, j = 0;
    int sum = 0;
    int maxSum = -64; // negative values (-9 * 7)
    for (i = 0; i < (arr_rows - 2); i++)
    {
        for (j = 0; j < (arr_columns - 2); j++)
        {
            sum = getSum(i, j, arr);
            if (sum > maxSum)
                maxSum = sum;
        }
    }
    return maxSum;
}
```

> **Time Complexity:** O(n). O(2n) - precise.
>
> **Auxiliary Space:** O(n).
