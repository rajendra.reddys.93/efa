---
title: Reverse an Array
date: 2019-12-19T20:05:48+05:30
weight: 5
summary: Reverse the given array
link: https://www.hackerrank.com/challenges/arrays-ds/problem
difficulty: Easy
categories: [Arrays]
slug: reverse-an-array
type: coding
sources: [HackerRank]
---

> **Link**: [https://www.hackerrank.com/challenges/arrays-ds/problem](https://www.hackerrank.com/challenges/arrays-ds/problem)
>
> **Difficulty**: Easy

## Solution 1: Duplicate array

```c
int* reverseArray(int a_count, int* a, int* result_count)
{
    int i = 0;
    int tempArr[a_count];
 
    // copy all elements of array in reverse order into tempArr
    for (i = 0; i < a_count; i++)
    {
        tempArr[a_count - i - 1] = a[i];
    }
 
    // copy back reversed array into a
    for (i = 0; i < a_count; i++)
    {
        a[i] = tempArr[i];
    }
 
    *result_count = a_count;
    return a;
}
```

> **Time Complexity:** O(n). O(2n) - precise.
>
> **Auxiliary Space:** O(n).

## Solution 2: Inplace swapping of array elements

```c
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
 
int* reverseArray(int a_count, int* a, int* result_count)
{
    int i = 0;
 
    // swap from starting till mid
    for (i = 0; i < (a_count / 2); i++)
    {
        swap(&a[i], &a[a_count - i - 1]);
    }
 
    *result_count = a_count;
    return a;
}
```

> **Time Complexity:** O(n). O(n/2) - precise.
>
> **Auxiliary Space:** O(1).
