---
title: Longest Common Prefix
date: 2021-04-19T22:55:09+05:30
weight: 14
summary: Find the longest common prefix string amongst an array of strings.
link: https://leetcode.com/problems/longest-common-prefix/
difficulty: Easy
categories: [String]
slug: longest-common-prefix
type: coding
sources: [Leetcode]
---

Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

### Example 1

```c
Input: strs = ["flower","flow","flight"]
Output: "fl"
```

### Example 2

```c
Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
```

### Constraints

> 0 <= strs.length <= 200
>
> 0 <= strs[i].length <= 200
>
> strs[i] consists of only lower-case English letters.

## Solution 1: Match Each Index

```c
char * longestCommonPrefix(char ** strs, int strsSize)
{    
    if (strsSize == 0) return "";
    if (strsSize == 1) return *strs;
    
    char *res = calloc(sizeof(char), 200);
    int j = 0, i = 0;
    
    while (j < strlen(strs[0]))
    {
        for (i = 0; i < strsSize; i++)
        {
            if (strs[0][j] != strs[i][j])
                return res;
        }
        res[j] = strs[0][j];
        j++;
    }
    return res;
}
```

> **Time Complexity:** O(n * m), where n - no of string, m - length of smallest string.
>
> **Auxiliary Space:** O(1).

## Solution 2: Optimised Solution 1

```c
char * longestCommonPrefix(char ** strs, int strsSize)
{    
    if (strsSize == 0) return "";

    int j = 0, i = 0;
    
    while (j < strlen(strs[0]))
    {
        for (i = 0; i < strsSize; i++)
        {
            if (strs[0][j] != strs[i][j])
            {
                strs[0][j] = '\0';
                return strs[0];
            }
        }
        j++;
    }
    return strs[0];
}
```

> **Time Complexity:** O(n * m), where n - no of string, m - length of smallest string.
>
> **Auxiliary Space:** O(1).

## Solution 3: Match Each String

```c
char * longestCommonPrefix(char ** strs, int strsSize)
{
    if (strsSize == 0) return "";
    if (strsSize == 1) return *strs;
    
    char *prefix = calloc(sizeof(char), 200);
    int j = 0, i = 0;
    
    strncpy(prefix, strs[0], strlen(strs[0]));
    
    for (i = 1; i < strsSize; i++)
    {
        j = 0;
        while (j < strlen(strs[i]))
        {
            if (prefix[j] != strs[i][j])
                break;
            j++;
        }
        prefix[j] = '\0';
    }
    return prefix;
}
```

> **Time Complexity:** O(n * m), where n - no of string, m - length of smallest string.
>
> **Auxiliary Space:** O(1).

## Solution 4: Optimised Solution 2

```c
char * longestCommonPrefix(char ** strs, int strsSize)
{    
    if (strsSize == 0) return "";
    
    int j = 0, prefixLength = 0;
    
    prefixLength = strlen(strs[0]);
    
    while (strsSize--)
    {
        j = 0;
        while (j < prefixLength)
        {
            if (strs[0][j] != strs[strsSize][j])
                break;
            j++;
        }
        strs[0][j] = '\0';
        prefixLength = j;
    }
    return strs[0];
}
```

> **Time Complexity:** O(n * m), where n - no of string, m - length of smallest string.
>
> **Auxiliary Space:** O(1).
