---
title: Running Sum of 1d Array
date: 2023-08-17T22:40:00+05:30
weight: 16
summary: Calculate Array with running sum of all previous numbers.
link: https://leetcode.com/problems/running-sum-of-1d-array/
difficulty: Easy
categories: [Arrays]
slug: running-sum
type: coding
sources: [Leetcode]
---

Given an array `nums`. We define a running sum of an array as `runningSum[i] = sum(nums[0]…nums[i])`.

> The problem statement and examples can be found in **[Leetcode](https://leetcode.com/problems/running-sum-of-1d-array/)** website.

Return the running sum of `nums`.

### Example 1

```c
Input: nums = [1,2,3,4]
Output: [1,3,6,10]
Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
```

### Example 2

```c
Input: nums = [1,1,1,1,1]
Output: [1,2,3,4,5]
Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
```

### Example 3

```c
Input: nums = [3,1,2,10,1]
Output: [3,4,6,16,17]
```

### Constraints

- 1 <= nums.length <= 1000
- 10^6 <= nums[i] <= 10^6

## Solution 1: Duplicate Array

```c++
class Solution {
public:
    vector<int> runningSum(vector<int>& nums) {

        auto const numsSize = nums.size();
        auto result = vector<int>{};
        auto runningSum = 0;
        
        for (auto index = std::size_t{0}; index < numsSize; index++) {
            runningSum += nums[index];
            result.push_back(runningSum);
        }
        
        return result;
    }
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## Solution 1: Inline replacement

```cpp
class Solution {
public:
    vector<int> runningSum(vector<int>& nums) {
        
        auto const numsSize = nums.size();
        
        for (auto index = std::size_t{1}; index < numsSize; index++) {
            nums[index] += nums[index - 1];
        }
        
        return nums;
    }
};
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
