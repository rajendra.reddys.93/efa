---
title: Palindrome Number
date: 2021-04-16T09:57:06+05:30
weight: 9
summary: Is given number is palindrome or not
link: https://leetcode.com/problems/palindrome-number/
difficulty: Easy
categories: [Math]
slug: palindrome-number
type: coding
sources: [Leetcode]
---

Given an integer x, return true if x is palindrome integer.

An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.

### Example 1

```c
Input: x = 121
Output: true
```

### Example 2

```c
Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
```

### Example 3

```c
Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
```

### Example 4

```c
Input: x = -101
Output: false
```

### Constraints

> -231 <= x <= 231 - 1

Follow up: Could you solve it without converting the integer to a string?

## Solution 1: Two Pointers

```c
bool isPalindrome(int x) {

    /* temp variables */
    int leftNum = 0;
    int rightNum = 0;
    int tempNum = x;
    
    /* negative nums are not palindromes */
    if (x < 0) return false;

    /* calculate no of digits in x */
    int noOfDigits = (int)(log10(x) + 1);
    int midCount = noOfDigits / 2;
        
    while (midCount > 0) {

        /* extracting first and last digit from x */
        noOfDigits--;
        leftNum = (int)(x / pow(10, noOfDigits));
        rightNum = (int)(tempNum % 10);

        if ( leftNum != rightNum )
            break;

        /* updating variables for next loop */
        /* drops first digit */
        x = x - (leftNum * pow(10, noOfDigits));
        /* drops last digit */
        tempNum = tempNum / 10;
        midCount--;
    }
    
    return ( (midCount > 0) ? false : true);
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## Solution 2: Optimised

```c

bool isPalindrome(int x) {

    /* temp variables */
    int revertedNum = 0;
    
    /* negative nums are not palindromes */
    if ( (x < 0) || (x % 10 == 0 && x != 0) )
        return false;
    
    // reverse last half
    while (x > revertedNum) {
        revertedNum = revertedNum * 10 + x % 10;
        x = x / 10;
    }
    
    /* comparision for even and odd digits */
    return ( (x == revertedNum) || (x == revertedNum / 10) );
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## Solution 3: Other approaches (Not tested)

```c
bool isPalindrome(int x)
{
    long long unsigned lst_digit, mul = 0;
    unsigned num = x;
    while(x != 0)
    {
        lst_digit = x % 10;
        mul = mul * 10 + lst_digit;
        x = x / 10;
    }
    return ((mul == num) ? true : false);
}
```

```c
bool isPalindrome(int x) {
    
    int digits[10];
    
    if (x < 0) {
        return false;
    }
    
    int maxIndex = 0;
    while(x != 0) {
        digits[maxIndex] = x % 10;
        x /= 10;
        maxIndex++;
    }
    
    int i = 0, j = maxIndex -1;
    
    while (i < j) {
        if (digits[i] != digits[j]) {
            return false;
        }
        i++;
        j--;
    }
    return true;
}
```
