---
title: Add Two Numbers
date: 2021-04-11T10:21:53+05:30
weight: 2
summary: Brief summary
link: https://leetcode.com/problems/add-two-numbers/
difficulty: Medium
categories: [SLL, LinkedList]
slug: add-two-numbers
type: coding
sources: [Leetcode]
---

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

### Example 1

```c
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.
```

### Example 2

```c
Input: l1 = [0], l2 = [0]
Output: [0]
```

### Example 3

```c
Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]
```

### Constraints

- The number of nodes in each linked list is in the range [1, 100].
- 0 <= Node.val <= 9
- It is guaranteed that the list represents a number that does not have leading zeros.

## Solution 1: Two Pointers

```c
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

void insertNode(struct ListNode **tail, struct ListNode *node) {
    if (*tail)
        (*tail)->next = node;        
    
    *tail = node;
}

struct ListNode* createNode(int val) {
    struct ListNode *node = (struct ListNode *)malloc(sizeof(struct ListNode));
    node->next = NULL;
    node->val = val;
    return node;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    
    int sum = 0;
    int carry = 0;
    int num1 = 0;
    int num2 = 0;
    struct ListNode *resTail = NULL;
    struct ListNode *resHead = NULL;
    
    while (l1 || l2) {
        if (l1) {
            num1 = l1->val;
            l1 = l1->next;
        }
        else {
            num1 = 0;
        }
    
        if (l2) {
            num2 = l2->val;
            l2 = l2->next;
        }
        else {
            num2 = 0;
        }
        
        sum = carry + num1 + num2;
        if (sum > 9) {
            carry = sum / 10;
            sum %= 10;
        } else {
            carry = 0;
        }
        
        struct ListNode *node = createNode(sum);
        insertNode(&resTail, node);
        if (resHead == NULL)
            resHead = node;

    }
    if (carry) {
        struct ListNode *node = createNode(carry);
        insertNode(&resTail, node);
    }
    return resHead;
}
```

> **Time Complexity:** O(nlogn).
>
> **Auxiliary Space:** O(1).
