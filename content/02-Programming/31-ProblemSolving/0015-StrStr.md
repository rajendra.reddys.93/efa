---
title: Implement strstr()
weight: 15
summary: Implement custom version strstr()
link: https://leetcode.com/problems/implement-strstr/
difficulty: Easy
categories: [String]
slug: strstr
type: coding
sources: [Leetcode]
---

Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

**Clarification:**

What should we return when needle is an empty string? This is a great question to ask during an interview.

For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().

### Example 1

```c
Input: haystack = "hello", needle = "ll"
Output: 2
```

### Example 2

```c
Input: haystack = "aaaaa", needle = "bba"
Output: -1
```

### Example 3

```c
Input: haystack = "", needle = ""
Output: 0
```

### Constraints

> 0 <= haystack.length, needle.length <= 5 * 104
>
> haystack and needle consist of only lower-case English characters.

## Solution 1: Brute Force

```c
int strStr(char * haystack, char * needle)
{    
    int hayStackLen = strlen(haystack);
    int needleLen = strlen(needle);
    int i = 0;
    int j = 0;
    
    if (needleLen == 0)
        return 0;
    
    for (i = 0; i <= hayStackLen - needleLen; i++)
    {
        if (haystack[i] == needle[0])
        {
            for (j = 1; j < needleLen; j++)
            {
                if (haystack[i+j] != needle[j])
                    break;
            }
            if (j == needleLen)
                return i;
        }
    }
    return -1;
}
```

> **Time Complexity:** O(hayStackLen).
>
> **Auxiliary Space:** O(1).

## Solution 2: Optimized

```c
int strStr(char * haystack, char * needle)
{    
    int hayStackLen = strlen(haystack);
    int needleLen = strlen(needle);
    int i = 0;
    int j = 0;
    
    if (needleLen == 0)
        return 0;
    
    for (i = 0; i <= hayStackLen - needleLen; i++)
    {
        for (j = 0; j < needleLen; j++)
        {
            if (haystack[i + j] != needle[j])
                break;
        }
        if (j == needleLen)
            return i;
    }
    return -1;
}
```

> **Time Complexity:** O(hayStackLen).
>
> **Auxiliary Space:** O(1).
