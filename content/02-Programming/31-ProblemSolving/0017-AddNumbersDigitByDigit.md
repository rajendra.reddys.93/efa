---
title: Add 2 Numbers Digit by Digit
date: 2023-11-06T20:27:48+0530
weight: 17
summary: Digit by digit addition of 2 numbers
slug: add-numbers-digit-by-digit
difficulty: easy
categories: [Math]
type: coding
sources: [EFA]
---

123 + 456 = 579

This has to be done by adding each digit one by one.

Example 1:

```
123
456
---
789

3 + 6 = 9
2 + 5 = 7
1 + 4 = 5
= 579
```

Example 2:

```
786093
193719
---
979812

          3 + 9 = 12 (2, 1 carry)
carry 1 + 9 + 1 = 11 (1, 1 carry)
carry 1 + 0 + 7 = 8  (8)
          6 + 3 = 9  (9)
          8 + 9 = 17 (7, 1 carry)
carry 1 + 7 + 1 = 9  (9)
= 979812
```

Example 3:

```
246
 35
---
281

          6 + 5 = 11 (1, 1 carry)
carry 1 + 4 + 3 = 8
          2 + 0 = 2
= 281
```

## Solution 1: Using while loop

```cpp
constexpr int addLastDigitWithCarry(int const num1, int const num2, int const carry) {
    int const num1LastDigit = num1 % 10;
    int const num2LastDigit = num2 % 10;
    return num1LastDigit + num2LastDigit + carry;
}

constexpr int power(int const num, int const times) {
    if (!times) return 1;
    return num * power(num, times - 1);
}

constexpr int multiplyByDigitPlace(int const sum, int const digitPlace) {
    return (sum % 10) * power(10, digitPlace);
}

constexpr int add(int num1, int num2) {
    int prevCarry = 0;
    int digitPlace = 0;
    int result = 0;

    while (num1 || num2) {
        int const digitSum = addLastDigitWithCarry(num1, num2, prevCarry);
        prevCarry = (digitSum > 9) ? 1 : 0;
        result += multiplyByDigitPlace(digitSum, digitPlace);
        digitPlace++;
        num1 /= 10;
        num2 /= 10;
    }

    return result;
}

int main() {
    // auto const sum = add(123, 456);
    // return (sum == 123 + 456) ? 1 : 0;
    // auto const sum = add(246, 35);
    // return (sum == 246 + 35) ? 1 : 0;
    auto const sum = add(786093, 193719);
    return (sum == 786093 + 193719) ? 1 : 0;
}
```

## Solution 2: Functional programming using recursion

```cpp
constexpr int addLastDigitWithCarry(int const num1, int const num2, int const carry) {
    int const num1LastDigit = num1 % 10;
    int const num2LastDigit = num2 % 10;
    return num1LastDigit + num2LastDigit + carry;
}

constexpr int power(int const num, int const times) {
    if (!times) return 1;
    return num * power(num, times - 1);
}

constexpr int multiplyByDigitPlace(int const sum, int const digitPlace) {
    return (sum % 10) * power(10, digitPlace);
}

constexpr int add(int const num1, int const num2, int const prevCarry = 0, int const digitPlace = 0) {
    if (num1 || num2) {
        int const digitSum = addLastDigitWithCarry(num1, num2, prevCarry);
        int const carry = (digitSum > 9) ? 1 : 0;
        int const result = multiplyByDigitPlace(digitSum, digitPlace);
        return result + add(num1 / 10, num2 / 10, carry, digitPlace + 1);
    }
    return 0;
}

int main() {
    // auto const sum = add(123, 456);
    // return (sum == 123 + 456) ? 1 : 0;
    // auto const sum = add(246, 35);
    // return (sum == 246 + 35) ? 1 : 0;
    auto const sum = add(786093, 193719);
    return (sum == 786093 + 193719) ? 1 : 0;
}
```