---
title: "Longest Substring Length"
date: 2021-04-16T09:26:09+05:30
weight: 3
summary: Longest Substring without repeating charecters
link: https://leetcode.com/problems/longest-substring-without-repeating-characters/
difficulty: Medium
categories: [Strings, Hash Table, Two Pointer, Sliding Window]
slug: longest-substring-length
type: coding
sources: [Leetcode]
---

Given a string s, find the length of the longest substring without repeating characters.

### Example 1

```c
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
```

### Example 2

```c
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
```

### Example 3

```c
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
```

### Example 4

```c
Input: s = ""
Output: 0
```

### Constraints

> 0 <= s.length <= 5 * 104
>
> s consists of English letters, digits, symbols and spaces.

## Solution 1: Brute Force

```c
bool isAllUnique(char *s, int start, int end) {
    uint8_t hash[128] = {0};
    for (int i = start; i <= end; i++) {
        if (hash[s[i]] == 1)
            return false;
        hash[s[i]] = 1;
    }
    return true;
}

int lengthOfLongestSubstring(char * s) {
    
    int maxLength = 0;
    int sLength = strlen(s);

    if ( (s == NULL) || (sLength == 0) )
        return 0;
    
    if (sLength == 1) return 1;
        
    for (int i = 0; i < sLength; i++) {
        for (int j = i; j < sLength; j++) {
            if ( (j - i + 1) > maxLength && isAllUnique(s, i, j) ) {
                maxLength = j - i + 1;
            }
        }
    }
    return maxLength;
}
```

> **Time Complexity:** O(n^3).
>
> **Auxiliary Space:** O(1).
