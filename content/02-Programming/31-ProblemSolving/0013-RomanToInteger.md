---
title: Roman To Integer
date: 2021-04-19T16:03:15+05:30
weight: 13
summary: Given a roman numeral, convert it to an integer.
link: https://leetcode.com/problems/roman-to-integer/submissions/
difficulty: Easy
categories: [String]
slug: roman-to-integer
type: coding
sources: [Leetcode]
---

Roman numerals are represented by seven different symbols: `I`, `V`, `X`, `L`, `C`, `D` and `M`.

```c
Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
```

For example, `2` is written as `II` in Roman numeral, just two one's added together. `12` is written as `XII`, which is simply `X + II`. The number `27` is written as `XXVII`, which is `XX + V + II`.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not `IIII`. Instead, the number four is written as `IV`. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as `IX`. There are six instances where subtraction is used:

- `I` can be placed before `V` (5) and `X` (10) to make 4 and 9.
- `X` can be placed before `L` (50) and `C` (100) to make 40 and 90.
- `C` can be placed before `D` (500) and `M` (1000) to make 400 and 900.

Given a roman numeral, convert it to an integer.

### Example 1

```c
Input: s = "III"
Output: 3
```

### Example 2

```c
Input: s = "IV"
Output: 4
```

### Example 3

```c
Input: s = "IX"
Output: 9
```

### Example 4

```c
Input: s = "LVIII"
Output: 58
Explanation: L = 50, V= 5, III = 3.
```

### Example 5

```c
Input: s = "MCMXCIV"
Output: 1994
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
```

### Constraints

> 1 <= s.length <= 15
>
> s contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M').
>
> It is guaranteed that s is a valid roman numeral in the range [1, 3999].

## Solution 1: Brute Force

```c
int romanCharToVal(char c) {
    switch(c) {
        case 'I': return 1;
        case 'V': return 5;
        case 'X': return 10;
        case 'L': return 50;
        case 'C': return 100;
        case 'D': return 500;
        case 'M': return 1000;
    }
    return 0;
}

int romanToInt(char * s) {
    int curVal = 0;
    int prevVal = 0;
    int res = 0;
    for (int i = strlen(s); i > -1; i--) {
        curVal = romanCharToVal(s[i]);
        if (curVal < prevVal) {
            res -= curVal;
        } else {
            res += curVal;
        }
        prevVal = curVal;
    }
    return res;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).

## Solution 2: Hash Map

```c
int romanToInt(char * s) {
    int curVal = 0;
    int prevVal = 0;
    int res = 0;    
    int valueMap[26];

    valueMap['I' - 'A'] = 1;
    valueMap['V' - 'A'] = 5;
    valueMap['X' - 'A'] = 10;
    valueMap['L' - 'A'] = 50;
    valueMap['C' - 'A'] = 100;
    valueMap['D' - 'A'] = 500;
    valueMap['M' - 'A'] = 1000;

    for (int i = strlen(s) - 1; i > -1; i--) {
        curVal = valueMap[s[i] - 'A'];
        if (curVal < prevVal) {
            res -= curVal;
        } else {
            res += curVal;
        }
        prevVal = curVal;
    }
    return res;
}
```

> **Time Complexity:** O(n).
>
> **Auxiliary Space:** O(1).
