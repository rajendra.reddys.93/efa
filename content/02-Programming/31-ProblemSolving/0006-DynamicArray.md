---
title: Dynamic Array
date: 2019-12-19T20:05:48+05:30
weight: 6
summary: Reverse the given array
link: https://www.hackerrank.com/challenges/dynamic-array/problem
difficulty: Easy
categories: [Arrays]
slug: dynamic-array
type: coding
sources: [HackerRank]
---

## Not solved yet

**Link :** [https://www.hackerrank.com/challenges/dynamic-array/problem](https://www.hackerrank.com/challenges/dynamic-array/problem)

**Difficulty :** Easy

## Solution 1

```c

```

**Time Complexity :** O(n)

**Auxiallry Space :** O(1)
