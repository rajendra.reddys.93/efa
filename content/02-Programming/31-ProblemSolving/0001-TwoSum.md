---
title: Two Sum
date: 2021-04-10T09:41:00+0000
weight: 1
summary: Find indices of 2 numbers whose sum is target.
link: https://leetcode.com/problems/two-sum/
difficulty: Easy
categories: [Arrays]
slug: two-sum
type: coding
sources: [Leetcode]
---

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

> The problem statement and examples can be found in **[Leetcode](https://leetcode.com/problems/two-sum/)** website.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

### Example 1

```c
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Output: Because nums[0] + nums[1] == 9, we return [0, 1].
```

### Example 2

```c
Input: nums = [3,2,4], target = 6
Output: [1,2]
```

### Example 3

```c
Input: nums = [3,3], target = 6
Output: [0,1]
```

### Constraints

- 2 <= nums.length <= 103
- -109 <= nums[i] <= 109
- -109 <= target <= 109
- Only one valid answer exists.

## Solution 1: Brute Force

```c
int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
    *returnSize = 2;
    int *resArr = malloc(2 * sizeof(int));
    for (int i = 0; i < numsSize; i++) {
        for (int j = i + 1; j < numsSize; j++) {
            if (nums[i] + nums[j] == target) {
                resArr[0] = i;
                resArr[1] = j;
                return resArr;
            }
        }
    }
    return resArr;
}
```

> **Time Complexity:** O(nlogn).
>
> **Auxiliary Space:** O(1).
