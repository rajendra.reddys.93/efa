﻿---
title: Disclaimer
weight: 7
summary: Disclaimer before using services.
---

Last updated: May 15, 2017

The information contained on [http://efaelectronicsforall.com/](http://efaelectronicsforall.com/) website (the "Service") is for general information purposes only.

EFA-Electronics For All assumes no responsibility for errors or omissions in the contents on the Service.

In no event shall EFA-Electronics For All be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. EFA-Electronics For All reserves the right to make additions, deletions, or modification to the contents on the Service at any time without prior notice.

EFA-Electronics For All does not warrant that the website is free of viruses or other harmful components.

This Disclaimer is licensed by TermsFeed Generator to EFA-Electronics For All.

## External links disclaimer

[http://efaelectronicsforall.com/](http://efaelectronicsforall.com/) website may contain links to external websites that are not provided or maintained by or in any way affiliated with EFA-Electronics For All.

Please note that the EFA-Electronics For All does not guarantee the accuracy, relevance, timeliness, or completeness of any information on these external websites.
