﻿---
title: Contribute
weight: 3
summary: Contribute to the content of this site.
---

This website is built with the content submitted by contributors. If you have a desire to share the knowledge that you know about electronics and programming to the world. You are in the write place. Please write to us at {{<mail>}} .

## Present Work

We are looking for contributions for below pages.

1. Education
   1. Electronics
   2. Micro-controllers
   3. C Programming
   4. PCB Designing

        PCB Designing using :
        * Orcad
        * Allegro
        * EAGLE
2. Tools
    1. Calculators
        * 555 Timer
        * Capacitor Charge Time
        * Star - Delta Transformer
        * Filter Circuits
        * Frequency and Wavelength
        * Fuse Color Code
        * Inductor Color Code
        * LM317 Constant Current
        * LM317 Voltage Regulator
        * Resistor Color Code
        * Resistor Lookup
        * Resonance
        * Stepper Motor
        * Wheatstone Bridge
    2. Converters
        * Area
        * Energy
        * Frequency
        * Mass
        * Speed
    3. PCB Utilities
        * PCB Trace Resistance
        * PCB Trace Width
3. Resources
    1. Standards & Constants
        * IC Packages
        * LM317 Regulator
        * LM788 Regulator
        * Number System
        * Radio Frequency
    2. Symbols
        * Miscellaneous
    3. Pin-Outs
        * 25 Pair Twisted Cable Color Code
        * Apple 30 Pin Connector
        * Apple Lighting Connector
        * Arduino Board Pin-outs
        * ATX Power Connector
        * Audio DIN Connector
        * AVR ISP Pin-outs
        * Beagle-bone Pin-outs
        * Car Audio ISO Connector
        * Display Port
        * DVI Connector
        * EIDE SATA Connector
        * Ethernet Connector
        * Fiber Optic Color Code
        * Fire Wire Connector
        * Jack Pin-outs
        * JTAG Pin-outs
        * LCD Display Pin-outs
        * MIDI Connector
        * MIDI Game Port
        * ODB2 Car Connector
        * Parallel Pin-outs
        * PC Peripheral
        * PDMI Connector
        * PS2 AT Connector
        * RCA Connector
        * RJ Connector
        * S Video Port
        * SCART Connector
        * Serial Port
        * USB Pin-outs
        * VESA Pin-outs
        * VGA Connector
        * XLR DMX Connector

## Other Work

In addition to above concepts, if you have any concept of your own and want to share with world. You can write to us at {{<mail>}} .

If you are looking to announce new products to the world, you can write around 150-200 words with your product image. We will publish your product in our NEWS category.

If you are open minded and you have a great idea. If you want to share your ideas with the world, you can write to us with details of your idea. We will publish you brand new Idea on our website to reach masses.

We will help you in bringing your Concepts and Idea to the world.

<!-- We encourage Digital Media. SAVE TREES! -->
