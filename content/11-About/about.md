---
title: About Us
weight: 1
summary: More about us.
---

EFA-Electronics For All is one stop solution for all who desire to learn about Electronics. With our detailed articles and tutorials written by Industry experts, Professors, well versed Lecturers any one can grasp the knowledge just by reading through them. We don't restrict ourselves to the text based content but we also focus on visualizing the concepts using the images, simulations and explainer videos. We believe that electronics is lot more fun to learn and practice. We want make it even more interesting with our great way of explaining it.

## Our Mission

To provide quality education about electronics to the masses via on-line and off-line content. We thrive to help students(any one who want to learn) across the globe to get their doubts clarified and learn new concepts by the industry experts and professors.

## Our Vision

To become global education service provider both in on-line and off-line content by delivering world class education. We want to diversify the way education is given to students, from theoretical concepts to practical experience by conducting workshops in their premises to enable them to reach their professional goals.

## Who We Are

We are group of electronics and communication engineers with a couple years of experience in the electronics and semiconductor industry. We also collaborate with the various professionals from other industries to present the concepts to our readers on this website. We have great team of professional tutors who are passionate about sharing knowledge about electronics.

## Our Expertise

### Hardware

* Schematics
* PCBs

### Firmware

* Microchip's PIC-8, PIC-16, PIC-32 controllers
* TI's MSP430 controllers
* NXP's 8051 controllers

### Software

* Windows 10 Mobile Applications - [EFA-Electronics for All](https://www.microsoft.com/en-us/store/p/efa-electronics-for-all/9nblggh5131t) , [UDP - Sender/Receiver](https://www.microsoft.com/en-us/store/p/udp-sender-reciever/9nblggh52bt0)
* Website - [EFA-Electronics For All](http://efaelectronicsforall.com)

### Video Editing, Motion Graphics, Designing

* [Image Invitations](/about/media/invitations)
* [Business Cards](/about/media/business-cards)
* [Video Editing](/about/media/video-editing)
* [Brochures](/about/media/brochures)
* [Menu Cards](/about/media/menu-cards)
* [Banners](/about/media/banners)
* [Certificates](/about/media/certificates)
* [Concept Designs](/about/media/concept-designs)
* [Logos](/about/media/logos)

## How You Can Contribute To EFA

If you interested in writing articles to EFA-Electronics For All, you can send your work to us at **[support@efaelectronicsforall.com](mailto:support@efaelectronicsforall.com)** or {{<mail>}}. We will publish your content on our website, once reviewed.

## Who Can Contribute To EFA

Anyone who are working in electronic industry or departments Related to electronics. If you are a Teacher/Lecturer/Professor, share your work on research or the subject that you teach or guide. If you are a student, share your projects that you might have done in your college. If you are a professional, share your projects or work carried out by your personal interest.

Please visit our [Contribute](~/WebHome/Contribute) page to know more.
