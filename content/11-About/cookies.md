﻿---
title: Cookies Policy
weight: 8
summary: Cookies policy.
---

Last updated: May 15, 2017

EFA-Electronics For All ("us", "we", or "our") uses cookies on the
[http://efaelectronicsforall.com](http://efaelectronicsforall.com/)
website (the "Service"). By using the Service, you consent to the use of cookies.

Our Cookies Policy explains what cookies are, how we use cookies, how
third-parties we may partner with may use cookies on the Service, your choices
regarding cookies and further information about cookies. This Cookies Policy is
licensed by TermsFeed Generator to EFA-Electronics For All.

## What are cookies

Cookies are small pieces of text sent by your web browser by a website you
visit. A cookie file is stored in your web browser and allows the Service or
a third-party to recognize you and make your next visit easier and the Service
more useful to you.

Cookies can be "persistent" or "session" cookies. Persistent cookies remain
on your personal computer or mobile device when you go off-line, while session
cookies are deleted as soon as you close your web browser.

## How EFA-Electronics For All uses cookies

When you use and access the Service, we may place a number of cookies files in
your web browser.

**We use cookies for the following purposes:**

To enable certain functions of the Service

We use both session and persistent cookies on the Service and we use different
types of cookies to run the Service:

Essential cookies. We may use essential cookies to authenticate users and prevent fraudulent use of user accounts.

## What are your choices regarding cookies

If you'd like to delete cookies or instruct your web browser to delete or
refuse cookies, please visit the help pages of your web browser.

Please note, however, that if you delete cookies or refuse to accept them,
you might not be able to use all of the features we offer, you may not be able
to store your preferences, and some of our pages might not display properly.

1. For the Chrome web browser, please visit this page from Google:
   [https://support.google.com/accounts/answer/32050](https://support.google.com/accounts/answer/32050)
2. For the Internet Explorer web browser, please visit this page from Microsoft:
   [http://support.microsoft.com/kb/278835](http://support.microsoft.com/kb/278835)
3. For the Firefox web browser, please visit this page from Mozilla:
   [https://support.mozilla.org/en-US/kb/delete-cookies-remove-info-websites-stored](https://support.mozilla.org/en-US/kb/delete-cookies-remove-info-websites-stored)
4. For the Safari web browser, please visit this page from Apple:
   [https://support.apple.com/kb/PH21411?locale=en_US](https://support.apple.com/kb/PH21411?locale=en_US)
5. For any other web browser, please visit your web browser's official web pages.

## Where can you find more information about cookies

You can learn more about cookies and the following third-party websites:

1. AllAboutCookies: [http://www.allaboutcookies.org/](http://www.allaboutcookies.org/)
2. Network Advertising Initiative: [http://www.networkadvertising.org/](http://www.networkadvertising.org/)
