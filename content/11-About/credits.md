﻿---
title: Credits
weight: 4
summary: Credits to the few tools used by us.
---

The journey of building EFA-Electronics For All (efaelectronicsforall.com) gives us the great pleasure. In the process of building our website, we have used various tools and resources. We take this as great opportunity to thank each one of them. Below are the tools and resources which made our work easy.

## Eagle

**[Eagle](https://www.autodesk.com/products/eagle/overview)** is Autodesk's free Electronics design tool. one can design Schematics and PCBs using Eagle. It includes a bunch of PCB Libraries with component symbols and PCB Footprint to ease the process of designing. All circuits in our website were created using Eagle software.

## PartSim.com

**[PartSim](http://www.partsim.com/)** is a free and easy to use circuit simulator that runs in your web browser. This web browser based simulation tool helped us simulate and obtain the waveform for any circuit. The Integration of various electronics and electrical components made our simulation very easy. Even you can use this great tool to simulate your circuits for free.

## W3Schools.com

**[W3Schools](https://www.w3schools.com/)** is a web developers site, with tutorials and references on web development languages such as HTML, CSS, JavaScript, PHP, SQL, W3.CSS, and Bootstrap, covering most aspects of web programming. It helped us to understand the basics of building a website. We have learned HTML, CSS, JQuery and JavaScript. It saved our time by introducing us to Bootstrap, which is open-source web framework.

## WebAlice

**[WebAlice](http://www.webalice.it/corrado.cantelmi/html/formulas/How_to_Write_Formulas_in_HTML5_and_CSS.html)**, The formulas and equations in our website are all text based not images. The advantage is that you can use these formulas in your work just by copying it. The CSS file obtained from [http://www.webalice.it/ corrado.cantelmi/ html/formulas/ How_to_Write_Formulas_in_HTML5_and_CSS.html](http://www.webalice.it/corrado.cantelmi/html/formulas/How_to_Write_Formulas_in_HTML5_and_CSS.html) helped us to make it possible to create each and every formula and equation in pure text, rather than a image file.

## Paint.Net

**[paint.net](https://www.getpaint.net/)** is free-ware graphic/image editing tool. It helped us to create and edit the various concepts and circuit in our project. Its light weight and advanced features are unmatchable with any other tool in its segment.

## MVC Forum

**[MVC Forums](http://www.mvcforum.com/)** is a new open source project for forums. It is developed using MVC 5, Entity Framework and Unity. The forum of our website is a complete customized version of it.
