
// vars
const cacheName = "efa-cache-v001";
const filesToCache = [
    "/index.html",
    "/offline.html",
    "/404.html",
    "/css/bundle.min.css",
    "/js/bundle.min.js",
    "/images/mbededu-logo.png",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "https://fonts.googleapis.com/css?family=Nunito:400,700"
];

// precache
self.addEventListener("install", e => {
    console.log("[ServiceWorker**] Install");
    e.waitUntil(
        caches.open(cacheName).then(cache => {
            console.log("[ServiceWorker**] Caching app shell");
            return cache.addAll(filesToCache);
        })
    );
});

// remove old cache
self.addEventListener("activate", event => {
    caches.keys().then(keyList => {
        return Promise.all(
            keyList.map(key => {
                if (key !== cacheName) {
                    console.log("[ServiceWorker] - Removing old cache", key);
                    return caches.delete(key);
                }
            })
        );
    });
});

// fetch event handler
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request).then(response => {
            return response || fetch(event.request);
        })
    );
});
